# Default - 0
randParams         = 0 

# 0: Caffe, 1: TensorFlow, 2: ONNX, Default - 0
modelType          = 2

# 0: Fixed quantization By tarininng Framework, 1: Dyanamic quantization by TIDL, Default - 1
quantizationStyle  = 1 

# quantRoundAdd/100 will be added while rounding to integer, Default - 50
quantRoundAdd      = 25

numParamBits       = 8 

inputNetFile      = "./test/testvecs/config/onnx_models/squeezenet1.1.onnx"
outputNetFile      = "./test/testvecs/config/tidl_models/onnx/tidl_net_squeezenet1.1.bin"
outputParamsFile   = "./test/testvecs/config/tidl_models/onnx/tidl_io_squeezenet1.1.bin"

inWidth  = 227
inHeight = 227 
inNumChannels = 3

rawSampleInData = 0
preProcType = 256
inMean = 123.675 116.28 103.53
inScale = 0.017125 0.017507 0.017429

sampleInData = "./test/testvecs/input/airshow_raw.y"
tidlStatsTool = "eve_test_dl_algo_ref.out"
