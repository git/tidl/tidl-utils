Two files (onnx.pb.cc and onnx.pb.h) are generated on Linux x86, by building Onnx with Protobuf/Protoc version 3.4.1.

Procedure to build onnx interface files:
1. Install dependency:
   - Protobuf version 3.4.1:
       git clone https://github.com/google/protobuf.git
       cd protobuf
       git checkout v3.4.1
       git submodule update --init --recursive
       ./autogen.sh
       ./configure 
       Make
       Make install

2. Build onnx
$ export ONNX_ML=1 #To clone ONNX with its ML extension
$ git clone https://github.com/onnx/onnx.git
$ unset ONNX_ML
$ cd onnx
$ export LD_LIBRARY_PATH=../protobuf-3.4.1/src/.libs:LD_LIBRARY_PATH
$ ../protobuf-3.4.1/src/protoc onnx/onnx.proto --proto_path=. --proto_path=../protobuf-3.4.1/src --cpp_out onnx/

After building complete, find onnx.pb.cc and onnx.pb.h in /onnx/onnx/