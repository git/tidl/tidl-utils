Collection of utilities for TIDL software package. Includes:
- Binary version of TIDL import tool (for Linux x86 and Linux ARM), which converts caffemode and TF-Slim models to internal represantation used by TIDL Lib.
  Also fast simulation version needed for initial calibration of dynamic quantization parameters is included.
- Simulation tool (for Linux x86 and Linux ARM), bit-exact with EVE execution, is also provided in TIDL-Utils package.
- Set of configuration files for import procedure as well as simulation, are also included
