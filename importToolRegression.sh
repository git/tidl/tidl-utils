#!/bin/bash

# This script can be used to run import tool regression test on both EVM and x86 Linux-devkit. 
# The executables, tidl_model_import.out and eve_test_dl_algo_ref.out, are supposed to be already 
# in the file system and can be searched by PATH env:
#   - in /usr/bin on EVM, 
#   - in <PLSDK installation dir>/linux-devkit/sysroots/x86_64-arago-linux/usr/bin on x86

# If the executables are in different directoris, the file path for tidl_model_import.out 
# needs to be specified in the commands below, and the file path for eve_test_dl_algo_ref.out 
# needs to be specified in the config files tidl_import_xxx.txt. 

tidl_model_import.out ./test/testvecs/config/import/caffe/tidl_import_j11_bn.txt
tidl_model_import.out ./test/testvecs/config/import/caffe/tidl_import_j11_cifar.txt
tidl_model_import.out ./test/testvecs/config/import/caffe/tidl_import_j11_v2.txt
tidl_model_import.out ./test/testvecs/config/import/caffe/tidl_import_JDetNet_768x320.txt
tidl_model_import.out ./test/testvecs/config/import/caffe/tidl_import_jseg21.txt
tidl_model_import.out ./test/testvecs/config/import/tensorflow/tidl_import_inceptionNetv1.txt
tidl_model_import.out ./test/testvecs/config/import/tensorflow/tidl_import_mobileNetv1.txt
tidl_model_import.out ./test/testvecs/config/import/onnx/tidl_import_squeezeNet1_1.txt
