Caffe package is not, currently, supported in Arago, so we use Linux x86 Caffe build for protobuf interface files.
These two files (caffe.pb.cc and caffe.pb.h) are generated on Linux x86, by building Caffe with Protobuf/Protoc version 3.6.1.
Please refer to ../readme.md about exact steps how to recreate these files.

