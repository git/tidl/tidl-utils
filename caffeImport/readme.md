Caffe package is not, currently, supported in Arago, so we use Linux x86 Caffe build for protobuf interface files.
These two files (caffe.pb.cc and caffe.pb.h) are generated on Linux x86, by building Caffe with Protobuf/Protoc version 3.4.1.

Procedure to build caffe interface files:
1. Get caffe release from: https://github.com/BVLC/caffe
   - This was verified with release 1.0: https://github.com/BVLC/caffe/archive/1.0.tar.gz
   - Follow the install instruction (for Caffe-CPU only) from: http://caffe.berkeleyvision.org/installation.html
2. Install dependency as specified in above install instruction:
   - Protobuf version 3.4.1:
       git clone https://github.com/google/protobuf.git
       cd protobuf
       git checkout v3.4.1
       git submodule update --init --recursive
       ./autogen.sh
       ./configure 
       Make
       Make install

   - boost:
       Download boost_1_67_0.tar.bz2, from https://www.boost.org/users/history/version_1_67_0.html
       Unzip 
       ./bootstrap.sh
       ./b2 install cxxflags=-fPIC --with-filesystem --with-test --with-log --with-program_options

   - gflag:
       git clone https://github.com/gflags/gflags.git
       cd gflags
       mkdir build && cd build
       cmake -DCMAKE_BUILD_TYPE=Release CMAKE_CXX_FLAGS="-fPIC" .. && cmake --build .
       make
       make install


After building the cafe, find caffe.pb.cc and caffe.pb.h in Caffe folder: ./.build_release/src/caffe/proto/

