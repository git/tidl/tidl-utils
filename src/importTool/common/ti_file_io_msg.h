/*
 *
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


#ifndef _OP_STRUCT_H_
#define _OP_STRUCT_H_


#define FILE_IO_SHARED_MEM_BASE      (0x87000000)
#define FILE_IO_SHARED_MEM_CORE_SYNC (FILE_IO_SHARED_MEM_BASE + 0x1000)

#define MAX_NUM_FILES (20U)
#define MAX_FILE_SIZE (200U)
#define MAX_NUM_MSG   (10U)

typedef struct HOSTFILE
{
    uint32_t     id;
    FILE        *fp;
} HOSTFILE;

typedef struct RETVALUE
{
    int32_t      ret;
} RETVALUE;

typedef struct OpStruct
{
    int32_t      opCode;
    char         fileName[MAX_FILE_SIZE];
    char         mode[4];
    uint32_t     fid;
    int32_t      offset;
    size_t       size;
    size_t       count;
} OpStruct;

typedef struct send_recv_Struct
{
    uint8_t      msgType;  //0 - send, 1 - receive
    char        *ptr;
    uint32_t     size;
} send_recv_Struct;


typedef struct MSG_Q
{
    volatile uint8_t   numMsg;
    send_recv_Struct   msgList[MAX_NUM_MSG];
} MSG_Q;

#endif /* _OP_STRUCT_H_ */
