/* Copyright 2018 The TensorFlow Authors. All Rights Reserved.
 * Copyright (c) 2019 Texas Instruments Incorporated

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <cmath>
#include <unordered_map>

#include "ti_dl.h"
#include "schema_generated.h"
#include "tidl_import_config.h"

#define TFLITE_SCHEMA_VERSION (3) // As defined in "tensorflow/lite/version.h"

using namespace std;
using namespace tflite;

template <class T>
using Offset = flatbuffers::Offset<T>;
template <class T>
using Vector = flatbuffers::Vector<T>;
using FlatBufferBuilder = flatbuffers::FlatBufferBuilder;

#define TFLITE_WRITE_SUCCESS    1
#define TFLITE_WRITE_FAILURE    0

  // List of op codes and mappings from builtin or custom op to opcode
struct OpCode {
  int builtin;
  std::string custom;
};

std::vector<std::string> tensornames_;
std::vector<std::string> inputs_, outputs_;
std::vector<std::string> custom_op_inputs_, custom_op_outputs_;
std::vector<std::pair<const uint8_t*, size_t>> buffers_;
std::vector<OpCode> opcodes_;
std::unordered_map<int, int> builtin_op_to_opcode_;
std::unordered_map<std::string, int> custom_op_to_opcode_;

const Model* tfliteModel_;
std::vector<int> evaluatedOps_;
std::vector<int> tfliteOps_;
int lastNodeTidlSubgraph;

int GetOpCodeForCustom(const std::string& custom_name)
{
  std::pair<decltype(custom_op_to_opcode_)::iterator, bool> result =
      custom_op_to_opcode_.insert(
          std::make_pair(custom_name, opcodes_.size()));
  if (result.second) {
    opcodes_.push_back({BuiltinOperator_CUSTOM, custom_name});
  }
  return result.first->second;
}

int GetOpCodeForBuiltin(int builtin_op_index) {
  std::pair<decltype(builtin_op_to_opcode_)::iterator, bool> result =
      builtin_op_to_opcode_.insert(
          std::make_pair(builtin_op_index, opcodes_.size()));
  if (result.second) {
    opcodes_.push_back({builtin_op_index, ""});
  }
  return result.first->second;
}

template <class T_OUTPUT, class T_INPUT>
Offset<Vector<T_OUTPUT>> ExportVector(FlatBufferBuilder* fbb,
                                      const T_INPUT& v) {
  std::vector<T_OUTPUT> inputs(v.begin(), v.end());
  return fbb->template CreateVector<T_OUTPUT>(inputs);
}

Offset<Vector<Offset<Buffer>>> ExportBuffers(
    FlatBufferBuilder* fbb) {
  std::vector<Offset<Buffer>> buffer_vector;
  for (auto buffer : buffers_) {
    auto data_offset = fbb->CreateVector(buffer.first, buffer.second);
    buffer_vector.push_back(CreateBuffer(*fbb, data_offset));
  }
  return fbb->template CreateVector<Offset<Buffer>>(buffer_vector);
}

/* Export tensors
 */
Offset<Vector<Offset<Tensor>>> ExportTensors(FlatBufferBuilder* fbb)
{
  std::vector<Offset<Tensor>> tensorsWritten;

  auto tensors  = (*tfliteModel_->subgraphs())[0]->tensors();
  auto operators = (*tfliteModel_->subgraphs())[0]->operators();
  auto outputs   = (*tfliteModel_->subgraphs())[0]->outputs();
  auto inputs    = (*tfliteModel_->subgraphs())[0]->inputs();

  int i, j;

  for (i = 0; i < tensors->size(); i++)
  {
    const auto *tensor = tensors->Get(i);

    int tensorUsed = 0;
    int inputFlag = 0, outputFlag = 0;
    int customInputFlag = 0, customOutputFlag = 0;
    // If the tensor is one of the input/output tensors for the nodes in the tfliteOps list,
    // include the tensor in the output tflite model
    for (auto op_index : tfliteOps_)
    {
      const auto *op = operators->Get(op_index);
      for (j = 0; j < op->outputs()->size(); j++)
      {
        if (strcmp(tensor->name()->c_str(), tensors->Get(op->outputs()->Get(j))->name()->c_str()) == 0)
        {
          tensorUsed = 1;
          break;
        }
      }
      for (j = 0; j < op->inputs()->size(); j++)
      {
        if (strcmp((char *)tensor->name()->c_str(), tensors->Get(op->inputs()->Get(j))->name()->c_str()) == 0)
        {
          tensorUsed = 1;
          break;
        }
      }
      if (tensorUsed)
        break;
    }
    // Flag the input of the whole graph. It is also the input of the tidl supported sugraph
    for (int j = 0; j < inputs->size(); j++)
    {
      if (strcmp((char *)tensor->name()->c_str(), tensors->Get(inputs->Get(j))->name()->c_str()) == 0)
      {
        tensorUsed = 1;
        inputFlag = 1;
        customInputFlag = 1;
        break;
      }
    }
    // Flag the output of the whole graph
    for (int j = 0; j < outputs->size(); j++)
    {
      if (strcmp((char *)tensor->name()->c_str(), tensors->Get(outputs->Get(j))->name()->c_str()) == 0)
      {
        tensorUsed = 1;
        outputFlag = 1;
        break;
      }
    }

    // If the tensor is the output of the lastNodeTidlSubgraph, it is the output of the tidl supported subgraph
    const auto *op = operators->Get(lastNodeTidlSubgraph);
    for (j = 0; j < op->outputs()->size(); j++)
    {
      if (strcmp((char *)tensor->name()->c_str(), tensors->Get(op->outputs()->Get(j))->name()->c_str()) == 0)
      {
        tensorUsed = 1;
        customOutputFlag = 1;
        break;
      }
    }

    if (tensorUsed)
    {
      // Create buffers with the buffer data and set the new buffer index
      int buffer_index = buffers_.size();
      // Buffer index of 0 is NULL, and therefore, adding an empty buffer
      // for it so that the actual buffers start with index 1
      if (buffer_index == 0)
      {
        buffers_.push_back(std::make_pair(
            reinterpret_cast<const uint8_t*>(NULL), 0));
        buffer_index = buffers_.size();
      }

      auto *data = (*tfliteModel_->buffers())[tensor->buffer()]->data();
      if (data)
      {
        buffers_.push_back(std::make_pair(
            reinterpret_cast<const uint8_t*>(data->data()), data->size()));
      } else {
        buffers_.push_back(std::make_pair(
            reinterpret_cast<const uint8_t*>(NULL), 0));
      }

      // Handle quantization parameters
      Offset<QuantizationParameters> quantization_params_written = 0;
      auto *quantization_params = tensor->quantization();
      if (quantization_params)
      {
        flatbuffers::resolver_function_t resolver;
        auto* quantization = quantization_params->UnPack(&resolver);
        quantization_params_written = CreateQuantizationParameters(*fbb, quantization);
      }

      // Copy shape information
      auto *shape = tensor->shape();
      std::vector<int> shapeWritten;
      for (j = 0; j < shape->size(); j++)
        shapeWritten.push_back(shape->Get(j));

      printf("Create Tensor %d, %s \n", tensorsWritten.size(), tensor->name()->c_str());
      tensorsWritten.push_back(CreateTensor(*fbb, ExportVector<int32_t>(fbb, shapeWritten),
                               tensor->type(), buffer_index,
                               fbb->CreateString(tensor->name()),
                               quantization_params_written, tensor->is_variable()));

      // Record the tensor names
      tensornames_.push_back(tensor->name()->c_str());

      // Record the input and output tensor names for the whole model and the tidl supported subgraph
      if (inputFlag)
        inputs_.push_back(tensor->name()->c_str());
      if (customInputFlag)
        custom_op_inputs_.push_back(tensor->name()->c_str());
      if (outputFlag)
        outputs_.push_back(tensor->name()->c_str());
      if (customOutputFlag)
        custom_op_outputs_.push_back(tensor->name()->c_str());
    }
  }
  return fbb->template CreateVector<Offset<Tensor>>(tensorsWritten);
}

template <class T>
std::vector<int> FindTensorIndices(
    const T& input) {
  std::vector<int> output;
  output.reserve(input.size());

  for (auto x : input) {
    // Find the matching tensor name
    for (int i = 0; i < tensornames_.size(); i++)
    {
      if (tensornames_[i].compare(x) == 0) {
        output.push_back(i);
      }
    }
  }
  return output;
}

/* Export operators
 */
std::string outputBinFiles;
Offset<Vector<Offset<Operator>>> ExportOperators(FlatBufferBuilder* fbb)
{
  auto operators = (*tfliteModel_->subgraphs())[0]->operators();
  auto tensors  = (*tfliteModel_->subgraphs())[0]->tensors();

  std::vector<Offset<Operator>> operators_written;
  std::vector<int> operator_to_opcode;
  int op_index_written = 0;
  int i, j;

  // A single tidl-am5-custom-op for the subgraph which can be offloaded to tidl
  // plus tflite operators run by tflite runtime
  operator_to_opcode.resize(tfliteOps_.size()+1, -1);

  // The first operator is the tidl-am5-custom-op
  operator_to_opcode[op_index_written++] = GetOpCodeForCustom("tidl-am5-custom-op");

  // tflite operators run by tflite runtime
  for (i = tfliteOps_.size()-1; i >= 0; i--)
  {
    int op_index = tfliteOps_[i];
    const auto *op = operators->Get(op_index);
    auto operator_codes = (*tfliteModel_->operator_codes())[op->opcode_index()]->builtin_code();
    operator_to_opcode[op_index_written++] = GetOpCodeForBuiltin(operator_codes);
  }

  // Second pass serialize operators
  // Start with tidl-am5-custom-op
  op_index_written = 0;
  auto custom_options_format = CustomOptionsFormat_FLEXBUFFERS;
  Offset<Vector<uint8_t>> custom_options = 0;

  // The filenames of the two imported .bin files are stored as custom data
  // of the operator tidl-am5-custom-op
  int custom_initial_data_size = outputBinFiles.size();
  custom_options = fbb->CreateVector(
      reinterpret_cast<const uint8_t*>(outputBinFiles.c_str()),
      custom_initial_data_size);

  Offset<void> builtin_options_written = 0;
  BuiltinOptions builtin_options_type = BuiltinOptions_NONE;

  int opcode_index = operator_to_opcode[op_index_written++];

  // Find the input/output tensor indices for tidl-am5-custom-op
  std::vector<int> written_inputs  = FindTensorIndices(custom_op_inputs_);
  std::vector<int> written_outputs = FindTensorIndices(custom_op_outputs_);
  auto inputs  = ExportVector<int32_t>(fbb, written_inputs);
  auto outputs = ExportVector<int32_t>(fbb, written_outputs);

  operators_written.push_back(CreateOperator(*fbb, opcode_index, inputs, outputs,
                              builtin_options_type, builtin_options_written,
                              custom_options, custom_options_format));

  BuiltinOptionsUnion builtin_options_union;
  flatbuffers::resolver_function_t resolver;
  // Handle the tflite operators run by tflite runtime
  for (i = tfliteOps_.size()-1; i >= 0; i--)
  {
    int op_index = tfliteOps_[i];
    custom_options = 0;
    custom_options_format = CustomOptionsFormat_FLEXBUFFERS;

    const auto *op = operators->Get(op_index);
    // Get the op's builtin_options and builtin_options_type
    builtin_options_type = op->builtin_options_type();
    auto *builtin_options = op->builtin_options();

    // Create builtin_options_union to write the builtin_options back via the UnPack-Pack procedure
    builtin_options_union.type  = builtin_options_type;
    builtin_options_union.value = builtin_options_union.UnPack(builtin_options, builtin_options_type, &resolver);
    builtin_options_written = builtin_options_union.Pack(*fbb);

    int opcode_index = operator_to_opcode[op_index_written++];

    // Find the input/output tensor indices for the operator
    std::vector<std::string> op_inputs, op_outputs;
    for (j = 0; j < op->inputs()->size(); j++)
      op_inputs.push_back(tensors->Get(op->inputs()->Get(j))->name()->c_str());
    for (j = 0; j < op->outputs()->size(); j++)
      op_outputs.push_back(tensors->Get(op->outputs()->Get(j))->name()->c_str());

    std::vector<int> written_inputs  = FindTensorIndices(op_inputs);
    std::vector<int> written_outputs = FindTensorIndices(op_outputs);
    auto inputs = ExportVector<int32_t>(fbb, written_inputs);
    auto outputs = ExportVector<int32_t>(fbb, written_outputs);

    operators_written.push_back(CreateOperator(*fbb, opcode_index, inputs, outputs,
                                builtin_options_type, builtin_options_written,
                                custom_options, custom_options_format));
  }

  return fbb->template CreateVector<Offset<Operator>>(operators_written);
}

Offset<Vector<Offset<OperatorCode>>> CreateOpCodeTable(
    FlatBufferBuilder* fbb) {
  std::vector<Offset<OperatorCode>> codes;
  for (auto it : opcodes_) {
    const char* custom_name = it.custom.empty() ? nullptr : it.custom.c_str();
    codes.push_back(CreateOperatorCodeDirect(
        *fbb, static_cast<BuiltinOperator>(it.builtin), custom_name));
  }
  return fbb->template CreateVector<Offset<OperatorCode>>(codes);
}

/* The main function to serialize the TFLite model in FlatBuffers format
 * The TFLite subgraph (with a single input and single output for full offload)
 * is created, and then the TFLite model.
 */
int32_t GetBuffer(std::unique_ptr<uint8_t[]>* out, size_t* size)
{
  if (!out || !size) return TFLITE_WRITE_FAILURE;
  FlatBufferBuilder builder(/*initial_size=*/10240);

  std::vector<Offset<SubGraph>> subgraphs_as_vector;
  {  // subgraph specific stuff
    auto tensors = ExportTensors(&builder);

    std::vector<int> written_inputs = FindTensorIndices(inputs_);
    std::vector<int> written_outputs = FindTensorIndices(outputs_);
    auto inputs  = ExportVector<int32_t>(&builder, written_inputs);
    auto outputs = ExportVector<int32_t>(&builder, written_outputs);

    auto ops = ExportOperators(&builder);
    subgraphs_as_vector.push_back(
        CreateSubGraph(builder, tensors, inputs, outputs, ops, /* name */ 0));
  }
  Offset<Vector<Offset<Buffer>>> buffers = ExportBuffers(&builder);

  auto description = builder.CreateString("Exported from TIDL import tool");

  auto op_codes = CreateOpCodeTable(&builder);
  auto model = CreateModel(builder, TFLITE_SCHEMA_VERSION, op_codes,
                           builder.CreateVector(subgraphs_as_vector),
                           description, buffers);
  ::tflite::FinishModelBuffer(builder, model);
  const uint8_t* buffer = builder.GetBufferPointer();
  *size = builder.GetSize();
  (*out).reset(new uint8_t[*size]);
  memcpy(out->get(), buffer, *size);
  return TFLITE_WRITE_SUCCESS;
}

/* Write the TFLite model in FlatBuffers format into a file
 */
int32_t tidl_writeFBModel(const std::string& filename)
{
  std::unique_ptr<uint8_t[]> buffer;
  size_t size;
  GetBuffer(&buffer, &size);

  FILE* fp = fopen(filename.c_str(), "wb");
  if (!fp) return TFLITE_WRITE_FAILURE;

  if (fwrite(buffer.get(), 1, size, fp) != size) return TFLITE_WRITE_FAILURE;
  if (fclose(fp)) return TFLITE_WRITE_FAILURE;

  return TFLITE_WRITE_SUCCESS;
}

/* Main function for writing the TIDL converted TFLite model
 * Name the output file with appendix of _tidl_am5 to the input TFlite model.
 * Obtain the output .bin file names after the TIDL conversion, which will be
 * written as custom data for the tidl-am5-custom-op later.
 */
int32_t tfLite_write(tidl_import_config * params, const Model* tfliteModel, int32_t (&nodeEvaluated)[TIDL_NUM_MAX_LAYERS], int32_t (&nodeNotImported)[TIDL_NUM_MAX_LAYERS])
{
  int i;

  std::string outputTfLiteFile = (const char *)params->inputNetFile;
  std::string str1 = "_tidl_am5";
  std::string str2 = "; ";

  outputBinFiles   = (const char *)params->outputNetFile;
  std::string outputParamsFile = (const char *)params->outputParamsFile;

  outputBinFiles.insert(outputBinFiles.end(), str2.begin(), str2.end());
  outputBinFiles.insert(outputBinFiles.end(), outputParamsFile.begin(), outputParamsFile.end());

  outputTfLiteFile.insert(outputTfLiteFile.end()-7, str1.begin(), str1.end());

  tfliteModel_ = tfliteModel;

  i = 0;
  while (nodeEvaluated[i] >= 0)
    evaluatedOps_.push_back(nodeEvaluated[i++]);

  // If a node is not imported in the TIDL conversion, add it to the tfliteOps list.
  i = 0;
  while (nodeNotImported[i] >= 0)
    tfliteOps_.push_back(nodeNotImported[i++]);

  // Record the first node which is in the evaluated list, but not in the tfliteOps list
  // This node is the last node of the tidl supported subgraph.
  lastNodeTidlSubgraph = evaluatedOps_[tfliteOps_.size()];

  // Find nodes which are skipped in the import process, and add them
  // to the tfliteOps list.
  auto operators = (*tfliteModel_->subgraphs())[0]->operators();
  for (i = operators->size()-1; i >= 0; i--)
  {
    std::vector<int>::iterator itEvaluated;
    std::vector<int>::iterator itTflite;
    itEvaluated = std::find (evaluatedOps_.begin(), evaluatedOps_.end(), i);
    itTflite = std::find (tfliteOps_.begin(), tfliteOps_.end(), i);
    // If a node is not in the evaluated list, and it is also not in the tfliteOps list,
    // it has been skipped. Add the skipped node to the tfliteOps list with ordering.
    if ( (itEvaluated == evaluatedOps_.end()) && (itTflite == tfliteOps_.end()))
    {
      auto itOrdered = std::find (tfliteOps_.begin(), tfliteOps_.end(), i-1);
      tfliteOps_.insert(itOrdered, i);
    }
  }

  printf("Writing the TIDL converted TFLite Model (FlatBuffers) to File: %s  \n", outputTfLiteFile.c_str());

  if (tidl_writeFBModel(outputTfLiteFile) == -1)
  {
    printf("Failed in writing the TFLite model!!!");
    return TFLITE_WRITE_FAILURE;
  }
  return TFLITE_WRITE_SUCCESS;
}
