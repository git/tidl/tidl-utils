/*
 *
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
//#include <io.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <float.h>
#include <cmath>

#include "ti_dl.h"
#include "tensorflow/core/framework/graph.pb.h"
#include "tidl_import_config.h"

using namespace std;
using namespace tensorflow;
using ::google::protobuf::Message;
using ::google::protobuf::io::FileInputStream;
using ::google::protobuf::io::FileOutputStream;
using ::google::protobuf::io::ZeroCopyInputStream;
using ::google::protobuf::io::CodedInputStream;
using ::google::protobuf::io::ZeroCopyOutputStream;
using ::google::protobuf::io::CodedOutputStream;

#include "tidl_import_common.h" 

#define IS_SIGNED_DATA (1)
#define QUAN_STYLE2_ROUND (0.5)
extern sTIDL_OrgNetwork_t      orgTIDLNetStructure;
extern sTIDL_OrgNetwork_t      tempTIDLNetStructure;
extern sTIDL_Network_t         tIDLNetStructure;

#define ENABLE_BIN_PARSE_PRINT  (0)
int32_t gloab_data_format = TIDL_DATA_FORMAT_UNDEFINED;

extern void TIDL_importQuantWriteLayerParams2(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
                                              int32_t              numLayers,
                                              FILE                 *fp1);

uint32_t TIDL_kernelReshape(float * param, uint32_t w, uint32_t h, uint32_t ci, uint32_t co)
{
  uint32_t i0, i1, i2, i3;
  float * tPtr = (float * )my_malloc(w*h*ci*co*sizeof(float));
  
	for(i0 = 0; i0 < co; i0++)
	{
	  for(i1 = 0; i1 < ci; i1++)
	  {
	    for(i2 = 0; i2 < h; i2++)
	    {
	      for(i3 = 0; i3 < w; i3++)
		  {
		    tPtr[i0*ci*h*w + i1*h*w + i2*w + i3] = param[i2*w*ci*co + i3*ci*co + i1*co + i0];
		  }
		}
	  }
	}
  memcpy(param,tPtr,w*h*ci*co*sizeof(float));
  free(tPtr);
  return 0;
}
uint32_t TIDL_kernelScale(float * param, float * scale, uint32_t w, uint32_t h, uint32_t ci, uint32_t co)
{
  uint32_t i0, i1, i2, i3;
  for(i0 = 0; i0 < co; i0++)
  {
    for(i1 = 0; i1 < ci; i1++)
    {
      for(i2 = 0; i2 < h; i2++)
      {
        for(i3 = 0; i3 < w; i3++)
        {
          param[i2*w*ci*co + i3*ci*co + i1*co + i0] *= scale[i0];
        }
      }
    }
  }
  return 0;
}
uint32_t TIDL_depthWiseKernelScale(float * param, float * scale, uint32_t k, uint32_t c)
{
  uint32_t i0, i1;
  for(i0 = 0; i0 < c; i0++)
  {
    for(i1 = 0; i1 < k; i1++)
    {
      param[i1*c + i0] *= scale[i0];
    }
  }
  return 0;
}

/*
 * Search the whole graph for const tensor with name "tensorName"
*/
TensorProto TIDL_getConstTensor(GraphDef& tfGraphDef, const string tensorName)
{
  int i;

  for (i = 0; i < tfGraphDef.node_size(); i++) 
  {
    // Find the node with name "tensorName"
    if((strcmp(tfGraphDef.node(i).name().c_str(), tensorName.c_str()) == 0))
    {
      // Return tensor if this node is a "Const" operator
      if((strcmp(tfGraphDef.node(i).op().c_str(), "Const") == 0))
      {
        if(tfGraphDef.node(i).attr().at(std::string("value")).has_tensor())
        {
          //printf("Const Tensor found.\n");
          auto & tensor = tfGraphDef.node(i).attr().at(std::string("value")).tensor();
          return(tensor);
        }
      }
      // Find the tensor for this node's input if this node is a "Identity" operator
      else if((strcmp(tfGraphDef.node(i).op().c_str(), "Identity") == 0))
      {
        //printf("Identity Tensor found.\n");
        return(TIDL_getConstTensor(tfGraphDef, tfGraphDef.node(i).input(0)));
      }
    }
  }

  return (tensorflow::TensorProto::default_instance());
}


int32_t TIDL_hasAttr(const NodeDef& node, char const * name)
{
  for (auto&& it = node.attr().begin(); it != node.attr().end();) 
  {
    auto& map = it->first;
    if(strcmp(map.c_str(), name) == 0)
    {
      return (1);
    }
    it++;
  }
  return (0);
}

int32_t TIDL_getAttr_type(const NodeDef& node, char * name, int32_t * type)
{
  if(TIDL_hasAttr(node,name))
  {
    auto& value = node.attr().at(std::string(name));
    if(value.type() == DT_UINT8)
    {
      *type = TIDL_UnsignedChar;
    }
    else if(value.type() == DT_INT8)
    { 
      *type = TIDL_SignedChar;
    }
    *type = TIDL_UnsignedChar;
    return (1);
  }
  return (0);
}

/*
*  Find padding of the node. Supported TensorFlow padding types are:
*     - SAME:  size of output feature-maps is the "same" as input.
*     - VALID: no padding. Only "valid" input data is used.
*/
int32_t TIDL_getAttr_padding(const NodeDef& node, char const * name, int32_t * padType)
{
  if(TIDL_hasAttr(node,name))
  {
    auto& value = node.attr().at(std::string(name));
    if(strcmp(value.s().c_str(),"SAME") == 0)
    {
      *padType = TIDL_PADDING_TYPE_SAME;
    }
    else if(strcmp(value.s().c_str(),"VALID") == 0)
    {
      *padType = TIDL_PADDING_TYPE_VALID;
    }
    else
    {
      *padType = TIDL_PADDING_TYPE_UNSUPPORTED;
      printf("TIDL limitation: Unsupported Padding type \n");
    }
    return (1);
  }
  return (0);
}

/*
*  Find data format of the node: NHWC or NCHW
*/
int32_t TIDL_getAttr_data_format(const NodeDef& node, char const * name)
{
  if(TIDL_hasAttr(node,name))
  {
    auto& value = node.attr().at(std::string(name));
    if(strcmp(value.s().c_str(),"NHWC") == 0)
    {
      if(gloab_data_format == TIDL_DATA_FORMAT_UNDEFINED)
      {
        // Set data format when it is found the first time 
        gloab_data_format = TIDL_DATA_FORMAT_NHWC;
      }
      // Check if data format is consisten when it is found again subsequently
      else if(gloab_data_format != TIDL_DATA_FORMAT_NHWC)
      {
        printf("\n ERROR: data_format is not common accross all the layers \n");
      }
    }
    else if(strcmp(value.s().c_str(),"NCHW") == 0)
    {
      if(gloab_data_format == TIDL_DATA_FORMAT_UNDEFINED)
      {
        gloab_data_format = TIDL_DATA_FORMAT_NCHW;
      }
      else if(gloab_data_format != TIDL_DATA_FORMAT_NCHW)
      {
        printf("\n ERROR: data_format is not common accross all the layers \n");
      }
    }
    else
    {
      printf("TIDL limitation: Unsupported data format \n");
    }        
    return (1);
  }
  return (0);
}

int32_t TIDL_getAttr_value(const NodeDef& node, char const * name, int32_t * valuePtr, int32_t idx)
{
  if(TIDL_hasAttr(node,name))
  {
    auto& value = node.attr().at(std::string(name));
    *valuePtr  = (int32_t)value.list().i(idx);

    return (1);
  }
  return (0);
}

int32_t TIDL_getAttr_float(const NodeDef& node, char const * name, float * valuePtr, int32_t idx)
{
  if (TIDL_hasAttr(node, name))
  {
    auto& value = node.attr().at(std::string(name));
    if (value.has_list())
    {
      *valuePtr = value.list().f(idx);
    }
    else if(idx == 0)
    {
      *valuePtr = value.f();
    }
    else
    {
      return (0);
    }
    return (1);
  }
  return (0);
}

int32_t TIDL_tfCopyInputConstTensor(GraphDef& tfGraphDef, int32_t nIdx, int32_t inIdx, sBufferPc_t &buf)
{
  TensorProto tensor = TIDL_getConstTensor(tfGraphDef, tfGraphDef.node(nIdx).input(inIdx));
  float * tPtr = (float *)tensor.tensor_content().c_str();
  buf.bufSize = tensor.tensor_content().size() / sizeof(float);
  buf.ptr = (float *)my_malloc(buf.bufSize*sizeof(float));
  if (tensor.dtype() == 1) //DT_FLOAT
  {
    memcpy(buf.ptr, tPtr, sizeof(float)*buf.bufSize);
    return TIDL_IMPORT_NO_ERR;
  }
  else
  {
    printf("TIDL limitation: Only float Mul tensor is suported. \n");
    return TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED;
  }
}

int32_t TIDL_tfMapPlaceHolderParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_DataLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = -1;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;

  return TIDL_IMPORT_NO_ERR;
}

/*==============================================================================
* Map convolution layer parameters
* Reference: https://www.tensorflow.org/api_docs/cc/class/tensorflow/ops/conv2-d
==============================================================================*/
int32_t TIDL_tfMapConvParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              nodeIndex,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&            tfGraphDef)
{
  int32_t status;
  int32_t padType;
  int32_t idx1, idx2;
  sTIDL_LayerPC_t &TIDLPCLayers  = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams = TIDLPCLayers.layerParams.convParams;

  TIDLPCLayers.layerType = TIDL_ConvolutionLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  TIDL_IMPORT_DBG_PRINT("Mapping conv layer parameters... \n");

  TIDL_IMPORT_DBG_PRINT2("Find input tensor %s\n", tfGraphDef.node(nodeIndex).input(1).c_str());
  TensorProto tensor         = TIDL_getConstTensor(tfGraphDef, tfGraphDef.node(nodeIndex).input(1));
  convParams.numInChannels   = tensor.tensor_shape().dim(2).size();
  convParams.numOutChannels  = tensor.tensor_shape().dim(3).size();
  convParams.kernelW         = tensor.tensor_shape().dim(0).size();
  convParams.kernelH         = tensor.tensor_shape().dim(1).size();
 
  convParams.numGroups       = 1;
  convParams.dilationW       = 1;
  convParams.dilationH       = 1;
  convParams.strideW         = 1;
  convParams.strideH         = 1;
  convParams.padW            = 0;
  convParams.padH            = 0;
  convParams.enableBias      = 0;
  convParams.enableRelU      = 0;
  convParams.enablePooling   = 0;

  TIDL_getAttr_data_format(tfGraphDef.node(nodeIndex), "data_format");
  if (gloab_data_format == TIDL_DATA_FORMAT_NCHW)
  { // "NCHW" format
    TIDL_IMPORT_DBG_PRINT("Conv data format is NCHW.\n");
    idx1 = 3;    // width
    idx2 = 2;    // hight
  }
  else
  { // "NHWC" format
    TIDL_IMPORT_DBG_PRINT("Conv data format is NHWC.\n");
    idx1 = 2;    // width
    idx2 = 1;    // hight
  }

  TIDL_getAttr_value(tfGraphDef.node(nodeIndex), "strides", &convParams.strideW, idx1);
  TIDL_getAttr_value(tfGraphDef.node(nodeIndex), "strides", &convParams.strideH, idx2);
  TIDL_getAttr_value(tfGraphDef.node(nodeIndex), "dilations", &convParams.dilationW, idx1);
  TIDL_getAttr_value(tfGraphDef.node(nodeIndex), "dilations", &convParams.dilationH, idx2);

  status = TIDL_IMPORT_NO_ERR;
  TIDL_getAttr_padding(tfGraphDef.node(nodeIndex), "padding", &padType);
  if (padType == 0)
  { // padding sizes are such that input and output sizes are equal ("same")
    convParams.padW = ((convParams.kernelW - 1)*convParams.dilationW) / 2;
    convParams.padH = ((convParams.kernelH - 1)*convParams.dilationH) / 2;
  }

  // Following is only for regular convolution layer ("Conv2D") 
  if (strcmp(tfGraphDef.node(nodeIndex).op().c_str(), "Conv2D") == 0)
  {
    // Read in the kernel weights
    status = TIDL_tfCopyInputConstTensor(tfGraphDef, nodeIndex, 1, TIDLPCLayers.weights);
    // Reshape kernel from TensorFlow shape to TIDL shape
    TIDL_kernelReshape((float *)TIDLPCLayers.weights.ptr, convParams.kernelW, 
                       convParams.kernelH, convParams.numInChannels, convParams.numOutChannels);
  }

  return status;
} /* TIDL_tfMapConvParams */

/*==============================================================================
* Map depthwise convolution layer parameters
==============================================================================*/
int32_t TIDL_tfMapDWConvParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              nodeIndex,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  int32_t status;

  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams = TIDLPCLayers.layerParams.convParams;

  // Obtain convolution parameters
  TIDL_tfMapConvParams(pOrgTIDLNetStructure, nodeIndex, layerIndex, dataIndex, tfGraphDef);
  if (convParams.numOutChannels != 1)
  {
    printf("TIDL limitation: DW Convolution with Depth multiplier > 1 is not suported now.\n");
    return TIDL_IMPORT_ERR_DWCONV_DEPTH_MULT_INVALID;
  }

  // Make it depthwise convolution
  convParams.numGroups = convParams.numOutChannels = convParams.numInChannels;

  // Obtain kernel weights and reshape them to fit TIDL implementation
  status = TIDL_tfCopyInputConstTensor(tfGraphDef, nodeIndex, 1, TIDLPCLayers.weights);
  TIDL_kernelReshape((float *)TIDLPCLayers.weights.ptr, convParams.kernelW, convParams.kernelH,
                     convParams.numInChannels/convParams.numGroups, convParams.numOutChannels);

  return status;
}

int32_t TIDL_tfMapBiasAddParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              nodeIndex,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  int32_t status;

  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_BiasLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;

  // Obtain bias parameters
  status = TIDL_tfCopyInputConstTensor(tfGraphDef, nodeIndex, 1,
                                       pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias);

  return status;
}

int32_t TIDL_tfMapAddParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_EltWiseLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.eltWiseParams.eltWiseType = TIDL_EltWiseSum;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.eltWiseParams.numInData = 2;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = tfGraphDef.node(i).input_size();

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapMulParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ScaleLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapMatMulParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  sTIDL_InnerProductParams_t &innerProductParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.innerProductParams;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
 
  TIDLPCLayers.layerType = TIDL_InnerProductLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
 
  TensorProto tensor         = TIDL_getConstTensor(tfGraphDef, tfGraphDef.node(i).input(1));
 
  innerProductParams.numInNodes   = tensor.tensor_shape().dim(0).size();
  innerProductParams.numOutNodes = tensor.tensor_shape().dim(1).size();
 
  TIDL_tfCopyInputConstTensor(tfGraphDef, i, 1,TIDLPCLayers.weights);
 
  /* Set default bias as zero, if next layer has bias it will get merged and this buffer will be used */
  TIDLPCLayers.bias.bufSize = innerProductParams.numOutNodes;
  TIDLPCLayers.bias.ptr = (float *)my_malloc(TIDLPCLayers.bias.bufSize * sizeof(float));
  memset(TIDLPCLayers.bias.ptr, 0, TIDLPCLayers.bias.bufSize);
 
  return 0;
}

void TIDL_tfBNToScaleBias(
  float    * scale,
  float    * bias,
  uint32_t  numCh,
  float * mean,
  float * var,
  float * gamma,
  float * beta,
  float eps
  )

{
  uint32_t j;
  for (j = 0; j < numCh; j++)
  {
    double m = mean[j];
    double v = var[j];
    double s = gamma[j];
    double b = beta[j];
    double inv_var = pow((eps + v), -0.5);
    scale[j] = (s)*inv_var;
    bias[j]  = (((-m)*s)*inv_var) + b;
  }
}

/*==============================================================================
* Function purpose: map parameters for fused batch norm layer
* Reference: https://www.tensorflow.org/api_docs/cc/class/tensorflow/ops/fused-batch-norm
==============================================================================*/
int32_t TIDL_tfMapFusedBnParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              nodeIndex,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  sBufferPc_t gamma;
  sBufferPc_t beta;
  sBufferPc_t mean;
  sBufferPc_t variance;
  float epsilon;

  int32_t dataSize, status;

  status = TIDL_IMPORT_NO_ERR;

  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_BatchNormLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  
  TIDL_getAttr_float(tfGraphDef.node(nodeIndex), "epsilon", &epsilon, 0);
  TIDL_tfCopyInputConstTensor(tfGraphDef, nodeIndex, 1, gamma);
  TIDL_tfCopyInputConstTensor(tfGraphDef, nodeIndex, 2, beta);
  TIDL_tfCopyInputConstTensor(tfGraphDef, nodeIndex, 3, mean);
  TIDL_tfCopyInputConstTensor(tfGraphDef, nodeIndex, 4, variance);

  dataSize = gamma.bufSize;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.ptr     = my_malloc(dataSize*sizeof(float));
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.bufSize = dataSize;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.ptr        = my_malloc(dataSize*sizeof(float));
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.bufSize    = dataSize;
  
  TIDL_tfBNToScaleBias((float *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.ptr,
                       (float *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.ptr, dataSize,
                       (float *)mean.ptr, (float *)variance.ptr, (float *)gamma.ptr, (float *)beta.ptr, epsilon
                      );
  my_free(mean.ptr);
  my_free(variance.ptr);
  my_free(gamma.ptr);
  my_free(beta.ptr);

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapReluParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ReLULayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapRelu6Params(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  TIDL_tfMapReluParams(pOrgTIDLNetStructure, i, layerIndex, dataIndex, tfGraphDef);
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.reluParams.reluType = TIDL_RelU6;

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapMaxPoolParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  int32_t status;
  int32_t padType;
  int32_t idx1, idx2;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_PoolingParams_t &poolParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams;

  TIDLPCLayers.layerType = TIDL_PoolingLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  poolParams.poolingType = TIDL_MaxPooling;
  TIDL_getAttr_data_format(tfGraphDef.node(i), "data_format");
  if (gloab_data_format == TIDL_DATA_FORMAT_NCHW)
  {
    idx1 = 3;
    idx2 = 2;
  }
  else
  {
    idx1 = 2;
    idx2 = 1;
  }

  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;
  TIDL_getAttr_value(tfGraphDef.node(i), "strides", &poolParams.strideW, idx1);
  TIDL_getAttr_value(tfGraphDef.node(i), "strides", &poolParams.strideH, idx2);
  TIDL_getAttr_value(tfGraphDef.node(i), "ksize",   &poolParams.kernelW, idx1);
  TIDL_getAttr_value(tfGraphDef.node(i), "ksize",   &poolParams.kernelH, idx2);

  TIDL_getAttr_padding(tfGraphDef.node(i), "padding", &padType);
  if (padType == TIDL_PADDING_TYPE_SAME)
  { // padding sizes are such that input and output sizes are equal ("same")
    poolParams.padW = ((poolParams.kernelW - 1)) / 2;
    poolParams.padH = ((poolParams.kernelH - 1)) / 2;
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapAvgPoolParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{

  TIDL_tfMapMaxPoolParams(pOrgTIDLNetStructure, i, layerIndex, dataIndex, tfGraphDef);
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams.poolingType = TIDL_AveragePooling;

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapMeanParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  int32_t status;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_PoolingParams_t &poolParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams;

  TIDLPCLayers.layerType = TIDL_PoolingLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  poolParams.poolingType = TIDL_AveragePooling;
  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;
  poolParams.kernelW = 0;
  poolParams.kernelH = 0;

  return TIDL_IMPORT_NO_ERR;
}


int32_t TIDL_tfMapConcatV2Params(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  int32_t idx;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ConcatLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  if (TIDL_hasAttr(tfGraphDef.node(i), "N"))
  {
    auto& value = tfGraphDef.node(i).attr().at(std::string("N"));
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = value.i();
  }
  else
  {
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = 2;
  }
  TensorProto tensor = TIDL_getConstTensor(tfGraphDef, tfGraphDef.node(i).input(pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs));
  auto& axis = tensor.int_val();
  if (gloab_data_format == TIDL_DATA_FORMAT_NHWC)
  {
    idx = 3;
  }
  else
  {
    idx = 1;
  }
  if (axis.Get(0) != idx)
  {
    printf("TIDL limitation: Concat is Only suported accross channels.\n");
    return TIDL_IMPORT_ERR_CONCAT_INVALID_DIM;
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapPadParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  int32_t idx,j;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_PadLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;

  TensorProto tensor = TIDL_getConstTensor(tfGraphDef, tfGraphDef.node(i).input(1));
  auto& padTensor = tensor.int_val();

  int32_t * tPtr = (int32_t *)tensor.tensor_content().c_str();
  memcpy((void*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerPCParams.padParams.padTensor, tPtr, tensor.tensor_content().size());

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapSliceParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  int32_t j, sliceNumChs[TIDL_NUM_OUT_BUFS];
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_SliceLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  
  if (TIDL_hasAttr(tfGraphDef.node(i), "num_slice"))
  {
    auto& value = tfGraphDef.node(i).attr().at(std::string("num_slice"));

    if (value.has_list())
    {
      pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs = value.list().i_size();
      for (j = 0; j < value.list().i_size(); j++)
      {
        sliceNumChs[j] = value.list().i(j);
      }
    }
    else
    {
      pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs = value.i();
      for (j = 0; j < value.i(); j++)
      {
        sliceNumChs[j] =
          pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].inData[0].dimValues[1] / value.i();;
      }
    }
  }
  else
  {
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs = 2;
    sliceNumChs[0] = ((pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].inData[0].dimValues[1] + 1) / 2);
    sliceNumChs[1] = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].inData[0].dimValues[1] - sliceNumChs[0];
  }
  for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs; j++)
  {
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerPCParams.sliceParams.sliceNumChs[j] = sliceNumChs[j];
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapSqueezeParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_SqueezeLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapReshapeParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ReshapeLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapSoftmaxParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_SoftMaxLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapShapeParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ShapeLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfMapUnSuportedlayerParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphDef&             tfGraphDef)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_UnSuportedLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

typedef struct {
  int8_t name[TIDL_STRING_SIZE];
  int32_t(*tidl_tfMapFunc)(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
    int32_t              i,
    int32_t              layerIndex,
    int32_t              *dataIndex,
    GraphDef&             tfGraphDef);
}sTIDL_tfOpParamMap_t;

#define TIDL_NUM_SUPPORTED_TF_OPERATORS 20
sTIDL_tfOpParamMap_t tidl_TfOpParamMapTable[TIDL_NUM_SUPPORTED_TF_OPERATORS] = 
{
  { "Placeholder",                     TIDL_tfMapPlaceHolderParams },//  TIDL_DataLayer,
  { "Conv2D",                          TIDL_tfMapConvParams },       //  TIDL_ConvolutionLayer ,
  { "DepthwiseConv2dNative",           TIDL_tfMapDWConvParams },     //  TIDL_ConvolutionLayer ,
  { "BiasAdd",                         TIDL_tfMapBiasAddParams },    //  TIDL_BiasLayer ,
  { "Add",                             TIDL_tfMapAddParams },        //  TIDL_EltWiseLayer ,
  { "Mul",                             TIDL_tfMapMulParams },        //  TIDL_ScaleLayer ,
  { "MatMul",                          TIDL_tfMapMatMulParams },     //  TIDL_InnerProductLayer ,  
  { "FusedBatchNorm",                  TIDL_tfMapFusedBnParams },    //  TIDL_BatchNormLayer ,
  { "Relu",                            TIDL_tfMapReluParams },       //  TIDL_ReLULayer ,
  { "Relu6",                           TIDL_tfMapRelu6Params },      //  TIDL_ReLULayer ,
  { "MaxPool",                         TIDL_tfMapMaxPoolParams },    //  TIDL_PoolingLayer ,
  { "AvgPool",                         TIDL_tfMapAvgPoolParams },    //  TIDL_PoolingLayer ,
  { "ConcatV2",                        TIDL_tfMapConcatV2Params },   //  TIDL_ConcatLayer ,
  { "Slice",                           TIDL_tfMapSliceParams },      //  TIDL_SliceLayer ,
  /* TIDL_SqueezeLayer and TIDL_ReshapeLayer (reshape following squeeze) are mapped to TIDL_FlattenLayer */
  { "Squeeze",                         TIDL_tfMapSqueezeParams },    //  TIDL_SqueezeLayer , 
  { "Reshape",                         TIDL_tfMapReshapeParams },    //  TIDL_ReshapeLayer ,
  { "Softmax",                         TIDL_tfMapSoftmaxParams },    //  TIDL_SoftMaxLayer ,
  { "Pad",                             TIDL_tfMapPadParams },        //  TIDL_PadLayer ,
  { "Mean",                            TIDL_tfMapMeanParams },       //  TIDL_PoolingLayer ,
  { "Shape",                           TIDL_tfMapShapeParams }       //  TIDL_ShapeLayer ,
};

sTIDL_tfOutRehapeMap_t sTIDL_tfOutRehapeTable[] =
{
  { TIDL_DataLayer                     ,  TIDL_tfOutReshapeDataLayer },
  { TIDL_ConvolutionLayer              ,  TIDL_tfOutReshapeConvLayer },
  { TIDL_PoolingLayer                  ,  TIDL_tfOutReshapePoolingLayer },
  { TIDL_ReLULayer                     ,  TIDL_tfOutReshapeRelu },
  { TIDL_PReLULayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_EltWiseLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_InnerProductLayer             ,  TIDL_tfOutReshapeIPLayer },
  { TIDL_SoftMaxLayer                  ,  TIDL_tfOutReshapeSoftmax },
  { TIDL_BatchNormLayer                ,  TIDL_tfOutReshapeBN },
  { TIDL_BiasLayer                     ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ScaleLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_Deconv2DLayer                 ,  TIDL_tfOutReshapeDeConvLayer },
  { TIDL_ConcatLayer                   ,  TIDL_tfOutReshapeConcatLayer },
  { TIDL_SplitLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_SliceLayer                    ,  TIDL_tfOutReshapeSliceLayre },
  { TIDL_CropLayer                     ,  TIDL_tfOutReshapeCropLayer },
  { TIDL_FlattenLayer                  ,  TIDL_tfOutReshapeFlattenLayer },
  { TIDL_DropOutLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ArgMaxLayer                   ,  TIDL_tfOutReshapeArgmaxLayer },
  { TIDL_DetectionOutputLayer          ,  TIDL_tfOutReshapeDetOutLayer },
  { TIDL_UnSuportedLayer               ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ConstDataLayer                ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ShuffleChannelLayer           ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ResizeLayer                   ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PriorBoxLayer                 ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PermuteLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ReshapeLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ShapeLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_SqueezeLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PadLayer                      ,  TIDL_tfOutReshapePadLayer },
  { TIDL_TransposeLayer                ,  TIDL_tfOutReshapeIdentity }
};


int32_t TIDL_getTfOpParamMapId(const char  * name)
{
  int32_t i = -1;
  for (i = 0; i < sizeof(tidl_TfOpParamMapTable) / sizeof(sTIDL_tfOpParamMap_t); i++)
  {
    if ((strcmp(name, (const char *)tidl_TfOpParamMapTable[i].name) == 0))
    {
      return (i);
    }
  }
  return (-1);
}

TIDL_TFLayerMapping_t TIDL_TFLayerMap[] =
{
  /* TIDL_SqueezeLayer and TIDL_ReshapeLayer (reshape following squeeze) are mapped to TIDL_FlattenLayer */
  { (char*)"TIDL_TFSlimFlatten",        (char*)"TIDL_SqueezeLayerTIDL_ReshapeLayer"   , 2 },
  { (char*)"TIDL_TFSlimShuffle",        (char*)"ResahpeSqueeze"              , 3 }
};

int32_t tidl_FindFlattenLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (tidl_isLayerType("TIDL_TFSlimFlatten", i1, pOrgTIDLNetStructure, TIDL_TFLayerMap, (sizeof(TIDL_TFLayerMap) / sizeof(TIDL_TFLayerMapping_t))))
    {
      int32_t mapIdx = tidl_getLayerTypeMapIdx("TIDL_TFSlimFlatten", TIDL_TFLayerMap, (sizeof(TIDL_TFLayerMap) / sizeof(TIDL_TFLayerMapping_t)));
      pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType = TIDL_FlattenLayer;
      pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1 + TIDL_TFLayerMap[mapIdx].NumOps - 1].outData[0];
      strcpy((char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0] , (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1 + TIDL_TFLayerMap[mapIdx].NumOps - 1].outDataNames[0]);
      pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1 + TIDL_TFLayerMap[mapIdx].NumOps - 1].outConsumerCnt[0];
      for (i2 = 0; i2 < (TIDL_TFLayerMap[mapIdx].NumOps - 1); i2++)
      {
        pOrgTIDLNetStructure.TIDLPCLayers[i1 + i2 + 1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1 + i2 + 1].numOutBufs = -1;
      }
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[i1];

      TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
      TIDLPCLayers.outData[0].dimValues[1] = 1;
      TIDLPCLayers.outData[0].dimValues[2] = 1;
      TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[1]* 
                                             TIDLPCLayers.inData[0].dimValues[2] *
                                             TIDLPCLayers.inData[0].dimValues[3];

      int32_t  idx = tidl_getOutLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0].dataId);
      if (idx == -1)
      {
        printf("Error in finding flatten layer: output layer cannot be found!\n");
        return TIDL_IMPORT_ERR_OUTPUT_LAYER_NOT_FOUND;
      }
      sTIDL_LayerPC_t &TIDLPCLayersout = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      TIDLPCLayersout.inData[0] = TIDLPCLayers.outData[0];
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_tfLayerFillTensorNames(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              nodeIndex,
  int32_t              layerIndex,
  GraphDef&             tfGraphDef)
{
  int32_t j;
  sTIDL_LayerPC_t *pLayer;

  pLayer = &pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  strcpy((char*)pLayer->name, tfGraphDef.node(nodeIndex).name().c_str());
  TIDL_IMPORT_DBG_PRINT2("Node name: %s\n", (char*)pLayer->name);

  if (pLayer->numInBufs > 0)
  {
    for (j = 0; j < pLayer->numInBufs; j++)
    {
      strcpy((char*)pLayer->inDataNames[j], tfGraphDef.node(nodeIndex).input(j).c_str());
      TIDL_IMPORT_DBG_PRINT2("Input name: %s\n", (char*)pLayer->inDataNames[j]);
    }
  }
  if (pLayer->numOutBufs > 0)
  {
    strcpy((char*)pLayer->outDataNames[0], tfGraphDef.node(nodeIndex).name().c_str());
    TIDL_IMPORT_DBG_PRINT2("Output name: %s\n", (char*)pLayer->outDataNames[0]);

    pLayer->outConsumerLinked[0] = 0;
    for (j = 1; j < pLayer->numOutBufs; j++)
    {
      char numberStr[10];
      strcpy((char*)pLayer->outDataNames[j], tfGraphDef.node(nodeIndex).name().c_str());
      strcat((char*)pLayer->outDataNames[j], "_");
      sprintf(numberStr, "%d",j);
      strcat((char*)pLayer->outDataNames[j], numberStr);
      pLayer->outConsumerLinked[j] = 0;
      TIDL_IMPORT_DBG_PRINT2("Output name: %s\n", (char*)pLayer->outDataNames[j]);
    }
  }
  pLayer->weightsElementSizeInBits = NUM_WHGT_BITS;
  pLayer->strideOffsetMethod = TIDL_strideOffsetCenter;

  return 0;
}

/*==============================================================================
* Function purpose: for a given layer, search the whole graph to find the number 
* of consumers. 
*    - A consumer is a layer which has one of the given layer's outputs as input. 
==============================================================================*/
int32_t tidl_tfLayerUpdateConsumerCount(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,          // not used, to be removed
  int32_t              layerIndex, // points to the layer to find consumers for
  GraphDef&            tfGraphDef)
{
  int32_t i0, i1, i2;
  int32_t numCons = 0;

  // search consumers for each output of the given layer
  for (i0 = 0; i0 < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs; i0++)
  { 
    // scan the whole graph to find number of consumers
    for (i1 = 0; i1 < tfGraphDef.node_size(); i1++)
    {
      // for each node, see if it has an input that is an output of the given layer
      for (i2 = 0; i2 < tfGraphDef.node(i1).input_size(); i2++)
      {
        if (strcmp((const char *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outDataNames[i0], 
                   tfGraphDef.node(i1).input(i2).c_str()) == 0)
        {
          numCons++;
        }
      }
    }

    // update the number of consumers for each output
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerCnt[i0] = numCons;
    TIDL_IMPORT_DBG_PRINT2("Number of consumers: %d\n", numCons);
  }

  return 0;
} /* tidl_tfLayerUpdateConsumerCount */


int32_t tf_import(tidl_import_config * params)
{
  int32_t                    i,j;
  int32_t                    layerNum;
  int32_t                    inputSize;
  int32_t                    pad,stride;
  int32_t                    layerIndex;
  int32_t                    tiLayerIndex;
  int32_t                    dataIndex;
  const uint8_t             *name;
  const uint8_t             *inputName[10];
  const uint8_t             *outputName;
  GraphDef                   tfGraphDef;
  int32_t status;
  int32_t                    dataSize;
  int32_t                    id, numUnsupportedLayers, importStatus, numErrs;
  int paramSet  = 0;
  int conv2DRandParams = 0;
  string attrKey;
  int32_t inLayerId = 0;
  int32_t weightsElementSizeInBits;
  int32_t mapTblIdx = -1;
  FILE    *fpNetFile;
  FILE    *fpParamsFile;

  string key = "value";

  printf("TF Model (Proto) File  : %s  \n",(const char *)params->inputNetFile);
  printf("TIDL Network File      : %s  \n", (const char *)params->outputNetFile);
  printf("TIDL Params File       : %s  \n\n", (const char *)params->outputParamsFile);

  TIDL_readProtoFromBinaryFile((const char *)params->inputNetFile, &tfGraphDef);
  gloab_data_format = TIDL_DATA_FORMAT_NHWC;
  layerIndex = 0;
  dataIndex  = 0;

  //printf("TF graph node size: %d\n", tfGraphDef.node_size()); 

  numErrs = 0;
  numUnsupportedLayers = 0;
  for (i = 0; i < tfGraphDef.node_size(); i++)
  {
#ifdef TIDL_IMPORT_ENABLE_DBG_PRINT
    printf("Parsing node %4d:, %s, %s \n", i, tfGraphDef.node(i).op().c_str(), tfGraphDef.node(i).name().c_str());
#endif
    // skip "Const" nodes because they are params and will be read by tidl_tfMapFunc()
    if (strcmp(tfGraphDef.node(i).op().c_str(), "Const") == 0)
    {
      continue;
    }

    /* Set default values of numInBufs and numOutBufs which may be changed by
       tidl_tfMapFunc below for certain layers. */
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs =  1;
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs = 1;

    /* Map each supported TF operator to a TIDL layer */
    mapTblIdx = TIDL_getTfOpParamMapId(tfGraphDef.node(i).op().c_str());
    if (mapTblIdx == -1)
    {
      printf("TIDL limitation: TensorFlow operator %s is not suported.\n", tfGraphDef.node(i).op().c_str());
      TIDL_tfMapUnSuportedlayerParams(&orgTIDLNetStructure, i, layerIndex, &dataIndex, tfGraphDef);
      numUnsupportedLayers++;
    }
    else
    {
      importStatus = tidl_TfOpParamMapTable[mapTblIdx].tidl_tfMapFunc(&orgTIDLNetStructure, i, layerIndex, &dataIndex, tfGraphDef);
      if(importStatus != TIDL_IMPORT_NO_ERR)
      {
        printf("TIDL limitation: TensorFlow operator %s cannot be mapped to a TIDL layer.\n", tfGraphDef.node(i).op().c_str());
        numUnsupportedLayers++;
      }
    }

    tidl_tfLayerFillTensorNames(&orgTIDLNetStructure, i, layerIndex, tfGraphDef);
    tidl_tfLayerUpdateConsumerCount(&orgTIDLNetStructure, i, layerIndex, tfGraphDef);
    tidl_linkInputTensors(&orgTIDLNetStructure, layerIndex);
    tidl_linkOutputTensors(&orgTIDLNetStructure, layerIndex);
    layerIndex++;
  }

  if(numUnsupportedLayers > 0)
  {
    printf("\nImport error: This TensorFlow model has unsupported operators. "
             "Please check TIDL User's Guide for supported operators.\n");
    numErrs++;
  }

  importStatus = tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\nImport error: This TensorFlow model's topology is not supported. "
           "Please check TIDL User's Guide for supported network topologies.\n");
    numErrs++;
  }

  tidl_fillInDataLayerShape(orgTIDLNetStructure,  params, layerIndex);
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);
  tidl_updateOutDataShape(orgTIDLNetStructure, 0, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_tfOutRehapeTable);

  importStatus = tidl_mergeBiasLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Bias layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeBNLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Batch Norm layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeReluLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Relu layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergePadLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Pad layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergePoolLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Pad layer cannot be merged into other layers.\n");
    numErrs++;
  }
  
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);
  tidl_FindFlattenLayer(orgTIDLNetStructure, layerIndex);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);

  importStatus = tidl_convertConv2DToIpLayer(orgTIDLNetStructure, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_tfOutRehapeTable);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Conv2D layer cannot be converted into Inner Product layer.\n");
    numErrs++;
  }

  importStatus = tidl_mergeFlattenLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Flatten layer cannot be merged.\n");
    numErrs++;
  }

  importStatus = tidl_mergeReshapeLayer(orgTIDLNetStructure, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_tfOutRehapeTable);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\nImport error: Reshape layer cannot be merged.\n");
    numErrs++;
  }

  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);

  tidl_importEltWiseParams(&orgTIDLNetStructure, layerIndex);

  /* Quantize and write out layer params */
  fpParamsFile = fopen((const char *)params->outputParamsFile, "wb+");
  TIDL_importQuantWriteLayerParams(&orgTIDLNetStructure, layerIndex, fpParamsFile);

  tiLayerIndex = tidl_copyPCNetToDeviceNet(orgTIDLNetStructure, tIDLNetStructure, layerIndex, NUM_WHGT_BITS);

  /* Have a final check if there are any layers which
     - are supported only by merging with other TIDL layers,
     - but are not able to be merged. */
  numUnsupportedLayers = tidl_countUnsupportedLayers(&tIDLNetStructure, tiLayerIndex);
  if(numUnsupportedLayers > 0)
  {
    printf("\nImport error: This TensorFlow model has operators that are supported"
           " by TIDL only if they can be merged with TIDL layers. But these operators"
           " cannot be merged with any TIDL layer.\n"
           "Please check TIDL User's Guide for supported TensorFlow operators.\n");
    numErrs++;
  }

  if(numErrs > 0)
  {
    /* Stop the import process here to prevent potential crash if continuing */
    return TIDL_IMPORT_FAILURE;
  }

  /* Function to set Conv2dKernelType in layer params based on the "conv2dKernelType"
     parameter from import config file.
  */
  TIDL_setConv2dKernelType(&tIDLNetStructure, tiLayerIndex);

  tidl_addOutDataLayer(tIDLNetStructure, tiLayerIndex);

  fpNetFile = fopen((const char *)params->outputNetFile, "wb+");
  fwrite(&tIDLNetStructure,1,sizeof(tIDLNetStructure),fpNetFile);

  fclose(fpNetFile);
  fclose(fpParamsFile);

  return TIDL_IMPORT_SUCCESS;
}

/*==============================================================================
* Print TensorFlow version and operators supported by TIDL. 
==============================================================================*/
void tidl_printTfSupport()
{
  int32_t i;

  printf("\nTIDL supports following TensorFlow 1.7+ operators "
         "(please refer to TIDL documentation for more detail): \n");
  for(i=0; i<TIDL_NUM_SUPPORTED_TF_OPERATORS; i++)
  {
    printf("   %d. %s\n", i+1, tidl_TfOpParamMapTable[i].name);
  }
}
