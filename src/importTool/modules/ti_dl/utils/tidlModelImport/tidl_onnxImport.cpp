/*
 *
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
//#include <io.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <float.h>
#include <cmath>

#include "ti_dl.h"
#include "tidl_import_config.h"
#include "onnx/onnx-ml.proto3.pb.h"

using namespace std;
using namespace onnx;
using ::google::protobuf::Message;
using ::google::protobuf::io::FileInputStream;
using ::google::protobuf::io::FileOutputStream;
using ::google::protobuf::io::ZeroCopyInputStream;
using ::google::protobuf::io::CodedInputStream;
using ::google::protobuf::io::ZeroCopyOutputStream;
using ::google::protobuf::io::CodedOutputStream;


#include "tidl_import_common.h" 

extern sTIDL_OrgNetwork_t      orgTIDLNetStructure;
extern sTIDL_OrgNetwork_t      tempTIDLNetStructure;
extern sTIDL_Network_t         tIDLNetStructure;

uint32_t TIDL_kernelReshape(uint8_t * param, uint32_t w, uint32_t h, uint32_t ci, uint32_t co, uint32_t nBytes);

int32_t TIDL_onnxGetAttrIdx(const NodeProto& node, char const * name)
{
  int32_t i;
  for (i = 0; i < node.attribute_size(); i++)
  {
    if ((strcmp(node.attribute(i).name().c_str(), name) == 0))
    {
      return(i);
    }
  }
  return (-1);
}

int32_t TIDL_onnxGetIntAttr(const NodeProto& node, char const * name, int32_t * valuePtr, int32_t idx)
{
  int32_t i = TIDL_onnxGetAttrIdx(node, name);
  if(i != -1)
  { 
    if (node.attribute(i).ints_size() > 0)
    {
      *valuePtr = node.attribute(i).ints(idx);
    }
    else if (idx == 0)
    {
      *valuePtr = node.attribute(i).i();
    }
    else
    {
      return (-1);
    }
    return (0);
  }
  return (-1);
}

int32_t TIDL_onnxGetFloatAttr(const NodeProto& node, char const * name, float * valuePtr, int32_t idx)
{
  int32_t i = TIDL_onnxGetAttrIdx(node, name);
  if (i != -1)
  {
    if (node.attribute(i).floats_size() > 0)
    {
      *valuePtr = node.attribute(i).floats(idx);
    }
    else if (idx == 0)
    {
      *valuePtr = node.attribute(i).f();
    }
    else
    {
      return (-1);
    }
    return (0);
  }
  return (-1);
}

int32_t TIDL_onnxGetStringAttr(const NodeProto& node, char * name, char * valuePtr, int32_t idx)
{
  int32_t i = TIDL_onnxGetAttrIdx(node, name);
  if (i != -1)
  {
      strcpy(valuePtr, node.attribute(i).s().c_str());
  }
  return (-1);
}

TensorProto TIDL_getInitializerTensor(GraphProto& onnGraph, const string name)
{
  int i;

  for (i = 0; i < onnGraph.initializer_size(); i++)
  {
    if ((strcmp(onnGraph.initializer(i).name().c_str(), name.c_str()) == 0))
    {
      auto & tensor = onnGraph.initializer(i);
      return(tensor);
    }
  }
  return (onnx::TensorProto::default_instance());
}

int32_t TIDL_onnxCopyTensor(TensorProto& tensor, void ** ptr, int32_t * size)
{
  int32_t i, tensorSize = 1;

  if (tensor.data_type() == TensorProto_DataType_FLOAT) //DT_FLOAT
  {
    float *dst;
    for (i = 0; i < tensor.dims_size(); i++)
    {
      tensorSize *= tensor.dims(i);
    }
    *size = tensorSize;
    dst = (float *)my_malloc(*size *sizeof(float));

    if (tensor.float_data_size() > 0)
    {
      if (tensor.float_data_size() != tensorSize)
      {
        printf("Tensor size and Dims size not matching !! \n");
      }

      for (i = 0; i < tensorSize; i++)
      {
        dst[i] = tensor.float_data(i);
      }
    }
    else if (tensor.raw_data().size() > 0)
    {
      if ((tensor.raw_data().size() / sizeof(float)) != tensorSize)
      {
        printf("Tensor size and Dims size not matching !! \n");
      }
      memcpy(dst, (float *)tensor.raw_data().c_str(), tensor.raw_data().size());
    }
    *ptr = dst;
    return TIDL_IMPORT_NO_ERR;
  }
  else if (tensor.data_type() == TensorProto_DataType_INT64) //DT_FLOAT
  {
    long long int *dst;
    for (i = 0; i < tensor.dims_size(); i++)
    {
      tensorSize *= tensor.dims(i);
    }
    *size = tensorSize;
    dst = (long long int *)my_malloc(*size *sizeof(long long int));

    if (tensor.int64_data_size() > 0)
    {
      if (tensor.int64_data_size() != tensorSize)
      {
        printf("Tensor size and Dims size not matching !! \n");
      }

      for (i = 0; i < tensorSize; i++)
      {
        dst[i] = tensor.int64_data(i);
      }
    }
    else if (tensor.raw_data().size() > 0)
    {
      if ((tensor.raw_data().size() / sizeof(long long int)) != tensorSize)
      {
        printf("Tensor size and Dims size not matching !! \n");
      }
      memcpy(dst, (long long int *)tensor.raw_data().c_str(), tensor.raw_data().size());
    }
    *ptr = dst;
    return TIDL_IMPORT_NO_ERR;
  }
  else
  {
    printf("TIDL limitation: Only float and INT64 tensor is suported.\n");
    return TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED;
  }
}

int32_t TIDL_onnxCopyFloatInitializerTensor(GraphProto& onnGraph, int32_t nIdx, int32_t inIdx, sBufferPc_t &buf)
{
  int32_t status;

  TensorProto tensor = TIDL_getInitializerTensor(onnGraph, onnGraph.node(nIdx).input(inIdx));
  status = TIDL_onnxCopyTensor(tensor, &buf.ptr, &buf.bufSize);

  return status;
}

int32_t TIDL_onnxMapConvParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t status1, status2;
  int32_t padType;
  int32_t idx1, idx2;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.convParams;
  int32_t pads[4];
  int32_t kernel_shape[2];
  TIDLPCLayers.layerType = TIDL_ConvolutionLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  TensorProto tensor = TIDL_getInitializerTensor(onnGraph, onnGraph.node(i).input(1));
  convParams.numOutChannels = tensor.dims(0);
  convParams.numInChannels  = tensor.dims(1);
  convParams.kernelH        = tensor.dims(2);
  convParams.kernelW        = tensor.dims(3);

  convParams.numGroups = 1;
  convParams.dilationW = 1;
  convParams.dilationH = 1;
  convParams.strideW = 1;
  convParams.strideH = 1;
  convParams.padW = 0;
  convParams.padH = 0;
  convParams.enableBias = 0;
  convParams.enableRelU = 0;
  convParams.enablePooling = 0;

  NodeProto node = onnGraph.node(i);
  TIDL_onnxGetIntAttr(node, "strides",   &convParams.strideH, 0);
  TIDL_onnxGetIntAttr(node, "strides",   &convParams.strideW, 1);
  TIDL_onnxGetIntAttr(node, "dilations", &convParams.dilationH, 0);
  TIDL_onnxGetIntAttr(node, "dilations", &convParams.dilationW, 1);
  TIDL_onnxGetIntAttr(node, "group",     &convParams.numGroups, 0);
  TIDL_onnxGetIntAttr(node, "pads",       &pads[0], 0);
  TIDL_onnxGetIntAttr(node, "pads",       &pads[1], 1);
  TIDL_onnxGetIntAttr(node, "pads",       &pads[2], 2);
  TIDL_onnxGetIntAttr(node, "pads",       &pads[3], 3);
  TIDL_onnxGetIntAttr(node, "kernel_shape", &kernel_shape[0], 0);
  TIDL_onnxGetIntAttr(node, "kernel_shape", &kernel_shape[1], 1);

  if ((pads[0] != 0) || (pads[1] != 0))
  {
    printf("TIDL limitation: Padding only supported in H and W axis \n");
  }
  convParams.padW = pads[3];
  convParams.padH = pads[2];
  if ((kernel_shape[0] != convParams.kernelH) || (kernel_shape[1] != convParams.kernelW))
  {
    printf("Weight Tensor size is not matching with Proto kernel_shape \n");
  }

  status1 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 1, TIDLPCLayers.weights);
  if (onnGraph.node(i).input_size() > 2)
  {
    convParams.enableBias = 1;
    status2 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 2, TIDLPCLayers.bias);
  }

  if(  (status1==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED)
     ||(status2==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED) )
  {
    return TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED;
  }

  return TIDL_IMPORT_NO_ERR;
}


int32_t TIDL_onnxGemmParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t status1, status2;
  int32_t transA = 0;
  int32_t transB = 1;
  float alpha = 1.0;
  float beta  = 1.0;

  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_InnerProductParams_t &innerProductParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.innerProductParams;
  
  orgTIDLNetStructure.TIDLPCLayers[i].layerType = TIDL_InnerProductLayer;
  innerProductParams.enableRelU = 0;

  TIDLPCLayers.layerType = TIDL_InnerProductLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  TensorProto tensor = TIDL_getInitializerTensor(onnGraph, onnGraph.node(i).input(1));
  if (tensor.dims_size() == 2)
  {
    innerProductParams.numOutNodes = tensor.dims(0);
    innerProductParams.numInNodes = tensor.dims(1);
  }
  else
  {
    printf("Could not find the weight tensor B initlizer in the Net.\n");
    return TIDL_IMPORT_ERR_WEIGHT_TENSOR_INIT_NOT_FOUND;
  }

  NodeProto node = onnGraph.node(i);
  TIDL_onnxGetIntAttr(node, "transA", &transA, 0);
  TIDL_onnxGetIntAttr(node, "transB", &transB, 0);
  TIDL_onnxGetFloatAttr(node, "alpha", &alpha, 0);
  TIDL_onnxGetFloatAttr(node, "beta", &beta, 0);

  if ((transA != 0) || (transB != 1) || (alpha != 1.0) || (beta != 1.0))
  {
    printf("TIDL limitation: Only supported Gemm Params are transA = 0, transB = 1, alpha = 1.0 and beta = 1.0."
           "The same will processed as Inner product or filly connected layer in TILD.\n");
    return TIDL_IMPORT_ERR_GEMM_PARAMS_INVALID;
  }

  status1 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 1, TIDLPCLayers.weights);
  status2 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 2, TIDLPCLayers.bias);
  if(  (status1==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED)
     ||(status2==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED) )
  {
    return TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED;
  }

  return TIDL_IMPORT_NO_ERR;
}


int32_t TIDL_onnxSoftmaxParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{

  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_SoftMaxLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return 0;
}

void TIDL_tfBNToScaleBias(
  float    * scale,
  float    * bias,
  uint32_t  numCh,
  float * mean,
  float * var,
  float * gamma,
  float * beta,
  float eps
  );

int32_t TIDL_onnxMapBNParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t status1, status2, status3, status4;
  sBufferPc_t gamma;
  sBufferPc_t beta;
  sBufferPc_t mean;
  sBufferPc_t variance;
  float epsilon = 1e-05;
  int32_t dataSize;

  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_BatchNormLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  NodeProto node = onnGraph.node(i);

  status1 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 1, gamma);
  status2 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 2, beta);
  status3 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 3, mean);
  status4 = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 4, variance);
  if(  (status1==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED)
     ||(status2==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED)
     ||(status3==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED)
     ||(status4==TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED) )
  {
    return TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED;
  }

  TIDL_onnxGetFloatAttr(node, "epsilon", &epsilon, 0);

  dataSize = gamma.bufSize;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.ptr = my_malloc(dataSize*sizeof(float));
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.bufSize = dataSize;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.ptr = my_malloc(dataSize*sizeof(float));
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.bufSize = dataSize;


  TIDL_tfBNToScaleBias((float *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.ptr,
    (float *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.ptr, dataSize,
    (float *)mean.ptr, (float *)variance.ptr, (float *)gamma.ptr, (float *)beta.ptr, epsilon
    );
  my_free(mean.ptr);
  my_free(variance.ptr);
  my_free(gamma.ptr);
  my_free(beta.ptr);

  return TIDL_IMPORT_NO_ERR;
}



int32_t TIDL_onnxMapMaxPoolParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t status;
  int32_t padType;
  int32_t idx1, idx2;
  int32_t pads[4] = { 0 };
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_PoolingParams_t &poolParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams;

  TIDLPCLayers.layerType = TIDL_PoolingLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  poolParams.poolingType = TIDL_MaxPooling;
  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;
  NodeProto node = onnGraph.node(i);
  TIDL_onnxGetIntAttr(node, "strides", &poolParams.strideW, 0);
  TIDL_onnxGetIntAttr(node, "strides", &poolParams.strideH, 1);
  TIDL_onnxGetIntAttr(node, "kernel_shape", &poolParams.kernelW, 0);
  TIDL_onnxGetIntAttr(node, "kernel_shape", &poolParams.kernelH, 1);
  TIDL_onnxGetIntAttr(node, "pads", &pads[0], 0);
  TIDL_onnxGetIntAttr(node, "pads", &pads[1], 1);
  TIDL_onnxGetIntAttr(node, "pads", &pads[2], 2);
  TIDL_onnxGetIntAttr(node, "pads", &pads[3], 3);
  if ((pads[0] != 0) || (pads[1] != 0))
  {
    printf("TIDL limitation: Padding only supported in H and W axis \n");
  }
  poolParams.padW = pads[3];
  poolParams.padH = pads[2];

  return 0;
}


int32_t TIDL_onnxMapArgmaxParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t status;
  int32_t axis, keepdim;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ArgMaxParams_t &argMaxParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.argMaxParams;

  TIDLPCLayers.layerType = TIDL_ArgMaxLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;

  NodeProto node = onnGraph.node(i);
  TIDL_onnxGetIntAttr(node, "axis", &axis, 0);
  TIDL_onnxGetIntAttr(node, "keepdims", &keepdim, 0);
  if (keepdim != 1)
  {
    printf(" keepdim for Argmax Layer shall be 1 \n");
  }
  if (axis != 1)
  {
    printf(" axis for Argmax Layer shall be 1 \n");
  }
  return 0;
}

int32_t TIDL_onnxMapReluParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ReLULayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return 0;
}

int32_t TIDL_onnxMapFlattenParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_FlattenLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return 0;
}

int32_t TIDL_onnxMapAddParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_EltWiseLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.eltWiseParams.eltWiseType = TIDL_EltWiseSum;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.eltWiseParams.numInData = onnGraph.node(i).input_size();
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = onnGraph.node(i).input_size();

  return 0;
}

int32_t TIDL_onnxMapConcatParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t idx;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ConcatLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  int32_t axis;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = onnGraph.node(i).input_size();
  NodeProto node = onnGraph.node(i);
  TIDL_onnxGetIntAttr(node, "axis", &axis, 0);
  if (axis != 1)
  {
    printf("TIDL limitation: Concat is only supported accross channels\n");
    return TIDL_IMPORT_ERR_CONCAT_INVALID_DIM;
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_onnxMapAvgPoolParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  TIDL_onnxMapMaxPoolParams(pOrgTIDLNetStructure, i, layerIndex, dataIndex, onnGraph);
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams.poolingType = TIDL_AveragePooling;
  return 0;
}
int32_t TIDL_onnxMapGlobalAvgPoolParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t status;
  int32_t padType;
  int32_t idx1, idx2;
  int32_t pads[4];
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_PoolingParams_t &poolParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams;

  TIDLPCLayers.layerType = TIDL_PoolingLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  poolParams.poolingType = TIDL_AveragePooling;
  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;
  poolParams.kernelW = 0;
  poolParams.kernelH = 0;
  return 0;
}


int32_t TIDL_onnxMapReshapeParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t status;

  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ReshapeLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  status = TIDL_onnxCopyFloatInitializerTensor(onnGraph, i, 1, TIDLPCLayers.weights);

  return status;
}

int32_t TIDL_onnxMapTransposeParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  int32_t attrIdx;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_TransposeLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  NodeProto node = onnGraph.node(i);
  attrIdx = TIDL_onnxGetAttrIdx(node, "perm");
  if (attrIdx != -1)
  {
    int32_t j;
    int32_t dims = node.attribute(attrIdx).ints_size();
    TIDLPCLayers.weights.ptr = (void*)malloc(sizeof(int)*dims);
    TIDLPCLayers.weights.bufSize = dims;
    int32_t *dimVlaue = (int*)TIDLPCLayers.weights.ptr;
    for (j = 0; j < TIDLPCLayers.weights.bufSize; j++)
    {
      dimVlaue[j] = node.attribute(attrIdx).ints(j);
    }
  }
  return 0;
}


int32_t TIDL_onnxMapDropoutParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_DropOutLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return 0;
}
int32_t TIDL_onnxMapConstParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ConstDataLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = 0;
  return 0;
}

int32_t TIDL_onnxMapUnSuportedlayerParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  GraphProto&            onnGraph)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_UnSuportedLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return 0;
}

typedef struct {
  int8_t name[TIDL_STRING_SIZE];
  int32_t(*tidl_onnxMapFunc)(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
    int32_t              i,
    int32_t              layerIndex,
    int32_t              *dataIndex,
    GraphProto&             onnxGraph);
}sTIDL_onnxOpParamMap_t;

#define TIDL_NUM_SUPPORTED_ONNX_OPERATORS 16
sTIDL_onnxOpParamMap_t tidl_onnxOpParamMapTable[TIDL_NUM_SUPPORTED_ONNX_OPERATORS] =
{
  { "Conv",                          TIDL_onnxMapConvParams },          // TIDL_ConvolutionLayer
  { "MaxPool",                       TIDL_onnxMapMaxPoolParams },       // TIDL_PoolingLayer
  { "Relu",                          TIDL_onnxMapReluParams },          // TIDL_ReLULayer
  { "Concat",                        TIDL_onnxMapConcatParams },        // TIDL_ConcatLayer
  { "AveragePool",                   TIDL_onnxMapAvgPoolParams },       // TIDL_PoolingLayer
  { "GlobalAveragePool",             TIDL_onnxMapGlobalAvgPoolParams }, // TIDL_PoolingLayer
  { "Reshape",                       TIDL_onnxMapReshapeParams },       // TIDL_ReshapeLayer
  { "Transpose",                     TIDL_onnxMapTransposeParams },     // TIDL_TransposeLayer
  { "Add",                           TIDL_onnxMapAddParams },           // TIDL_EltWiseLayer
  { "Sum",                           TIDL_onnxMapAddParams },           // TIDL_EltWiseLayer
  { "ArgMax",                        TIDL_onnxMapArgmaxParams },        // TIDL_ArgMaxLayer
  { "BatchNormalization",            TIDL_onnxMapBNParams },            // TIDL_BatchNormLayer
  { "Gemm",                          TIDL_onnxGemmParams },             // TIDL_InnerProductLayer
  { "Softmax",                       TIDL_onnxSoftmaxParams },          // TIDL_SoftMaxLayer
  { "Flatten",                       TIDL_onnxMapFlattenParams },
  { "Dropout",                       TIDL_onnxMapDropoutParams }        // TIDL_DropOutLayer
};
int32_t TIDL_getOnnxOpParamMapId(const char  * name)
{
  int32_t i = -1;
  for (i = 0; i < sizeof(tidl_onnxOpParamMapTable) / sizeof(sTIDL_onnxOpParamMap_t); i++)
  {
    if ((strcmp(name, (const char *)tidl_onnxOpParamMapTable[i].name) == 0))
    {
      return (i);
    }
  }
  return (-1);
}

TIDL_TFLayerMapping_t TIDL_OnnxLayerMap[] =
{
  { (char*)"TIDL_OnnxShuffle",        (char*)"TIDL_ReshapeLayerTIDL_TransposeLayerTIDL_ReshapeLayer"   , 3 },
  { (char*)"TIDL_TFSlimShuffle",        (char*)"NANA"              , 3 }
};

int32_t tidl_FindOnnxShuffleLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (tidl_isLayerType("TIDL_OnnxShuffle", i1, pOrgTIDLNetStructure, TIDL_OnnxLayerMap, (sizeof(TIDL_OnnxLayerMap) / sizeof(TIDL_TFLayerMapping_t))))
    {
      int32_t mapIdx = tidl_getLayerTypeMapIdx("TIDL_OnnxShuffle", TIDL_OnnxLayerMap, (sizeof(TIDL_OnnxLayerMap) / sizeof(TIDL_TFLayerMapping_t)));
      pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType = TIDL_FlattenLayer;
      
      sTIDL_LayerPC_t &TIDLPCLayers1 = pOrgTIDLNetStructure.TIDLPCLayers[i1];
      sTIDL_LayerPC_t &TIDLPCLayers2 = pOrgTIDLNetStructure.TIDLPCLayers[i1+1];
      sTIDL_LayerPC_t &TIDLPCLayers3 = pOrgTIDLNetStructure.TIDLPCLayers[i1+2];

      if ((TIDLPCLayers1.weights.bufSize ==  5) && (TIDLPCLayers2.weights.bufSize ==  5) && (TIDLPCLayers3.weights.bufSize == 4))
      {
        int64_t * reshape1  = (int64_t *)TIDLPCLayers1.weights.ptr;
        int64_t * reshape2  = (int64_t *)TIDLPCLayers3.weights.ptr;
        int32_t * transpose = (int32_t *)TIDLPCLayers2.weights.ptr;
        if ((reshape1[4] == reshape2[3]) && (reshape1[3] == reshape2[2]) && (4 == transpose[4]) && (3 == transpose[3]) && (1 == transpose[2]) && (2 == transpose[1]))
        {
          pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1 + TIDL_OnnxLayerMap[mapIdx].NumOps - 1].outData[0];
          strcpy((char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1 + TIDL_OnnxLayerMap[mapIdx].NumOps - 1].outDataNames[0]);
          pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1 + TIDL_OnnxLayerMap[mapIdx].NumOps - 1].outConsumerCnt[0];
          for (i2 = 0; i2 < (TIDL_OnnxLayerMap[mapIdx].NumOps - 1); i2++)
          {
            pOrgTIDLNetStructure.TIDLPCLayers[i1 + i2 + 1].numInBufs = -1;
            pOrgTIDLNetStructure.TIDLPCLayers[i1 + i2 + 1].numOutBufs = -1;
          }
          TIDLPCLayers1.layerType = TIDL_ShuffleChannelLayer;
          //TIDLPCLayers1.layerParams.shuffleLayerParams.numGroups = reshape1[1];
        }
      }
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_onnxLayerFillTensorNames(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  GraphProto&             onnxGraph)
{
  int32_t j;
  strcpy((char*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].name, onnxGraph.node(i).name().c_str());

  if (pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs > 0)
  {
    for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs; j++)
    {
      strcpy((char*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].inDataNames[j], onnxGraph.node(i).input(j).c_str());
    }
  }
  if (pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs > 0)
  {
    for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs; j++)
    {
      strcpy((char*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outDataNames[j], onnxGraph.node(i).output(j).c_str());
      pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerLinked[j] = 0;
    }
  }
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weightsElementSizeInBits = NUM_WHGT_BITS;

  return 0;
}

int32_t tidl_onnxLayerUpdateConsumerCount(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  GraphProto&             onnxGraph)
{
  int32_t i0, i1, i2;
  int32_t numCons = 0;
  for (i0 = 0; i0 < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs; i0++)
  {

    for (i1 = 0; i1 < onnxGraph.node_size(); i1++)
    {
      for (i2 = 0; i2 < onnxGraph.node(i1).input_size(); i2++)
      {
        if (strcmp((const char *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outDataNames[i0], onnxGraph.node(i1).input(i2).c_str()) == 0)
        {
          numCons++;
        }
      }
    }
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerCnt[i0] = numCons;
  }
  return 0;
}

sTIDL_tfOutRehapeMap_t sTIDL_onnxOutRehapeTable[] =
{
  { TIDL_DataLayer                     ,  TIDL_tfOutReshapeDataLayer },
  { TIDL_ConvolutionLayer              ,  TIDL_tfOutReshapeConvLayer },
  { TIDL_PoolingLayer                  ,  TIDL_tfOutReshapePoolingLayer },
  { TIDL_ReLULayer                     ,  TIDL_tfOutReshapeRelu },
  { TIDL_PReLULayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_EltWiseLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_InnerProductLayer             ,  TIDL_tfOutReshapeIPLayer },
  { TIDL_SoftMaxLayer                  ,  TIDL_tfOutReshapeSoftmax },
  { TIDL_BatchNormLayer                ,  TIDL_tfOutReshapeBN },
  { TIDL_BiasLayer                     ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ScaleLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_Deconv2DLayer                 ,  TIDL_tfOutReshapeDeConvLayer },
  { TIDL_ConcatLayer                   ,  TIDL_tfOutReshapeConcatLayer },
  { TIDL_SplitLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_SliceLayer                    ,  TIDL_tfOutReshapeSliceLayre },
  { TIDL_CropLayer                     ,  TIDL_tfOutReshapeCropLayer },
  { TIDL_FlattenLayer                  ,  TIDL_tfOutReshapeFlattenLayer },
  { TIDL_DropOutLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ArgMaxLayer                   ,  TIDL_tfOutReshapeArgmaxLayer },
  { TIDL_DetectionOutputLayer          ,  TIDL_tfOutReshapeDetOutLayer },
  { TIDL_UnSuportedLayer               ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ConstDataLayer                ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ShuffleChannelLayer           ,  TIDL_tfOutReshapeIdentity }, 
  { TIDL_ResizeLayer                   ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PriorBoxLayer                 ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PermuteLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ReshapeLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ShapeLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_SqueezeLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PadLayer                      ,  TIDL_tfOutReshapePadLayer },
  { TIDL_TransposeLayer                ,  TIDL_tfOutReshapeIdentity }
  
};

int32_t onnx_import(tidl_import_config * params)
{
  int32_t                    i,j;
  int32_t                    layerNum;
  int32_t                    inputSize;
  int32_t                    pad,stride;
  int32_t                    layerIndex;
  int32_t                    tiLayerIndex;
  int32_t                    dataIndex;
  const uint8_t             *name;
  const uint8_t             *inputName[10];
  const uint8_t             *outputName;
  ModelProto                 onnxModel;
  int32_t                    status, importStatus, numErrs, numUnsupportedLayers;
  int32_t                    dataSize;
  int32_t                    id;
  int paramSet  = 0;
  int conv2DRandParams = 0;
  string attrKey;
  int32_t inLayerId = 0;  
  int32_t weightsElementSizeInBits;
  int32_t mapTblIdx = -1;
  FILE    *fpNetFile;
  FILE    *fpParamsFile;

  string key = "value";

  printf("ONNX Model (Proto) File: %s  \n",(const char *)params->inputNetFile);
  printf("TIDL Network File      : %s  \n", (const char *)params->outputNetFile);
  printf("TIDL Params  File      : %s  \n\n", (const char *)params->outputParamsFile);

  TIDL_readProtoFromBinaryFile((const char *)params->inputNetFile, &onnxModel);
  GraphProto onnxGraph = onnxModel.graph();

  layerIndex = 0;
  dataIndex  = 0;
  numErrs    = 0;
  numUnsupportedLayers = 0;

  for (i = 0; i < onnxGraph.node_size(); i++)
  {
    //printf("Parsing node %4d: %s, %s  \n", i, onnxGraph.node(i).name().c_str(), onnxGraph.node(i).op_type().c_str());

    if (strcmp(onnxGraph.node(i).op_type().c_str(), "Constant") == 0)
    {
      continue;
    }

    /* Set default values of numInBufs and numOutBufs which may be changed by
       tidl_onnxMapFunc below for certain layers. */
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs = 1;
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs = 1;
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].inData[0].dataId = -1;

    mapTblIdx = TIDL_getOnnxOpParamMapId(onnxGraph.node(i).op_type().c_str());
    if (mapTblIdx == -1)
    {
      printf("TIDL limitation: ONNX operator %s is not suported.\n", onnxGraph.node(i).op_type().c_str());
      TIDL_onnxMapUnSuportedlayerParams(&orgTIDLNetStructure, i, layerIndex, &dataIndex, onnxGraph);
      numUnsupportedLayers++;
    }
    else
    {
      importStatus = tidl_onnxOpParamMapTable[mapTblIdx].tidl_onnxMapFunc(&orgTIDLNetStructure, i, layerIndex, &dataIndex, onnxGraph);
      if(importStatus != TIDL_IMPORT_NO_ERR)
      {
        printf("TIDL limitation: ONNX operator %s cannot be mapped to a TIDL layer.\n", onnxGraph.node(i).op_type().c_str());
        numUnsupportedLayers++;
      }
    }

    tidl_onnxLayerFillTensorNames(&orgTIDLNetStructure, i, layerIndex, onnxGraph);
    tidl_onnxLayerUpdateConsumerCount(&orgTIDLNetStructure, i, layerIndex, onnxGraph);
    tidl_linkInputTensors(&orgTIDLNetStructure, layerIndex);
    tidl_linkOutputTensors(&orgTIDLNetStructure, layerIndex);

    layerIndex++;
  }

  //printf("Number of original layers: %d\n", layerIndex);

  if(numUnsupportedLayers > 0)
  {
    printf("\nImport error: This ONNX model has unsupported operators. "
             "Please check TIDL User's Guide for supported operators.\n");
    numErrs++;
  }

  tidl_addInDataLayer(orgTIDLNetStructure, layerIndex, &dataIndex);
  layerIndex = orgTIDLNetStructure.numLayers;

  importStatus = tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\nImport error: This ONNX model's topology is not supported. "
           "Please check TIDL User's Guide for supported network topologies.\n");
    numErrs++;
  }

  tidl_FindOnnxShuffleLayer(orgTIDLNetStructure, layerIndex);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  //printf("Number of layers after tidl_removeMergedLayersFromNet: %d\n", layerIndex);

  tidl_fillInDataLayerShape(orgTIDLNetStructure, params, layerIndex);
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);
  tidl_updateOutDataShape(orgTIDLNetStructure, 0, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_onnxOutRehapeTable);

  importStatus = tidl_mergeDropoutLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Dropout layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeBiasLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Bias layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeBNLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Batch Norm layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeReluLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Relu layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergePadLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Pad layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeFlattenLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Flatten layer cannot be merged into other layers.\n");
    numErrs++;
  }
  
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;

  tidl_convertRelUToBNLayer(orgTIDLNetStructure, layerIndex);

  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);

  importStatus = tidl_convertConv2DToIpLayer(orgTIDLNetStructure, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_onnxOutRehapeTable);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Conv2D layer cannot be converted into Inner Product layer.\n");
    numErrs++;
  }

  importStatus = tidl_mergeReshapeLayer(orgTIDLNetStructure, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_onnxOutRehapeTable);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\nImport error: Reshape layer cannot be merged.\n");
    numErrs++;
  }

  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;

  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);

  importStatus = tidl_convertIpLayerInputShape(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Inner product layer input cannot be shaped.\n");
    numErrs++;
  }

  tidl_importEltWiseParams(&orgTIDLNetStructure, layerIndex);

  /* Quantize and write out layer params */
  fpParamsFile = fopen((const char *)params->outputParamsFile, "wb+");
  TIDL_importQuantWriteLayerParams(&orgTIDLNetStructure, layerIndex, fpParamsFile);

  tiLayerIndex = tidl_copyPCNetToDeviceNet(orgTIDLNetStructure, tIDLNetStructure, layerIndex, NUM_WHGT_BITS);

  /* Have a final check if there are any layers which
     - are supported only by merging with other TIDL layers,
     - but are not able to be merged. */
  numUnsupportedLayers = tidl_countUnsupportedLayers(&tIDLNetStructure, tiLayerIndex);
  if(numUnsupportedLayers > 0)
  {
    printf("\nImport error: This ONNX model has operators that are supported"
           " by TIDL only if they can be merged with TIDL layers. But these operators"
           " cannot be merged with any TIDL layer.\n"
           "Please check TIDL User's Guide for supported ONNX operators.\n");
    numErrs++;
  }

  if(numErrs > 0)
  {
    /* Stop the import process here to prevent potential crash if continuing */
    return TIDL_IMPORT_FAILURE;
  }

  /* Function to set Conv2dKernelType in layer params based on the "conv2dKernelType"
     parameter from import config file. 
  */     
  TIDL_setConv2dKernelType(&tIDLNetStructure, tiLayerIndex); 

  tidl_addOutDataLayer(tIDLNetStructure, tiLayerIndex);

  fpNetFile = fopen((const char *)params->outputNetFile, "wb+");
  fwrite(&tIDLNetStructure,1,sizeof(tIDLNetStructure),fpNetFile);

  fclose(fpNetFile);
  fclose(fpParamsFile);

  return TIDL_IMPORT_SUCCESS;
}

/*==============================================================================
* Print ONNX version and operators supported by TIDL, where opset_version 5 is
* obtained from onnx-operators.proto3.
==============================================================================*/
void tidl_printOnnxSupport()
{
  int32_t i;

  printf("\nTIDL supports following ONNX operators with opset_version 5 "
         "(please refer to TIDL documentation for more detail): \n");
  for(i=0; i<TIDL_NUM_SUPPORTED_ONNX_OPERATORS; i++)
  {
    printf("   %d. %s\n", i+1, tidl_onnxOpParamMapTable[i].name);
  }
}
