/*
 *
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
#ifdef _WIN32
#include <io.h>
#else
#define O_BINARY O_RDONLY
#include <unistd.h>
#endif
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <cmath>
#include <cfloat>

using namespace std;
using ::google::protobuf::Message;
using ::google::protobuf::io::FileInputStream;
using ::google::protobuf::io::FileOutputStream;
using ::google::protobuf::io::ZeroCopyInputStream;
using ::google::protobuf::io::CodedInputStream;
using ::google::protobuf::io::ZeroCopyOutputStream;
using ::google::protobuf::io::CodedOutputStream;

#include "ti_dl.h"
#include "tidl_import_config.h"
#include "tidl_import_common.h"
#define ALIGN_CHANNEL_PITCH (1)
#define TIDL_MSMC_NUM_PHY_BANKS ( 4U)
#define TIDL_MSMC_PHY_BANK_SIZE (256U)
#define TIDL_MSMC_BANK_PITCH (TIDL_MSMC_NUM_PHY_BANKS * TIDL_MSMC_PHY_BANK_SIZE)
#define TIDL_MSMC_CACHE_LINE_SIZE (128U)
#define ALIGN_SIZE(x,y)       ((((x) + ((y)-1)) / (y)) * (y))

extern int32_t gloab_data_format;
extern sTIDL_OrgNetwork_t      orgTIDLNetStructure;
extern sTIDL_OrgNetwork_t      tempTIDLNetStructure;
extern sTIDL_Network_t         tIDLNetStructure;

#define QUAN_STYLE2_ROUND ((gParams.quantRoundAdd*1.0 / 100))

const char * TIDL_LayerString[] = 
{
"TIDL_DataLayer            ",
"TIDL_ConvolutionLayer     ",
"TIDL_PoolingLayer         ",
"TIDL_ReLULayer            ",
"TIDL_PReLULayer           ",
"TIDL_EltWiseLayer         ",
"TIDL_InnerProductLayer    ",
"TIDL_SoftMaxLayer         ",
"TIDL_BatchNormLayer       ",
"TIDL_BiasLayer            ",
"TIDL_ScaleLayer           ",
"TIDL_Deconv2DLayer        ",
"TIDL_ConcatLayer          ",
"TIDL_SplitLayer           ",
"TIDL_SliceLayer           ",
"TIDL_CropLayer            ",
"TIDL_FlattenLayer         ",
"TIDL_DropOutLayer         ",
"TIDL_ArgMaxLayer          ",
"TIDL_DetectionOutputLayer ",
"TIDL_UnSuportedLayer",
"TIDL_ConstDataLayer",
"TIDL_ShuffleChannelLayer",
"TIDL_ResizeLayer",
"TIDL_PriorBoxLayer",
"TIDL_PermuteLayer",
"TIDL_ReshapeLayer",
"TIDL_ShapeLayer",
"TIDL_SqueezeLayer",
"TIDL_PadLayer",
"TIDL_TransposeLayer",
};

static int totalMemAllocation = 0;
FILE *fpAlloc = NULL;
void * my_malloc(int size)
{
  void *ptr;
  //if(fpAlloc == NULL) fpAlloc = fopen ("MemAllocation.txt", "w");
  totalMemAllocation += size;
  ptr = malloc(size);
  assert(ptr != NULL);

  //fprintf(fpAlloc, "Alloc: Ptr: %0x, Size: %0x\n",ptr,size);
  //fflush(fpAlloc);
  return ptr;
}

void my_free(void *ptr)
{
  //fprintf(fpAlloc, "Free: Ptr: %0x\n",ptr);
  //fflush(fpAlloc);
  free(ptr);
}
FILE *paramDebugFile = NULL;
int debugLayeId = 0;

/* compares quantized values and the original values (for parameters) */
template <class quantParamType>
int TIDL_CompareParams(quantParamType *quantizedParams, float *origParams, int paramNum, float scale) {
  /* absolute value of difference is considered */
  float meanDifference = 0;
  float maxDifference = 0;

  float meanRelDifference = 0;
  float maxRelDifference = 0;
  float orgMaxFloat   = 0;
  float quantMaxFloat = 0;

  float meanOrigFloat = 0;

  int relValidNum = 0;
  int maxRelDiffIndex = 0;
  for (int i = 0; i < paramNum; i++)
  {
    float quantParamFloat = quantizedParams[i] / scale;
    float origFloat = origParams[i];
    float difference = quantParamFloat>origFloat ? (quantParamFloat - origFloat) : (origFloat - quantParamFloat); /* abs value */
    float absOrigFloat = origFloat>0 ? origFloat : -origFloat;
    float absQuantFloat = quantParamFloat>0 ? quantParamFloat : -quantParamFloat;
    int  absQuantizedParams = quantizedParams[i] > 0 ? quantizedParams[i] : -quantizedParams[i];
    meanOrigFloat += absOrigFloat;

    meanDifference += difference;

    if (maxDifference < difference)
    {
      maxDifference = difference;
    }
    if (orgMaxFloat < absOrigFloat)
    {
      orgMaxFloat = absOrigFloat;
    }
    if (quantMaxFloat < absQuantFloat)
    {
      quantMaxFloat = absQuantFloat;
    }
    float relDifference = 0;

    if (absQuantizedParams > 2)
    {
      relDifference = (difference / absOrigFloat) * 100;
      relValidNum++;
    }

    if (maxRelDifference < relDifference)
    {
      maxRelDifference = relDifference;
      maxRelDiffIndex = i;
    }
    meanRelDifference += relDifference;

  }
  meanDifference /= paramNum;

  if (relValidNum != 0)
    meanRelDifference /= relValidNum;
  else
    meanRelDifference = -1;

  meanOrigFloat /= paramNum;
  if (paramDebugFile != NULL)
  {
    fprintf(paramDebugFile, "%d,%f,%f,%f,%f,%f,%f,%f,%f,%f\n", debugLayeId, meanDifference, maxDifference, meanOrigFloat, meanRelDifference, orgMaxFloat, quantMaxFloat, origParams[maxRelDiffIndex], quantizedParams[maxRelDiffIndex] / scale, maxRelDifference);
  }
  return 0;

}

int32_t TIDL_QuantizeUnsignedMax(uint8_t * params, float * data, int32_t dataSize, float min, float max, int32_t numBits, int32_t weightsElementSizeInBits, int32_t * zeroWeightValue)
{
  int32_t i;
  if (max == min)
  {
    if (min)
    {
      min = min*0.5;
    }
    else
    {
      min = -1;
    }
  }
  float absRange = abs(max - min);

  float quantPrec = ((1.0*(1 << numBits)) / absRange);
  float pData;
  int32_t param;

  int32_t offset;
  if ((quantPrec * 256) > INT32_MAX)
  {
    quantPrec = INT32_MAX / 256;
  }
  if(min  > 0)
  {
    offset = (min *  quantPrec + QUAN_STYLE2_ROUND);
  }
  else
  {
    offset = (min *  quantPrec - QUAN_STYLE2_ROUND);
  }

  //Convert float params to 8-bit or 16-bit
  if(weightsElementSizeInBits <= 8)
  {
    for(i = 0; i < dataSize; i++)
    {
      pData = data[i]; 
      if(pData  > 0)
      {
        param = (pData *  quantPrec + QUAN_STYLE2_ROUND);
      }
      else
      {
        param = (pData *  quantPrec - QUAN_STYLE2_ROUND);
      }
      param = param - offset;

      params[i] = param > ((1 << weightsElementSizeInBits) - 1) ? ((1 << weightsElementSizeInBits) - 1) : param;
    }
  }
  else
  {
	uint16_t *params16 = (uint16_t *)params;

  for(i = 0; i < dataSize; i++)
  {
    pData = data[i]; 
    if(pData  > 0)
    {
      param = (pData *  quantPrec + QUAN_STYLE2_ROUND);
    }
    else
    {
      param = (pData *  quantPrec - QUAN_STYLE2_ROUND);
    }
    param = param - offset;

      params16[i] = param > ((1 << weightsElementSizeInBits) - 1) ? ((1 << weightsElementSizeInBits) - 1) : param;
    }
  }
  *zeroWeightValue = -offset;
  return ((int32_t)(quantPrec*256));
}

#if 0
int32_t TIDL_QuantizeUnsignedMax(uint8_t * params, float * data, int32_t dataSize, float min, float max, int32_t weightsElementSizeInBits)

{
  int32_t i;
  float absRange = abs(max - min);

  float quantPrec = ((1.0*(1 << NUM_WHGT_BITS)) / absRange);
  float pData;
  int32_t param;

  int32_t offset;
  if (min > 0)
  {
    offset = (min *  quantPrec + QUAN_STYLE2_ROUND);
  }
  else
  {
    offset = (min *  quantPrec - QUAN_STYLE2_ROUND);
  }

  //Convert float params to 8-bit or 16-bit
  if (weightsElementSizeInBits <= 8)
  {
    for (i = 0; i < dataSize; i++)
    {
      pData = data[i];
      if (pData > 0)
      {
        param = (pData *  quantPrec + QUAN_STYLE2_ROUND);
      }
      else
      {
        param = (pData *  quantPrec - QUAN_STYLE2_ROUND);
      }
      param = param - offset;

      params[i] = param > ((1 << weightsElementSizeInBits) - 1) ? ((1 << weightsElementSizeInBits) - 1) : param;
    }
  }
  else
  {
    uint16_t *params16 = (uint16_t *)params;

    for (i = 0; i < dataSize; i++)
    {
      pData = data[i];
      if (pData > 0)
      {
        param = (pData *  quantPrec + QUAN_STYLE2_ROUND);
      }
      else
      {
        param = (pData *  quantPrec - QUAN_STYLE2_ROUND);
      }
      param = param - offset;

      params16[i] = param > ((1 << weightsElementSizeInBits) - 1) ? ((1 << weightsElementSizeInBits) - 1) : param;
    }
  }
  return ((int32_t)(quantPrec * 256));
}
#endif

template <class Tout>
float TIDL_QuantizeSignedMax(Tout * params, float * data, int32_t dataSize, float min, float max, int32_t weightsElementSizeInBits)

{
  int32_t i;
  float absRange = (abs(max) > abs(min)) ? abs(max) : abs(min);

#if 0
  if (gParams.quantizationStyle == TIDL_quantStyleP2Dynamic)
  {
    absRange = (float)ceil(log((double)absRange) / log((double)2));
    absRange = pow(2.0, (double)absRange);
  }
#endif

  float quantPrec;
  float pData;
  int32_t param;
  if (absRange != 0)
  {
    quantPrec = ((1.0*(1 << (weightsElementSizeInBits - 1))) / absRange);
  }
  else
  {
    quantPrec = 1;
  }

  for (i = 0; i < dataSize; i++)
  {
    pData = data[i];
    if (pData > 0)
    {
      param = (pData *  quantPrec + QUAN_STYLE2_ROUND);
    }
    else
    {
      param = (pData *  quantPrec - QUAN_STYLE2_ROUND);
    }
    param = param > ((1 << (weightsElementSizeInBits - 1)) - 1) ? ((1 << (weightsElementSizeInBits - 1)) - 1) : param;
    params[i] = param < (-1 * (1 << (weightsElementSizeInBits - 1))) ? (-1 * (1 << (weightsElementSizeInBits - 1))) : param;
  }
  TIDL_CompareParams(params, data, dataSize, quantPrec);
  return (quantPrec);
}

template float TIDL_QuantizeSignedMax<signed char>(signed char * params, float * data, int32_t dataSize, float min, float max, int32_t weightsElementSizeInBits);
template float TIDL_QuantizeSignedMax<signed short>(signed short * params, float * data, int32_t dataSize, float min, float max, int32_t weightsElementSizeInBits);

int32_t TIDL_normalize(float data, float min, float max)
{
  int32_t param;
  if (max == min)
  {
    if (min)
    {
      min = min * 0.5;
    }
    else
    {
      min = -1;
    }
  }
  float absRange = abs(max - min);
  float quantPrec = ((1.0*(1 << NUM_BIAS_BITS)) / absRange);
  if ((quantPrec * 256) > INT32_MAX)
  {
    quantPrec = INT32_MAX / 256;
  }
  if(data  > 0)
  {
    param = (data *  quantPrec + QUAN_STYLE2_ROUND);
  }
  else
  {
    param = (data *  quantPrec - QUAN_STYLE2_ROUND);
  }


  return param;
}

#if 0
int32_t TIDL_normalize(float data, float min, float max)
{
  int32_t param;
  float absRange = abs(max - min);
  float quantPrec = ((1.0*(1 << NUM_WHGT_BITS)) / absRange);
  if (data > 0)
  {
    param = (data *  quantPrec + QUAN_STYLE2_ROUND);
  }
  else
  {
    param = (data *  quantPrec - QUAN_STYLE2_ROUND);
  }


  return param;
}
#endif

bool TIDL_readProtoFromTextFile(const char* fileName, Message* proto)
{
  int32_t           fd;
  bool              success;
  FileInputStream   *input;

  fd = open(fileName, O_RDONLY); // file existence already verified 

  input = new FileInputStream(fd);
  success = google::protobuf::TextFormat::Parse(input, proto);
  delete input;
  close(fd);
  if (!success)
  {
    printf("ERROR: Reading text proto file\n");
  }
  return success;
}

#define APP_CNN_INTEROP_CAFFE_READ_BINARY_TOTAL_BYTE_LIMIT  2147483647
#define APP_CNN_INTEROP_CAFFE_READ_BINARY_WARNING_THRESHOLD 1073741824

bool TIDL_readProtoFromBinaryFile(const char* fileName, Message* proto)
{
  int                   fd;
  ZeroCopyInputStream   *rawInput;
  CodedInputStream      *codedInput;
  bool                  success;

  fd = open(fileName, O_BINARY); // file existence already verified 

  rawInput = new FileInputStream(fd);
  codedInput = new CodedInputStream(rawInput);

  codedInput->SetTotalBytesLimit(
    APP_CNN_INTEROP_CAFFE_READ_BINARY_TOTAL_BYTE_LIMIT,
    APP_CNN_INTEROP_CAFFE_READ_BINARY_WARNING_THRESHOLD
    );

  success = proto->ParseFromCodedStream(codedInput);
  delete codedInput;
  delete rawInput;
  close(fd);
  if (!success)
  {
    printf("ERROR: Reading binary proto file\n");
  }
  return success;
}

int32_t TIDL_getLayerIdx(sTIDL_OrgNetwork_t * pOrgTIDLNetStructure, int32_t numLayer, const char *bufName)
{
  int32_t i, j;
  for (i = (numLayer - 1); i >= 0; i--)
  {
    for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[i].numOutBufs; j++)
    {
      if (strcmp((const char*)bufName, (const char*)pOrgTIDLNetStructure->TIDLPCLayers[i].outDataNames[j]) == 0)
      {
        return i;
      }
    }
  }
  return (-1);
}

int32_t TIDL_getDataID(sTIDL_DataParams_t *data,
  sTIDL_OrgNetwork_t * pOrgTIDLNetStructure,
  int32_t            numLayer,
  int8_t             *bufName)
{
  int32_t i, j;
  for (i = (numLayer - 1); i >= 0; i--)
  {
    for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[i].numOutBufs; j++)
    {
      if (strcmp((const char*)bufName,
        (const char*)pOrgTIDLNetStructure->TIDLPCLayers[i].outDataNames[j]) == 0)
      {
        *data = pOrgTIDLNetStructure->TIDLPCLayers[i].outData[j];
        return 0;
      }
    }
  }
  return -1;
}

int32_t TIDL_isDataBufUsed(int32_t           dataId,
  sTIDL_Network_t   *pTIDLNetStructure,
  int32_t           numLayer)
{
  int32_t i, j;
  for (i = 0; i < numLayer; i++)
  {
    for (j = 0; j < pTIDLNetStructure->TIDLLayers[i].numInBufs; j++)
    {
      if (pTIDLNetStructure->TIDLLayers[i].inData[j].dataId == dataId)
      {
        return 1;
      }
    }
  }
  return 0;
}

int32_t TIDL_isInputConv2D(sTIDL_OrgNetwork_t   *pOrgTIDLNetStruct,
  int32_t              numLayer,
  const char           *bufName)
{
  int32_t i, j;
  for (i = (numLayer - 1); i >= 0; i--)
  {
    for (j = 0; j < pOrgTIDLNetStruct->TIDLPCLayers[i].numOutBufs; j++)
    {
      if (strcmp((const char*)bufName,
        (const char*)pOrgTIDLNetStruct->TIDLPCLayers[i].outDataNames[j]) == 0)
      {
        if ((pOrgTIDLNetStruct->TIDLPCLayers[i].numOutBufs == 1) &&
          (pOrgTIDLNetStruct->TIDLPCLayers[i].layerType == TIDL_ConvolutionLayer))
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }
    }
  }
  return 0;
}

void TIDL_UpdateInDataBuff(sTIDL_OrgNetwork_t * pOrgTIDLNetStructure,
  uint32_t numLayers, sTIDL_DataParams_t dataBuf)
{
  uint32_t i, j;
  for (i = 0; i < numLayers; i++)
  {
    for (j = 0; (j < pOrgTIDLNetStructure->TIDLPCLayers[i].numInBufs) &&
      (pOrgTIDLNetStructure->TIDLPCLayers[i].numInBufs > 0); j++)
    {
      if (pOrgTIDLNetStructure->TIDLPCLayers[i].inData[j].dataId ==
        dataBuf.dataId)
      {
        pOrgTIDLNetStructure->TIDLPCLayers[i].inData[j] = dataBuf;
      }
    }
  }

  return;
}

void TIDL_findRange(float * data, int32_t dataSize, float * minOut, float * maxOut, float scale)
{
  float min = FLT_MAX;
  float max = FLT_MIN;
  int32_t i;
  for (i = 0; i < dataSize; i++)
  {
    min = ((data[i] * scale) < min) ? (data[i] * scale) : min;
    max = ((data[i] * scale) > max) ? (data[i] * scale) : max;
  }
  *minOut = (min < *minOut) ? min : *minOut;
  *maxOut = (max > *maxOut) ? max : *maxOut;
}

int32_t TIDL_findSymQ(float  min, float max)
{
  int32_t qValue = 0, i;
  float absMax = (abs(min) > abs(max)) ? abs(min) : abs(max);
  int32_t quantSteps = ((1.0*(1 << (NUM_WHGT_BITS - 1))) / absMax);
  while ((quantSteps & (quantSteps - 1)) != 0)
  {
    quantSteps--;
  }
  for (i = 32; i >= 0; i--)
  {
    if (quantSteps & (1 << i))
    {
      qValue = i;
    }
  }
  return(qValue);
}


int64_t TIDL_roundSat(int64_t val, uint8_t bits, int64_t min, int64_t max)
{
  if (bits > 0)
  {
    val += (1U << (bits - 1U));
    val >>= bits;
  }
  val = val < min ? min : val;
  val = val > max ? max : val;
  return val;

}
#if 0
int32_t TIDL_alignParamsWrite(FILE *fp, sBuffer_t * buf, uint32_t *totalParamSize, uint32_t numBytes)
{

  uint32_t alignSize = (*totalParamSize % 64);
  int32_t offset;
  if (alignSize)
  {
    alignSize = 64 - alignSize;
    uint8_t * ptr = (uint8_t *)my_malloc(alignSize*sizeof(uint8_t));
    memset(ptr, 0, alignSize*sizeof(uint8_t));

    *totalParamSize += alignSize;
    if (fp) fwrite(ptr, 1, alignSize, fp);
    free(ptr);
  }
  uint32_t writeSize = buf->bufSize * numBytes;
  if (fp)
  {
    fwrite(buf->ptr, 1, writeSize, fp);
    free(buf->ptr);
  }
  offset = *totalParamSize;
  *totalParamSize += writeSize;
  return (offset);
}
#endif

void TIDL_convertSbuff(sBuffer_t * sBuffDst, sBufferPc_t *sBuffSrc)
{
  RESET_PTR(sBuffDst->ptr);  // ptr is not used by TIDL lib 
  sBuffDst->bufSize = sBuffSrc->bufSize;  
}

void  TIDL_fillDataBufPadRequirements(sTIDL_Network_t * tIDLNetStructure)
{
  int32_t i, j, k, l;
  int32_t padW, padH, curPadW, curPadH;
  int32_t foundInData;
  for (i = 0; i < tIDLNetStructure->numLayers; i++)
  {
    for (j = 0; j < tIDLNetStructure->TIDLLayers[i].numOutBufs; j++)
    {
      padW = 1;//:TODO: Temporary change to make same padding for all layers in mobile to be 1
      padH = 1;//:TODO: Temporary change to make same padding for all layers in mobile to be 1
      for (k = i + 1; k < tIDLNetStructure->numLayers; k++)
      {
        for (l = 0; l < tIDLNetStructure->TIDLLayers[k].numInBufs; l++)
        {
          curPadW = 0;
          curPadH = 0;
          if (tIDLNetStructure->TIDLLayers[i].outData[j].dataId == tIDLNetStructure->TIDLLayers[k].inData[l].dataId)
          {
            if ((tIDLNetStructure->TIDLLayers[k].layerType == TIDL_ConvolutionLayer) ||
              (tIDLNetStructure->TIDLLayers[k].layerType == TIDL_Deconv2DLayer))
            {
              curPadW = tIDLNetStructure->TIDLLayers[k].layerParams.convParams.padW;
              curPadH = tIDLNetStructure->TIDLLayers[k].layerParams.convParams.padH;
            }
            else if (tIDLNetStructure->TIDLLayers[k].layerType == TIDL_PoolingLayer)
            {
              if (tIDLNetStructure->TIDLLayers[k].layerParams.poolParams.padW)
              {
                curPadW = tIDLNetStructure->TIDLLayers[k].layerParams.poolParams.padW;
              }
              else
              {
                curPadW = ((tIDLNetStructure->TIDLLayers[k].layerParams.poolParams.kernelW - 1) / 2);
              }
              if (tIDLNetStructure->TIDLLayers[k].layerParams.poolParams.padH)
              {
                curPadH = tIDLNetStructure->TIDLLayers[k].layerParams.poolParams.padH;
              }
              else
              {
                curPadH = ((tIDLNetStructure->TIDLLayers[k].layerParams.poolParams.kernelH - 1) / 2);
              }
            }
            if (curPadW > padW) padW = curPadW;
            if (curPadH > padH) padH = curPadH;
          }
        }
      }
      //TBDLater tIDLNetStructure->TIDLLayers[i].outData[j].padW = padW;
      //TBDLater tIDLNetStructure->TIDLLayers[i].outData[j].padH = padH;
      //TBDLater tIDLNetStructure->TIDLLayers[i].outData[j].pitch[TIDL_LINE_PITCH] = tIDLNetStructure->TIDLLayers[i].outData[j].dimValues[TIDL_DIM_WIDTH] + tIDLNetStructure->TIDLLayers[i].outData[j].padW;
      /* Align pitch accross channel so that we dont access same bank */
      //TBDLater tIDLNetStructure->TIDLLayers[i].outData[j].pitch[TIDL_CHANNEL_PITCH] = (tIDLNetStructure->TIDLLayers[i].outData[j].dimValues[TIDL_DIM_HEIGHT] + 2 * tIDLNetStructure->TIDLLayers[i].outData[j].padH + 1) * tIDLNetStructure->TIDLLayers[i].outData[j].pitch[TIDL_LINE_PITCH];
#if ALIGN_CHANNEL_PITCH
      tIDLNetStructure->TIDLLayers[i].outData[j].pitch[TIDL_CHANNEL_PITCH] = ALIGN_SIZE(tIDLNetStructure->TIDLLayers[i].outData[j].pitch[TIDL_CHANNEL_PITCH], TIDL_MSMC_BANK_PITCH) + TIDL_MSMC_CACHE_LINE_SIZE;
#endif
      tIDLNetStructure->TIDLLayers[i].outData[j].pitch[TIDL_ROI_PITCH] = tIDLNetStructure->TIDLLayers[i].outData[j].dimValues[TIDL_DIM_NUMCH] * tIDLNetStructure->TIDLLayers[i].outData[j].pitch[TIDL_CHANNEL_PITCH];


      //tIDLNetStructure->TIDLLayers[i].outData[j].padW = 4;
      //tIDLNetStructure->TIDLLayers[i].outData[j].padH = 4;
    }
  }
  for (i = 0; i < tIDLNetStructure->numLayers; i++)
  {
    for (j = 0; j < tIDLNetStructure->TIDLLayers[i].numInBufs; j++)
    {
      foundInData = 0;
      for (k = 0; ((k < tIDLNetStructure->numLayers) && (foundInData == 0)); k++)
      {
        for (l = 0; ((l < tIDLNetStructure->TIDLLayers[k].numOutBufs) && (foundInData == 0)); l++)
        {
          if (tIDLNetStructure->TIDLLayers[i].inData[j].dataId == tIDLNetStructure->TIDLLayers[k].outData[l].dataId)
          {
            //TBDLater  tIDLNetStructure->TIDLLayers[i].inData[j].padW = tIDLNetStructure->TIDLLayers[k].outData[l].padW;
            //TBDLater  tIDLNetStructure->TIDLLayers[i].inData[j].padH = tIDLNetStructure->TIDLLayers[k].outData[l].padH;
            tIDLNetStructure->TIDLLayers[i].inData[j].pitch[TIDL_LINE_PITCH]       = tIDLNetStructure->TIDLLayers[k].outData[l].pitch[TIDL_LINE_PITCH];
            tIDLNetStructure->TIDLLayers[i].inData[j].pitch[TIDL_CHANNEL_PITCH]    = tIDLNetStructure->TIDLLayers[k].outData[l].pitch[TIDL_CHANNEL_PITCH];
            tIDLNetStructure->TIDLLayers[i].inData[j].pitch[TIDL_ROI_PITCH]        = tIDLNetStructure->TIDLLayers[k].outData[l].pitch[TIDL_ROI_PITCH];



            foundInData = 1;
          }
        }
      }
      if (foundInData == 0)
      {
        printf("Could not find Indata for data ID %d \n", tIDLNetStructure->TIDLLayers[i].inData[j].dataId);
      }
    }
  }
}



int32_t tidltb_isOutDataBuff(sTIDL_Network_t *pTIDLNetStructure, int32_t dataId,
  int32_t layersGroupId)
{
  int32_t i, j;
  for (i = 0; i < pTIDLNetStructure->numLayers; i++)
  {
    for (j = 0; j < pTIDLNetStructure->TIDLLayers[i].numInBufs; j++)
    {
      if ((pTIDLNetStructure->TIDLLayers[i].layersGroupId != layersGroupId) &&
        (pTIDLNetStructure->TIDLLayers[i].inData[j].dataId == dataId))
      {
        return 1;
      }
    }
  }
  return 0;
}

int32_t tidltb_isInDataBuff(sTIDL_Network_t * pTIDLNetStructure, int32_t dataId,
  int32_t layersGroupId)
{
  int32_t i, j;
  for (i = 0; i < pTIDLNetStructure->numLayers; i++)
  {
    for (j = 0; j < pTIDLNetStructure->TIDLLayers[i].numInBufs; j++)
    {
      if ((pTIDLNetStructure->TIDLLayers[i].layersGroupId == layersGroupId) &&
        (pTIDLNetStructure->TIDLLayers[i].inData[j].dataId == dataId))
      {
        return 1;
      }
    }
  }
  return 0;
}
void TIDL_writeNInts(FILE * fp1, int32_t * intData, int32_t n, const char * name)
{
  fprintf(fp1, "%s = ", name);
  for (int32_t i = 0; i < n; i++)
  {
    fprintf(fp1, "%8d ", intData[i]);
  }
  fprintf(fp1, "\n");
}

int32_t TIDL_tfOutReshapeDataLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeConvLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.convParams;

  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;
  if (convParams.enableRelU)
  {
    TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;
  }
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
  TIDLPCLayers.outData[0].dimValues[1] = convParams.numOutChannels;
  TIDLPCLayers.outData[0].dimValues[2] = ((TIDLPCLayers.inData[0].dimValues[2] + (convParams.padH * 2) -
    ((convParams.kernelH - 1)* convParams.dilationH + 1)) / convParams.strideH) + 1;
  TIDLPCLayers.outData[0].dimValues[3] = ((TIDLPCLayers.inData[0].dimValues[3] + (convParams.padW * 2) -
    ((convParams.kernelW - 1)* convParams.dilationW + 1)) / convParams.strideW) + 1;

  convParams.numInChannels = TIDLPCLayers.inData[0].dimValues[1];

  TIDLPCLayers.numMacs =
    (int64_t)(((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
      TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3] *
      convParams.kernelW *convParams.kernelH *
      TIDLPCLayers.inData[0].dimValues[1]) / convParams.numGroups);

  return TIDL_IMPORT_NO_ERR;
}

#if 0
int32_t TIDL_tfOutReshapeResize(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.convParams;

  TIDLPCLayers.outData[0].elementType = TIDLPCLayers.inData[0].elementType;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
  TIDLPCLayers.outData[0].dimValues[1] = TIDLPCLayers.inData[0].dimValues[1];
  TIDLPCLayers.outData[0].dimValues[2] = TIDLPCLayers.inData[0].dimValues[2] * pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.resizeParams.resizeRatio[2];
  TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[3] * pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.resizeParams.resizeRatio[3];

  TIDLPCLayers.numMacs =
    (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
      TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3] * 4);
  return 0;
}
#endif

int32_t TIDL_tfOutReshapePoolingLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_PoolingParams_t &poolParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams;
  TIDLPCLayers.outData[0].elementType = TIDLPCLayers.inData[0].elementType;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  if (poolParams.kernelH > 0 || poolParams.kernelW > 0)
  {
    TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
    TIDLPCLayers.outData[0].dimValues[1] = TIDLPCLayers.inData[0].dimValues[1];
    TIDLPCLayers.outData[0].dimValues[2] = (((TIDLPCLayers.inData[0].dimValues[2] +
      poolParams.padH*2.0) - (poolParams.kernelH)) / poolParams.strideH) + 1;
    TIDLPCLayers.outData[0].dimValues[3] = (((TIDLPCLayers.inData[0].dimValues[3] +
      poolParams.padW*2.0) - (poolParams.kernelW)) / poolParams.strideW) + 1;
    TIDLPCLayers.numMacs =
      (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
        TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3] *
        poolParams.kernelW *poolParams.kernelH);
  }
  else
  {
    TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
    TIDLPCLayers.outData[0].dimValues[1] = TIDLPCLayers.inData[0].dimValues[1];
    TIDLPCLayers.outData[0].dimValues[2] = 1;
    TIDLPCLayers.outData[0].dimValues[3] = 1;
    TIDLPCLayers.numMacs =
      (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
        TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3]);
  }

  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams.numChannels =  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dimValues[1];

  return TIDL_IMPORT_NO_ERR;
}
int32_t TIDL_tfOutReshapeIdentity(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  TIDLPCLayers.outData[0].elementType = TIDLPCLayers.inData[0].elementType;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
  TIDLPCLayers.outData[0].dimValues[1] = TIDLPCLayers.inData[0].dimValues[1];
  TIDLPCLayers.outData[0].dimValues[2] = TIDLPCLayers.inData[0].dimValues[2];
  TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[3];
  TIDLPCLayers.numMacs =
    (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
      TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3]);
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeBN(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  TIDL_tfOutReshapeIdentity(pOrgTIDLNetStructure, layerIndex);
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  if (TIDLPCLayers.layerParams.batchNormParams.enableRelU)
  {
    TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;
  }

  TIDLPCLayers.layerParams.batchNormParams.numChannels = TIDLPCLayers.inData[0].dimValues[1];

  return TIDL_IMPORT_NO_ERR;
}


int32_t TIDL_tfOutReshapeRelu(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  TIDL_tfOutReshapeIdentity(pOrgTIDLNetStructure, layerIndex);
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeSoftmax(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  TIDL_tfOutReshapeIdentity(pOrgTIDLNetStructure, layerIndex);
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeIPLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_InnerProductParams_t &innerProductParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.innerProductParams;

  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
  TIDLPCLayers.outData[0].dimValues[1] =  1;
  TIDLPCLayers.outData[0].dimValues[2] =  1;
  TIDLPCLayers.outData[0].dimValues[3] = innerProductParams.numOutNodes;

  TIDLPCLayers.numMacs =
    (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * (innerProductParams.numOutNodes* innerProductParams.numInNodes + innerProductParams.numOutNodes));
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeDeConvLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  return -1; 
}

int32_t TIDL_tfOutReshapeConcatLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  int32_t j;
  TIDLPCLayers.outData[0].dimValues[1] = 0;
  TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;
  for (j = 0; j < TIDLPCLayers.numInBufs; j++)
  {
    if (TIDLPCLayers.inData[j].elementType == TIDL_SignedChar)
    {
      TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;
    }
    TIDLPCLayers.outData[0].dimValues[1] += TIDLPCLayers.inData[j].dimValues[1];
  }
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
  TIDLPCLayers.outData[0].dimValues[2] = TIDLPCLayers.inData[0].dimValues[2];
  TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[3];
  TIDLPCLayers.numMacs =
    (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
      TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3]);

  return TIDL_IMPORT_NO_ERR;
}
int32_t TIDL_tfOutReshapeSliceLayre(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  int32_t j;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  for (j = 0; j < TIDLPCLayers.numOutBufs; j++)
  {
    TIDLPCLayers.outData[j].elementType = TIDLPCLayers.inData[0].elementType;
    TIDLPCLayers.outData[j].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
    TIDLPCLayers.outData[j].dimValues[2] = TIDLPCLayers.inData[0].dimValues[2];
    TIDLPCLayers.outData[j].dimValues[3] = TIDLPCLayers.inData[0].dimValues[3];
    TIDLPCLayers.outData[j].dimValues[1] = TIDLPCLayers.layerPCParams.sliceParams.sliceNumChs[j];
  }
  TIDLPCLayers.numMacs = 0;

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeCropLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  return -1;
}

int32_t TIDL_tfOutReshapeFlattenLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  TIDLPCLayers.outData[0].elementType = TIDLPCLayers.inData[0].elementType;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
  TIDLPCLayers.outData[0].dimValues[1] = 1;
  TIDLPCLayers.outData[0].dimValues[2] = 1;
  TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[1] *
    TIDLPCLayers.inData[0].dimValues[2] *
    TIDLPCLayers.inData[0].dimValues[3];
  TIDLPCLayers.numMacs =
    (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
      TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3]);
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeArgmaxLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0];
  TIDLPCLayers.outData[0].dimValues[1] = 1;
  TIDLPCLayers.outData[0].dimValues[2] = TIDLPCLayers.inData[0].dimValues[2];
  TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[3];
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.argMaxParams.numChannels = TIDLPCLayers.inData[0].dimValues[1];

  TIDLPCLayers.numMacs =
    (int64_t)((int64_t)TIDLPCLayers.outData[0].dimValues[0] * TIDLPCLayers.outData[0].dimValues[1] *
      TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3]);
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapePadLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;
  TIDLPCLayers.outData[0].numDim = TIDLPCLayers.inData[0].numDim;
  TIDLPCLayers.outData[0].dimValues[0] = TIDLPCLayers.inData[0].dimValues[0]
    + TIDLPCLayers.layerPCParams.padParams.padTensor[0] + TIDLPCLayers.layerPCParams.padParams.padTensor[1];

  if (gloab_data_format == 0)
  {
    TIDLPCLayers.outData[0].dimValues[1] = TIDLPCLayers.inData[0].dimValues[1]
      + TIDLPCLayers.layerPCParams.padParams.padTensor[3 * 2 + 0] + TIDLPCLayers.layerPCParams.padParams.padTensor[3 * 2 + 1];
    TIDLPCLayers.outData[0].dimValues[2] = TIDLPCLayers.inData[0].dimValues[2]
      + TIDLPCLayers.layerPCParams.padParams.padTensor[1 * 2 + 0] + TIDLPCLayers.layerPCParams.padParams.padTensor[1 * 2 + 1];
    TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[3]
      + TIDLPCLayers.layerPCParams.padParams.padTensor[2 * 2 + 0] + TIDLPCLayers.layerPCParams.padParams.padTensor[2 * 2 + 1];
  }
  else
  {
    TIDLPCLayers.outData[0].dimValues[1] = TIDLPCLayers.inData[0].dimValues[1]
      + TIDLPCLayers.layerPCParams.padParams.padTensor[1 * 2 + 0] + TIDLPCLayers.layerPCParams.padParams.padTensor[1 * 2 + 1];
    TIDLPCLayers.outData[0].dimValues[2] = TIDLPCLayers.inData[0].dimValues[2]
      + TIDLPCLayers.layerPCParams.padParams.padTensor[2 * 2 + 0] + TIDLPCLayers.layerPCParams.padParams.padTensor[2 * 2 + 1];
    TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[3]
      + TIDLPCLayers.layerPCParams.padParams.padTensor[3 * 2 + 0] + TIDLPCLayers.layerPCParams.padParams.padTensor[3 * 2 + 1];
  }
  TIDLPCLayers.numMacs = 0;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfOutReshapeDetOutLayer(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure, int32_t layerIndex)
{
  return -1;
}

/*==============================================================================
* Function purpose: for current layer, search from all mapped layers and find those
*   whose output is the input of current layer. Once a match is found, it will 
*   link the two layers: assign output data id of the found layer to input data 
*   id of current layer. 
==============================================================================*/
int32_t tidl_linkInputTensors(sTIDL_OrgNetwork_t  *pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i0, i1, i2;
  sTIDL_LayerPC_t *pCurrentLayer;
  sTIDL_LayerPC_t *pSearchLayer;

  pCurrentLayer = &pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  for (i0 = 0; i0 < pCurrentLayer->numInBufs; i0++)
  {
    for (i1 = layerIndex - 1; i1 >= 0; i1--)
    {
      pSearchLayer = &pOrgTIDLNetStructure->TIDLPCLayers[i1];
      for (i2 = 0; i2 < pSearchLayer->numOutBufs; i2++)
      {
        if (pSearchLayer->outConsumerLinked[i2] < pSearchLayer->outConsumerCnt[i2])
        {
          if (strcmp((const char *)pCurrentLayer->inDataNames[i0], 
                     (const char *)pSearchLayer->outDataNames[i2]) == 0)
          {
            pCurrentLayer->inData[i0].dataId = pSearchLayer->outData[i2].dataId;
            pSearchLayer->outConsumerLinked[i2]++;
          }
        }
      }
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

/*==============================================================================
* Function purpose: similar to tidl_linkInputTensors, except that it tries to 
*   match the output of current layer to the input of other layers. 
==============================================================================*/
int32_t tidl_linkOutputTensors(sTIDL_OrgNetwork_t  *pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i0, i1, i2;
  for (i0 = 0; i0 < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs; i0++)
  {
    for (i1 = layerIndex - 1; i1 >= 0; i1--)
    {
      for (i2 = 0; i2 < pOrgTIDLNetStructure->TIDLPCLayers[i1].numInBufs; i2++)
      {
        if (pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerLinked[i0] < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerCnt[i0])
        {
          if (strcmp((const char *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outDataNames[i0], 
                     (const char *)pOrgTIDLNetStructure->TIDLPCLayers[i1].inDataNames[i2]) == 0)
          {
            pOrgTIDLNetStructure->TIDLPCLayers[i1].inData[i2].dataId = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[i0].dataId;
            pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerLinked[i0]++;
          }
        }
      }
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_isAllInsAvailable(sTIDL_LayerPC_t  *orgLayer, sTIDL_OrgNetwork_t  *ptempTIDLNetStructure, int32_t layerIndex)
{
  int32_t i0, i1, i2;
  int32_t status = 0;
  int32_t availableIns = 0;
  for (i0 = 0; i0 < orgLayer->numInBufs; i0++)
  {
    for (i1 = 0; i1 < layerIndex; i1++)
    {
      for (i2 = 0; i2 < ptempTIDLNetStructure->TIDLPCLayers[i1].numOutBufs; i2++)
      {
        if (strcmp((const char *)ptempTIDLNetStructure->TIDLPCLayers[i1].outDataNames[i2], (const char *)orgLayer->inDataNames[i0]) == 0)
        {
          availableIns++;
        }
      }
    }
  }
  /* Is shall be orgLayer->numInBufs <= availableIns, temprary fix to get caffe import working
     TODO : need rever back after migatration caffe to new import framework */

  if ((orgLayer->numInBufs <= availableIns) || (orgLayer->numInBufs == -1))
  {
    status = 1;
  }
  return(status);
}

int32_t tidl_sortLayersInProcOrder(sTIDL_OrgNetwork_t  *pOrgTIDLNetStructure, sTIDL_OrgNetwork_t  *ptempTIDLNetStructure, int32_t numLayers)
{
  int32_t i0, i1, i2, status;
  int32_t newNetIdx, newNetIdxTemp;
  int32_t *isAddedToList = (int32_t *)my_malloc(numLayers*sizeof(int32_t));
  memset(isAddedToList, 0, sizeof(int32_t)*numLayers);

  status = TIDL_IMPORT_NO_ERR;

  /* Sort layers in processing order according to inputs and outputs of each layer */
  newNetIdx = 0;
  while (newNetIdx < numLayers)
  {
    /* For each layer, find the layer whose input is the output of this layer. */
    newNetIdxTemp = newNetIdx;
    for (i0 = 0; i0 < numLayers; i0++)
    {
      if (isAddedToList[i0] == 0)
      {
        if (tidl_isAllInsAvailable(&pOrgTIDLNetStructure->TIDLPCLayers[i0], ptempTIDLNetStructure, newNetIdx))
        {
          ptempTIDLNetStructure->TIDLPCLayers[newNetIdx] = pOrgTIDLNetStructure->TIDLPCLayers[i0];
          newNetIdx++;
          isAddedToList[i0] = 1;
        }
      }
    }

    /* If no matching layer is found among all layers, this topology is not supported by TIDL */
    if(newNetIdx == newNetIdxTemp)
    {
      /* Return error: network topology not supported */
      printf("Error in sorting layers: matching layer cannot be found!\n");
      status = TIDL_IMPORT_ERR_NET_TOPOLOTY_NOT_SUPPORTED;
      break;
    }
  }

  my_free(isAddedToList);
  ptempTIDLNetStructure->numLayers = newNetIdx;
  memset((void *)pOrgTIDLNetStructure, 0, sizeof(orgTIDLNetStructure));
  memcpy((void *)pOrgTIDLNetStructure, (void *)ptempTIDLNetStructure, sizeof(orgTIDLNetStructure));

  return status;
}

int32_t tidl_removeMergedLayersFromNet(sTIDL_OrgNetwork_t  *pOrgTIDLNetStructure, sTIDL_OrgNetwork_t  *ptempTIDLNetStructure, int32_t layerIndex)
{
  int32_t i0, i1, i2;
  int32_t newNetIdx = 0;

  for (i0 = 0; i0 < layerIndex; i0++)
  {
    if ((pOrgTIDLNetStructure->TIDLPCLayers[i0].numInBufs != -1) ||
      (pOrgTIDLNetStructure->TIDLPCLayers[i0].numOutBufs != -1))
    {
      // copy unmerged layers (both numInBufs and numOutBufs are set to -1 for merged layers) 
      ptempTIDLNetStructure->TIDLPCLayers[newNetIdx] = pOrgTIDLNetStructure->TIDLPCLayers[i0];
      newNetIdx++;
    }
  }
  ptempTIDLNetStructure->numLayers = newNetIdx;
  memset((void *)pOrgTIDLNetStructure, 0, sizeof(orgTIDLNetStructure));
  memcpy((void *)pOrgTIDLNetStructure, (void *)ptempTIDLNetStructure, sizeof(orgTIDLNetStructure));
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_upateAInDataId(sTIDL_OrgNetwork_t  *pOrgTIDLNetStructure, int32_t layerIndex, int32_t oldId, int32_t currId)
{
  int32_t i0, i1, i2, i3, i4;
  for (i3 = 0; i3 < layerIndex; i3++)
  {
    for (i4 = 0; i4 < pOrgTIDLNetStructure->TIDLPCLayers[i3].numInBufs; i4++)
    {
      if (pOrgTIDLNetStructure->TIDLPCLayers[i3].inData[i4].dataId == oldId)
      {
        pOrgTIDLNetStructure->TIDLPCLayers[i3].inData[i4].dataId = currId;
      }
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_sortDataIds(sTIDL_OrgNetwork_t  *pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i0, i1, i2, i3, i4;
  int32_t maxDataId = 0;
  int32_t currId = 0;
  int32_t oldId = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    for (i2 = 0; i2 < pOrgTIDLNetStructure->TIDLPCLayers[i1].numOutBufs; i2++)
    {
      maxDataId = pOrgTIDLNetStructure->TIDLPCLayers[i1].outData[i2].dataId >= maxDataId ? pOrgTIDLNetStructure->TIDLPCLayers[i1].outData[i2].dataId : maxDataId;
    }
  }
  maxDataId = maxDataId + 1;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    for (i2 = 0; i2 < pOrgTIDLNetStructure->TIDLPCLayers[i1].numOutBufs; i2++)
    {
      pOrgTIDLNetStructure->TIDLPCLayers[i1].outData[i2].dataId += maxDataId;
    }
    for (i2 = 0; i2 < pOrgTIDLNetStructure->TIDLPCLayers[i1].numInBufs; i2++)
    {
      pOrgTIDLNetStructure->TIDLPCLayers[i1].inData[i2].dataId += maxDataId;
    }
  }

  for (i1 = 0; i1 < layerIndex; i1++)
  {
    for (i2 = 0; i2 < pOrgTIDLNetStructure->TIDLPCLayers[i1].numOutBufs; i2++)
    {
      oldId = pOrgTIDLNetStructure->TIDLPCLayers[i1].outData[i2].dataId;
      pOrgTIDLNetStructure->TIDLPCLayers[i1].outData[i2].dataId = currId;
      tidl_upateAInDataId(pOrgTIDLNetStructure, layerIndex, oldId, currId);
      currId++;
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_updateOutDataShape(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t startIdx, int32_t layerIndex, sTIDL_tfOutRehapeMap_t * sTIDL_tfOutRehapeTable)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = startIdx; i1 < layerIndex; i1++)
  {
//    printf("In tidl_updateOutDataShape, i1 is %d\n", i1);
    status = sTIDL_tfOutRehapeTable[pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType].tidl_tfOutReshape(&pOrgTIDLNetStructure, i1);
    if (status != -1)
    {
      for (i2 = 0; i2 < pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs; i2++)
      {
        for (i3 = 0; i3 < layerIndex; i3++)
        {
          for (i4 = 0; i4 < pOrgTIDLNetStructure.TIDLPCLayers[i3].numInBufs; i4++)
          {
            if (pOrgTIDLNetStructure.TIDLPCLayers[i3].inData[i4].dataId == pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dataId)
            {
              pOrgTIDLNetStructure.TIDLPCLayers[i3].inData[i4] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[i2];
            }
          }

        }
      }
    }
  }
  return status;
}
int32_t tidl_getInLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex, int32_t dataId)
{
  int32_t i1, i2;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    for (i2 = 0; i2 < pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs; i2++)
    {
      if (pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dataId == dataId)
      {
        return (i1);
      }
    }
  }
  return (-1);
}
int32_t tidl_getOutLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex, int32_t dataId)
{
  int32_t i1, i2;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    for (i2 = 0; i2 < pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs; i2++)
    {
      if (pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[i2].dataId == dataId)
      {
        return (i1);
      }
    }
  }
  return (-1);
}
int32_t tidl_mergeFlattenLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_FlattenLayer)
    {
      int32_t  idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (idx == -1)
      {
        printf("Error in merging Flatten layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      if (((TIDLPCLayers.layerType == TIDL_InnerProductLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1)) || 
		((TIDLPCLayers.layerType == TIDL_PoolingLayer) && (TIDLPCLayers.layerParams.poolParams.poolingType == TIDL_AveragePooling)))
      {
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_mergeBiasLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_BiasLayer)
    {
      int32_t  idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (idx == -1)
      { /* Didn't find any layer whose output data ID is the same as this layer's input data ID.  */
        printf("Error in merging Bias layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_BIAS_NOT_MERGED;
      }
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      if ((TIDLPCLayers.layerType == TIDL_ConvolutionLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;
        if (TIDLPCLayers.layerParams.convParams.enableBias == 0)
        {
          TIDLPCLayers.layerParams.convParams.enableBias = 1;
          TIDLPCLayers.bias = pOrgTIDLNetStructure.TIDLPCLayers[i1].bias;
        }
        else
        {
          float * src = (float *)pOrgTIDLNetStructure.TIDLPCLayers[i1].bias.ptr;
          float * dst = (float *)TIDLPCLayers.bias.ptr;
          for (i2 = 0; i2 < TIDLPCLayers.bias.bufSize; i2++)
          {
            dst[i2] += src[i2];
          }
        }
      }
      else if((TIDLPCLayers.layerType == TIDL_InnerProductLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;
        float * src = (float *)pOrgTIDLNetStructure.TIDLPCLayers[i1].bias.ptr;
        float * dst = (float *)TIDLPCLayers.bias.ptr;
        for (i2 = 0; i2 < TIDLPCLayers.bias.bufSize; i2++)
        {
          dst[i2] += src[i2];
        }
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_mergePadLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  int32_t padW, padH;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_PadLayer)
    {
      int32_t  inIdx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (inIdx == -1)
      {
        printf("Error in merging Pad layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
      int32_t  outIdx = tidl_getOutLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0].dataId);
      if (outIdx == -1)
      {
        printf("Error in merging Pad layer: could not find output layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        return TIDL_IMPORT_ERR_OUTPUT_LAYER_NOT_FOUND;
      }

      sTIDL_LayerPC_t &TIDLPCLayersIn = pOrgTIDLNetStructure.TIDLPCLayers[inIdx];
      sTIDL_LayerPC_t &TIDLPCLayersOut = pOrgTIDLNetStructure.TIDLPCLayers[outIdx];

      if (gloab_data_format == 0)
      {
        padW = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerPCParams.padParams.padTensor[2 * 2 + 0];
        padH = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerPCParams.padParams.padTensor[1 * 2 + 0];
      }
      else
      {
        padW = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerPCParams.padParams.padTensor[3 * 2 + 0];
        padH = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerPCParams.padParams.padTensor[2 * 2 + 0];;
      }

      if ((TIDLPCLayersOut.layerType == TIDL_ConvolutionLayer) &&
        (pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0] == 1) &&
        (TIDLPCLayersIn.outConsumerCnt[0] == 1) &&
        (TIDLPCLayersOut.layerParams.convParams.strideW > 1) && 
        (TIDLPCLayersOut.layerParams.convParams.strideH > 1) &&
        ((TIDLPCLayersOut.layerParams.convParams.kernelW/2) == padW) &&
        ((TIDLPCLayersOut.layerParams.convParams.kernelH/2) == padH))
      {
        TIDLPCLayersIn.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;

        TIDLPCLayersOut.layerParams.convParams.padW = padW;
        TIDLPCLayersOut.layerParams.convParams.padH = padH;
        TIDLPCLayersOut.strideOffsetMethod = TIDL_strideOffsetTopLeft;

          //TIDLPCLayersIn.outData[0]        = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        TIDLPCLayersOut.inData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0];
        strcpy((char *)TIDLPCLayersOut.inDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;
      }
      else
      {
        printf("TIDL limitation: Currently PAD layer is supported if the following layer is convolution with stride > 1.\n");
        return TIDL_IMPORT_ERR_PAD_NOT_MERGEABLE;
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_mergePoolLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if ((pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_PoolingLayer) && 
	    (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.poolParams.strideW == 2) &&
	    (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.poolParams.strideH == 2) &&
	    (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.poolParams.kernelW == 2) &&
	    (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.poolParams.kernelH == 2))
    {
      int32_t  idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (idx == -1)
      {
        printf("Error in merging BatchNorm layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
	  
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      if ((TIDLPCLayers.layerType == TIDL_ConvolutionLayer) &&
          (TIDLPCLayers.outConsumerCnt[0] == 1) &&
	      ((TIDLPCLayers.outData[0].dimValues[2] % 2) == 0) && 
          ((TIDLPCLayers.outData[0].dimValues[3] % 2) == 0))
      {		  
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;

        TIDLPCLayers.layerParams.convParams.enablePooling = 1; 	
        TIDLPCLayers.layerParams.convParams.poolParams.poolingType = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.poolParams.poolingType;
        TIDLPCLayers.layerParams.convParams.poolParams.kernelW   = 2;
        TIDLPCLayers.layerParams.convParams.poolParams.kernelH   = 2;
        TIDLPCLayers.layerParams.convParams.poolParams.strideW   = 2;
        TIDLPCLayers.layerParams.convParams.poolParams.strideH   = 2;
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_mergeBNLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_BatchNormLayer)
    {
      int32_t  idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (idx == -1)
      {
        printf("Error in merging BatchNorm layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      if ((TIDLPCLayers.layerType == TIDL_ConvolutionLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;

        if (TIDLPCLayers.layerParams.convParams.enableBias == 0)
        {
          TIDLPCLayers.layerParams.convParams.enableBias = 1;
          TIDLPCLayers.bias.ptr = my_malloc(sizeof(float)*TIDLPCLayers.outData[0].dimValues[1]);
          TIDLPCLayers.bias.bufSize = TIDLPCLayers.outData[0].dimValues[1];
          float * dst = (float *)TIDLPCLayers.bias.ptr;
          for (i2 = 0; i2 < TIDLPCLayers.bias.bufSize; i2++)
          {
            dst[i2] = 0;
          }
        }
        /* Merge BN scale and Bias to Conv2d */
        float * weights = (float *)TIDLPCLayers.weights.ptr;
        float * bias = (float *)TIDLPCLayers.bias.ptr;

        float * scale = (float *)pOrgTIDLNetStructure.TIDLPCLayers[i1].weights.ptr;
        float * bias2 = (float *)pOrgTIDLNetStructure.TIDLPCLayers[i1].bias.ptr;
        int32_t weightsSize = (TIDLPCLayers.weights.bufSize / TIDLPCLayers.bias.bufSize);
        for (i2 = 0; i2 < TIDLPCLayers.bias.bufSize; i2++)
        {
          for (i3 = 0; i3 < weightsSize; i3++)
          {
            weights[i2*weightsSize + i3] *= scale[i2];
          }
          bias[i2] = bias[i2] * scale[i2] + bias2[i2];
        }
        my_free(scale);
        my_free(bias2);
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_mergeReluLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  int32_t merged;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_ReLULayer)
    {
      merged = 0;
      int32_t  idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (idx == -1)
      {
        printf("Error in merging ReLU layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      if ((TIDLPCLayers.layerType == TIDL_ConvolutionLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.layerParams.convParams.enableRelU = 1;
        TIDLPCLayers.layerParams.convParams.reluParams.reluType = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.reluParams.reluType;
        merged = 1;
      }
      if ((TIDLPCLayers.layerType == TIDL_EltWiseLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.layerParams.eltWiseParams.enableRelU = 1;
        TIDLPCLayers.layerParams.eltWiseParams.reluParams.reluType = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.reluParams.reluType;
        merged = 1;
      }
      if ((TIDLPCLayers.layerType == TIDL_BatchNormLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.layerParams.batchNormParams.enableRelU = 1;
        TIDLPCLayers.layerParams.batchNormParams.reluParams.reluType = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.reluParams.reluType;
        merged = 1;
      }
      if ((TIDLPCLayers.layerType == TIDL_InnerProductLayer) &&
        (TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.layerParams.innerProductParams.enableRelU = 1;
        TIDLPCLayers.layerParams.innerProductParams.reluParams.reluType = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.reluParams.reluType;
        merged = 1;
      }	  
      if (merged == 1)
      {
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_mergeDropoutLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_DropOutLayer)
    {
      int32_t  idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (idx == -1)
      {
        printf("Error in merging Dropout layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      if ((TIDLPCLayers.outConsumerCnt[0] == 1))
      {
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_mergeReshapeLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex, sTIDL_tfOutRehapeMap_t * sTIDL_tfOutRehapeTable)
{
  int32_t i1, i2, i3, i4;
  int32_t status = 0;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_ReshapeLayer)
    {
      int32_t  idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (idx == -1)
      {
        printf("Error in merging Reshape layer: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[idx];
      if ((TIDLPCLayers.layerType == TIDL_InnerProductLayer) ||
        ((TIDLPCLayers.layerType == TIDL_PoolingLayer) && (TIDLPCLayers.layerParams.poolParams.poolingType == TIDL_AveragePooling) && (TIDLPCLayers.outConsumerCnt[0] == 1)))
      {
        if (TIDLPCLayers.layerType == TIDL_PoolingLayer)
        {
          TIDLPCLayers.layerParams.poolParams.kernelW = 0;
          TIDLPCLayers.layerParams.poolParams.kernelH = 0;
        }
        TIDLPCLayers.numMacs += pOrgTIDLNetStructure.TIDLPCLayers[i1].numMacs;
        TIDLPCLayers.outData[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0];
        strcpy((char *)TIDLPCLayers.outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[0]);
        TIDLPCLayers.outConsumerCnt[0] = pOrgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[0];
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numInBufs = -1;
        pOrgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs = -1;
        
        TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.outData[0].dimValues[1] * TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3];
        TIDLPCLayers.outData[0].dimValues[2] = 1;
        TIDLPCLayers.outData[0].dimValues[1] = 1;
        sTIDL_LayerPC_t *TIDLPCLayersOut;
        int32_t  outIdx = tidl_getOutLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0].dataId);
        if (outIdx != -1)
        {
          TIDLPCLayersOut = &pOrgTIDLNetStructure.TIDLPCLayers[outIdx];
          TIDLPCLayersOut->inData[0] = TIDLPCLayers.outData[0];
          tidl_updateOutDataShape(pOrgTIDLNetStructure, outIdx, layerIndex, sTIDL_tfOutRehapeTable);
        }
      }
      else {
        printf("TIDL limitation: Reshape layer cannot be merged with layers other than InnerProduct or AveragePooling layer!\n");
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}


int32_t tidl_convertIpLayerInputShape(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_InnerProductLayer)
    {
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[i1];
      int32_t  inIdx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
      if (inIdx == -1)
      {
        printf("Error in converting InnerProduct layer input shape: could not find input layer: %s!\n",
               pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
        return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
      }
      sTIDL_LayerPC_t &TIDLPCLayersIn = pOrgTIDLNetStructure.TIDLPCLayers[inIdx];

      if ((TIDLPCLayersIn.layerType == TIDL_PoolingLayer) && (TIDLPCLayersIn.layerParams.poolParams.poolingType == TIDL_AveragePooling) && (TIDLPCLayersIn.outConsumerCnt[0] == 1))
      {
        if (TIDLPCLayersIn.layerType == TIDL_PoolingLayer)
        {
          TIDLPCLayersIn.layerParams.poolParams.kernelW = 0;
          TIDLPCLayersIn.layerParams.poolParams.kernelH = 0;
        }

        TIDLPCLayersIn.outData[0].dimValues[3] = TIDLPCLayersIn.outData[0].dimValues[1] * TIDLPCLayersIn.outData[0].dimValues[2] * TIDLPCLayersIn.outData[0].dimValues[3];
        TIDLPCLayers.inData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[1] * TIDLPCLayers.inData[0].dimValues[2] * TIDLPCLayers.inData[0].dimValues[3];
        TIDLPCLayersIn.outData[0].dimValues[1] = 1;
        TIDLPCLayersIn.outData[0].dimValues[2] = 1;
        TIDLPCLayers.inData[0].dimValues[1]    = 1;
        TIDLPCLayers.inData[0].dimValues[2]    = 1;

      }
      else
      {
        if ((TIDLPCLayersIn.outData[0].dimValues[1] != 1) || (TIDLPCLayersIn.outData[0].dimValues[2] != 1))
        {
          printf("TIDL limitation: Input of TIDL_InnerProductLayer layer needs to be flattened. Please add Flatten layer to import this model. \n");
          return TIDL_IMPORT_ERR_IP_INPUT_NOT_FLATTENED;
        }
      }
    }
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_convertConv2DToIpLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex, sTIDL_tfOutRehapeMap_t * sTIDL_tfOutRehapeTable)
{
  int32_t i1, i2, i3, i4;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_ConvolutionLayer)
    {
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[i1];
      sTIDL_ConvParams_t &convParams = pOrgTIDLNetStructure.TIDLPCLayers[i1].layerParams.convParams;
      if ((convParams.kernelW == 1) && (convParams.kernelH == 1) && (TIDLPCLayers.inData[0].dimValues[2] == 1) && (TIDLPCLayers.inData[0].dimValues[3] == 1))
      {
        int32_t  inIdx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].inData[0].dataId);
        if (inIdx == -1)
        {
          printf("Error in converting Conv2d layer to InnerProduct layer: could not find input layer: %s!\n",
                 pOrgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[0]);
          return TIDL_IMPORT_ERR_INPUT_LAYER_NOT_FOUND;
        }
        sTIDL_LayerPC_t &TIDLPCLayersIn = pOrgTIDLNetStructure.TIDLPCLayers[inIdx];

        if ((TIDLPCLayersIn.layerType == TIDL_PoolingLayer) && (TIDLPCLayersIn.layerParams.poolParams.poolingType == TIDL_AveragePooling) && (TIDLPCLayersIn.outConsumerCnt[0] == 1))
        {
          sTIDL_LayerPC_t *TIDLPCLayersOut;
          int32_t  outIdx = tidl_getOutLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i1].outData[0].dataId);
          if (outIdx != -1)
          {
            TIDLPCLayersOut = &pOrgTIDLNetStructure.TIDLPCLayers[outIdx];
          }
          if ((outIdx == -1) || (TIDLPCLayersOut->layerType == TIDL_InnerProductLayer) ||
            (TIDLPCLayersOut->layerType == TIDL_SoftMaxLayer) || (TIDLPCLayersOut->layerType == TIDL_FlattenLayer) || (TIDLPCLayersOut->layerType == TIDL_ReshapeLayer))
          {
            TIDLPCLayersIn.layerParams.poolParams.kernelW = 0;
            TIDLPCLayersIn.layerParams.poolParams.kernelH = 0;

            sTIDL_LayerPC_t TIDLPCLayerstemp = TIDLPCLayers;
            TIDLPCLayers.layerType = TIDL_InnerProductLayer;
            TIDLPCLayers.inData[0].dimValues[3] = TIDLPCLayers.inData[0].dimValues[1] * TIDLPCLayers.inData[0].dimValues[2] * TIDLPCLayers.inData[0].dimValues[3];
            TIDLPCLayers.inData[0].dimValues[2] = 1;
            TIDLPCLayers.inData[0].dimValues[1] = 1;
            TIDLPCLayersIn.outData[0] = TIDLPCLayers.inData[0];

            TIDLPCLayers.outData[0].dimValues[3] = TIDLPCLayers.outData[0].dimValues[1] * TIDLPCLayers.outData[0].dimValues[2] * TIDLPCLayers.outData[0].dimValues[3];
            TIDLPCLayers.outData[0].dimValues[2] = 1;
            TIDLPCLayers.outData[0].dimValues[1] = 1;
            TIDLPCLayersOut->inData[0] = TIDLPCLayers.outData[0];
            tidl_updateOutDataShape(pOrgTIDLNetStructure, outIdx, layerIndex, sTIDL_tfOutRehapeTable);

            TIDLPCLayers.layerParams.innerProductParams.numInNodes = TIDLPCLayers.inData[0].dimValues[3];
            TIDLPCLayers.layerParams.innerProductParams.numOutNodes = TIDLPCLayers.outData[0].dimValues[3];
            TIDLPCLayers.layerParams.innerProductParams.enableRelU = TIDLPCLayerstemp.layerParams.convParams.enableRelU;
          }
        }
      }
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_convertRelUToBNLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i1, i2, i3, i4;
  for (i1 = 0; i1 < layerIndex; i1++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_ReLULayer)
    {
      sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure.TIDLPCLayers[i1];
      pOrgTIDLNetStructure.TIDLPCLayers[i1].layerType = TIDL_BatchNormLayer;
      TIDLPCLayers.outData[0].elementType = TIDL_UnsignedChar;

      int32_t dataSize = TIDLPCLayers.outData[0].dimValues[1];

      TIDLPCLayers.weights.ptr = my_malloc(dataSize*sizeof(float));
      TIDLPCLayers.weights.bufSize = dataSize;
      TIDLPCLayers.bias.ptr = my_malloc(dataSize*sizeof(float));
      TIDLPCLayers.bias.bufSize = dataSize;
      float * scalePtr = (float*)TIDLPCLayers.weights.ptr;
      float * biasPtr  = (float*)TIDLPCLayers.bias.ptr;

      for (i2 = 0; i2 < dataSize; i2++)
      {
        scalePtr[i2] = 1;
        biasPtr[i2]  = 0;
      }
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

// Quantize and write parameters out to file
void TIDL_importQuantWriteLayerParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
                                      int32_t              numLayers,
                                      FILE                 *fp1
                                     )
{
  int32_t zeroWeightValue, i;
  sTIDL_LayerPC_t *pTIDLPCLayers;

  for(i=0; i<numLayers; i++)
  {
    pTIDLPCLayers = &(pOrgTIDLNetStructure->TIDLPCLayers[i]);

    if((pTIDLPCLayers->layerType == TIDL_ConvolutionLayer)  ||
       (pTIDLPCLayers->layerType == TIDL_InnerProductLayer) ||
       (pTIDLPCLayers->layerType == TIDL_BatchNormLayer))
    {
      float min = FLT_MAX;
      float max = FLT_MIN;
      int32_t weightsElementSizeInBits = pTIDLPCLayers->weightsElementSizeInBits;

      if(pTIDLPCLayers->layerType == TIDL_ConvolutionLayer)
      {
        float *  data     = (float *)pTIDLPCLayers->weights.ptr;
        uint32_t dataSize = pTIDLPCLayers->weights.bufSize;
        uint8_t * params  = (uint8_t *)malloc(dataSize * ((weightsElementSizeInBits-1)/8 + 1));
        TIDL_findRange(data, dataSize, &min , &max, 1.0);

        pTIDLPCLayers->layerParams.convParams.weightsQ = 
          TIDL_QuantizeUnsignedMax((uint8_t *)params, data,dataSize, min , max, 
                                   NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
        pTIDLPCLayers->layerParams.convParams.zeroWeightValue = zeroWeightValue;

        if(weightsElementSizeInBits > 8)
          fwrite(params,2,dataSize,fp1);
        else
          fwrite(params,1,dataSize,fp1);

        free(params);
        free(data);

        // bufSize is not needed by calibration - below is from ti_dl\test\src\tidl_tb.c:
        //   dataSize = (conv2dPrms->numInChannels* conv2dPrms->numOutChannels* 
        //              conv2dPrms->kernelW*conv2dPrms->kernelH)/conv2dPrms->numGroups;
        //   FREAD((int8_t *)conv2dPrms->weights.ptr,1,2*dataSize, fp1);
        pTIDLPCLayers->weights.ptr = NULL;
        pTIDLPCLayers->weights.bufSize = 0;

        if(pTIDLPCLayers->layerParams.convParams.enableBias)
        {
          min = FLT_MAX;
          max = FLT_MIN;
          {
            float * biasData = (float *)pTIDLPCLayers->bias.ptr;
            uint32_t biasDataSize = pTIDLPCLayers->bias.bufSize;
            TIDL_findRange(biasData, biasDataSize, &min, &max, 1.0);
          }

          data = (float *)pTIDLPCLayers->bias.ptr;
          dataSize = pTIDLPCLayers->bias.bufSize;
          max = abs(min) >  abs(max) ? abs(min) : abs(max);
          pTIDLPCLayers->layerParams.convParams.biasQ =
            TIDL_QuantizeUnsignedMax(0, 0, 0, 0, max, NUM_BIAS_BITS, (NUM_BIAS_BYTES * 8), &zeroWeightValue);

          int16_t * params = (int16_t *)malloc(dataSize*NUM_BIAS_BYTES);
          for (int idx = 0; idx < dataSize; idx++) 
          {
            int32_t biasParam = TIDL_normalize(data[idx], 0 , max);
            params[idx] = (int16_t)TIDL_roundSat(biasParam,0,SHRT_MIN,SHRT_MAX);
          }
          fwrite(params, NUM_BIAS_BYTES,dataSize,fp1);
          free(params);
          free(data);
          pTIDLPCLayers->bias.ptr = NULL;
          pTIDLPCLayers->bias.bufSize = 0;
        }
        if (pTIDLPCLayers->layerParams.convParams.biasQ == 0)
        {
          pTIDLPCLayers->layerParams.convParams.biasQ = 1;
        }
      }
      else if(pTIDLPCLayers->layerType == TIDL_InnerProductLayer)
      {
        float *  data     = (float *)pTIDLPCLayers->weights.ptr;
        uint32_t dataSize = pTIDLPCLayers->weights.bufSize;
        uint8_t * params = (uint8_t *)malloc(dataSize * ((weightsElementSizeInBits-1)/8 + 1));
        TIDL_findRange(data, dataSize, &min , &max, 1.0);
        pTIDLPCLayers->layerParams.innerProductParams.weightsQ = 
          TIDL_QuantizeUnsignedMax((uint8_t *)params, data,dataSize, min , max,  NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
        pTIDLPCLayers->layerParams.innerProductParams.zeroWeightValue = zeroWeightValue;

        if(weightsElementSizeInBits > 8)
          fwrite(params,2,dataSize,fp1);
        else
          fwrite(params,1,dataSize,fp1);

        free(params);
        free(data);
        pTIDLPCLayers->weights.ptr = NULL;
        pTIDLPCLayers->weights.bufSize = 0;

        min = FLT_MAX;
        max = FLT_MIN;

        {
          float * biasData = (float *)(pTIDLPCLayers->bias.ptr);
          uint32_t biasDataSize = pTIDLPCLayers->bias.bufSize;
          TIDL_findRange(biasData, biasDataSize, &min, &max, 1.0 );
        }

        max = abs(min) >  abs(max) ? abs(min) : abs(max);
        pTIDLPCLayers->layerParams.innerProductParams.biasQ =
          TIDL_QuantizeUnsignedMax(0, 0, 0, 0, max, NUM_BIAS_BITS, (NUM_BIAS_BYTES * 8), &zeroWeightValue);

        data     = (float *)pTIDLPCLayers->bias.ptr;
        dataSize = pTIDLPCLayers->bias.bufSize;
        {
          int16_t *params = (int16_t *)malloc(dataSize*NUM_BIAS_BYTES);
          for (int idx = 0; idx < dataSize; idx++) 
          {
            int32_t biasParam = TIDL_normalize(data[idx], 0 , max);
            params[idx] = (int16_t)TIDL_roundSat(biasParam,0,SHRT_MIN,SHRT_MAX);
          }
          fwrite(params, NUM_BIAS_BYTES,dataSize,fp1);
          free(params);
        }
        free(data);
        pTIDLPCLayers->bias.ptr = NULL;
        pTIDLPCLayers->bias.bufSize = 0;

        if (pTIDLPCLayers->layerParams.innerProductParams.biasQ == 0)
        {
          pTIDLPCLayers->layerParams.innerProductParams.biasQ = 1;
        }
      }
      else if (pTIDLPCLayers->layerType == TIDL_BatchNormLayer)
      {
        float *  data = (float *)pTIDLPCLayers->weights.ptr;
        uint32_t dataSize = pTIDLPCLayers->weights.bufSize;
        uint8_t * params = (uint8_t *)malloc(dataSize * ((weightsElementSizeInBits - 1) / 8 + 1));
        TIDL_findRange(data, dataSize, &min, &max, 1.0);

        pTIDLPCLayers->layerParams.batchNormParams.weightsQ =
          TIDL_QuantizeUnsignedMax((uint8_t *)params, data, dataSize, min, max, NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
        pTIDLPCLayers->layerParams.batchNormParams.zeroWeightValue = zeroWeightValue;

        if (weightsElementSizeInBits > 8)
          fwrite(params, 2, dataSize, fp1);
        else
          fwrite(params, 1, dataSize, fp1);

        free(params);
        free(data);
        pTIDLPCLayers->weights.ptr = NULL;
        pTIDLPCLayers->weights.bufSize = 0;

        {
          min = FLT_MAX;
          max = FLT_MIN;
          {
            float * biasData = (float *)pTIDLPCLayers->bias.ptr;
            uint32_t biasDataSize = pTIDLPCLayers->bias.bufSize;
            TIDL_findRange(biasData, biasDataSize, &min, &max, 1.0);
          }

          data = (float *)pTIDLPCLayers->bias.ptr;
          dataSize = pTIDLPCLayers->bias.bufSize;
          max = abs(min) > abs(max) ? abs(min) : abs(max);
          pTIDLPCLayers->layerParams.batchNormParams.biasQ =
            TIDL_QuantizeUnsignedMax(0, 0, 0, 0, max, NUM_BIAS_BITS, (NUM_BIAS_BYTES * 8), &zeroWeightValue);

          int16_t * params = (int16_t *)malloc(dataSize*NUM_BIAS_BYTES);
          for (int idx = 0; idx < dataSize; idx++)
          {
            int32_t biasParam = TIDL_normalize(data[idx], 0, max);
            params[idx] = (int16_t)TIDL_roundSat(biasParam, 0, SHRT_MIN, SHRT_MAX);
          }
          fwrite(params, NUM_BIAS_BYTES, dataSize, fp1);
          free(params);
          free(data);
          if (pTIDLPCLayers->layerParams.batchNormParams.biasQ == 0)
          {
            pTIDLPCLayers->layerParams.batchNormParams.biasQ = 1;
          }
          pTIDLPCLayers->bias.ptr = NULL;
          pTIDLPCLayers->bias.bufSize = 0;
        }

        if (pTIDLPCLayers->layerParams.batchNormParams.reluParams.reluType == TIDL_PRelU)
        {
          float * slopeData = (float *)pTIDLPCLayers->slope.ptr;
          uint32_t slopeDataSize = pTIDLPCLayers->slope.bufSize;
          uint8_t * params = (uint8_t *)malloc(slopeDataSize * ((weightsElementSizeInBits - 1) / 8 + 1));
          float min = FLT_MAX;
          float max = FLT_MIN;
          TIDL_findRange(slopeData, slopeDataSize, &min, &max, (1.0));
          pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ =
            TIDL_QuantizeUnsignedMax((uint8_t *)params, slopeData, slopeDataSize, min, max, NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
          pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ /= 256;
          pTIDLPCLayers->layerParams.batchNormParams.reluParams.zeroSlopeValue = zeroWeightValue;

          if (weightsElementSizeInBits > 8)
          {
            fwrite(params, 2, slopeDataSize, fp1);
          }
          else
          {
            fwrite(params, 1, slopeDataSize, fp1);
          }
          free(params);
          free(slopeData);
          pTIDLPCLayers->slope.ptr = NULL;
          pTIDLPCLayers->slope.bufSize = 0;
          if (pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ == 0)
          {
            pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ = 1;
          }
        }
      }
    }

  }  // for(i=0; i<numLayers; i++)
} //TIDL_importQuantWriteLayerParams

// Debugging: write out parameters in text format
void TIDL_importWriteParamsQuant8(FILE *fp, uint8_t *data, uint32_t size)
{
    int32_t i;

    for(i=0; i<size; i++)
    {
        fprintf(fp, "%d\n", (int)data[i]);
    }
}

void TIDL_importWriteParamsQuant16(FILE *fp, int16_t *data, uint32_t size)
{
    int32_t i;

    for(i=0; i<size; i++)
    {
        fprintf(fp, "%d\n", (int)data[i]);
    }
}

void TIDL_importWriteParams(FILE *fp, float *data, uint32_t size)
{
    int32_t i;

    for(i=0; i<size; i++)
    {
        fprintf(fp, "%f\n", data[i]);
    }
}
void tidl_importEltWiseParams(sTIDL_OrgNetwork_t  *pOrgTIDLNetStructure, int32_t layerIndex)
{
    int32_t i;
    
    for(i=0; i<layerIndex; i++)
    {
      if(pOrgTIDLNetStructure->TIDLPCLayers[i].layerType == TIDL_EltWiseLayer) {
        pOrgTIDLNetStructure->TIDLPCLayers[i].layerParams.eltWiseParams.numChannels 
            = pOrgTIDLNetStructure->TIDLPCLayers[i].outData[0].dimValues[1];
      }
    }
}
void TIDL_importQuantWriteLayerParamsDbg(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
                                         int32_t              numLayers,
                                         FILE                 *fp1)
{
  int32_t zeroWeightValue, i;
  sTIDL_LayerPC_t *pTIDLPCLayers;
  FILE *fpConvParams;
  FILE *fpIpParams;
  FILE *fpBnParams;
  FILE *fpConvParamsQuant;
  FILE *fpIpParamsQuant;
  FILE *fpBnParamsQuant;

  fpConvParams = fopen("convParams.dat", "w");
  fpIpParams = fopen("innerProdParams.dat", "w");
  fpBnParams = fopen("batchNormParams.dat", "w");
  fpConvParamsQuant = fopen("convParamsQuant.dat", "w");
  fpIpParamsQuant = fopen("innerProdParamsQuant.dat", "w");
  fpBnParamsQuant = fopen("batchNormParamsQuant.dat", "w");

  for(i=0; i<numLayers; i++)
  {
    pTIDLPCLayers = &(pOrgTIDLNetStructure->TIDLPCLayers[i]);

    if((pTIDLPCLayers->layerType == TIDL_ConvolutionLayer)  ||
       (pTIDLPCLayers->layerType == TIDL_InnerProductLayer) ||
       (pTIDLPCLayers->layerType == TIDL_BatchNormLayer))
    {
      float min = FLT_MAX;
      float max = FLT_MIN;
      int32_t weightsElementSizeInBits = pTIDLPCLayers->weightsElementSizeInBits;

      if(pTIDLPCLayers->layerType == TIDL_ConvolutionLayer)
      {
        float *  data     = (float *)pTIDLPCLayers->weights.ptr;
        uint32_t dataSize = pTIDLPCLayers->weights.bufSize;

        TIDL_importWriteParams(fpConvParams, data, dataSize);

        uint8_t * params  = (uint8_t *)malloc(dataSize * ((weightsElementSizeInBits-1)/8 + 1));
        TIDL_findRange(data, dataSize, &min , &max, 1.0);

        pTIDLPCLayers->layerParams.convParams.weightsQ = 
          TIDL_QuantizeUnsignedMax((uint8_t *)params, data,dataSize, min , max,  
                                   NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
        pTIDLPCLayers->layerParams.convParams.zeroWeightValue = zeroWeightValue;

        fprintf(fpConvParamsQuant, "Layer %d, Conv layer weights, number of params:  %d\n", i, dataSize);
        if(weightsElementSizeInBits == 8) {
          TIDL_importWriteParamsQuant8(fpConvParamsQuant, params, dataSize);
        }
        else {
          TIDL_importWriteParamsQuant16(fpConvParamsQuant, (int16_t *)params, dataSize);
        }

        printf("In TIDL_QuantizeUnsignedMax, weightsElementSizeInBits is %d.\n", weightsElementSizeInBits);
        if(weightsElementSizeInBits > 8)
          fwrite(params,2,dataSize,fp1);
        else
          fwrite(params,1,dataSize,fp1);

        free(params);
        free(data);

        pTIDLPCLayers->weights.ptr = NULL;
        pTIDLPCLayers->weights.bufSize = 0;

        if(pTIDLPCLayers->layerParams.convParams.enableBias)
        {
          printf("In TIDL_QuantizeUnsignedMax, enableBias is TRUE.\n");

          min = FLT_MAX;
          max = FLT_MIN;
          {
            float * biasData = (float *)pTIDLPCLayers->bias.ptr;
            uint32_t biasDataSize = pTIDLPCLayers->bias.bufSize;
            TIDL_findRange(biasData, biasDataSize, &min, &max, 1.0);
          }

          data = (float *)pTIDLPCLayers->bias.ptr;
          dataSize = pTIDLPCLayers->bias.bufSize;

          TIDL_importWriteParams(fpConvParams, data, dataSize);

          max = abs(min) >  abs(max) ? abs(min) : abs(max);
          pTIDLPCLayers->layerParams.convParams.biasQ =
            TIDL_QuantizeUnsignedMax(0, 0, 0, 0, max, NUM_BIAS_BITS, (NUM_BIAS_BYTES * 8), &zeroWeightValue);

          int16_t * params = (int16_t *)malloc(dataSize*NUM_BIAS_BYTES);
          for (int idx = 0; idx < dataSize; idx++) 
          {
            int32_t biasParam = TIDL_normalize(data[idx], 0 , max);
            params[idx] = (int16_t)TIDL_roundSat(biasParam,0,SHRT_MIN,SHRT_MAX);
          }

          fprintf(fpConvParamsQuant, "Layer %d, Conv layer bias, number of params:  %d\n", i, dataSize);
          TIDL_importWriteParamsQuant16(fpConvParamsQuant, params, dataSize);

          fwrite(params, NUM_BIAS_BYTES,dataSize,fp1);
          free(params);
          free(data);
          pTIDLPCLayers->bias.ptr = NULL;
          pTIDLPCLayers->bias.bufSize = 0;
        }
        if (pTIDLPCLayers->layerParams.convParams.biasQ == 0)
        {
          pTIDLPCLayers->layerParams.convParams.biasQ = 1;
        }
      }
      else if(pTIDLPCLayers->layerType == TIDL_InnerProductLayer)
      {
        float *  data     = (float *)pTIDLPCLayers->weights.ptr;
        uint32_t dataSize = pTIDLPCLayers->weights.bufSize;

        TIDL_importWriteParams(fpIpParams, data, dataSize);

        uint8_t * params = (uint8_t *)malloc(dataSize * ((weightsElementSizeInBits-1)/8 + 1));
        TIDL_findRange(data, dataSize, &min , &max, 1.0);
        pTIDLPCLayers->layerParams.innerProductParams.weightsQ = 
          TIDL_QuantizeUnsignedMax((uint8_t *)params, data,dataSize, min , max,  NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
        pTIDLPCLayers->layerParams.innerProductParams.zeroWeightValue = zeroWeightValue;

        if(weightsElementSizeInBits > 8)
          fwrite(params,2,dataSize,fp1);
        else
          fwrite(params,1,dataSize,fp1);

        free(params);
        free(data);
        pTIDLPCLayers->weights.ptr = NULL;
        pTIDLPCLayers->weights.bufSize = 0;

        min = FLT_MAX;
        max = FLT_MIN;

        {
          float * biasData = (float *)(pTIDLPCLayers->bias.ptr);
          uint32_t biasDataSize = pTIDLPCLayers->bias.bufSize;
          TIDL_findRange(biasData, biasDataSize, &min, &max, 1.0 );
        }

        max = abs(min) >  abs(max) ? abs(min) : abs(max);
        pTIDLPCLayers->layerParams.innerProductParams.biasQ =
          TIDL_QuantizeUnsignedMax(0, 0, 0, 0, max, NUM_BIAS_BITS, (NUM_BIAS_BYTES * 8), &zeroWeightValue);

        data     = (float *)pTIDLPCLayers->bias.ptr;
        dataSize = pTIDLPCLayers->bias.bufSize;

        TIDL_importWriteParams(fpIpParams, data, dataSize);

        {
          int16_t *params = (int16_t *)malloc(dataSize*NUM_BIAS_BYTES);
          for (int idx = 0; idx < dataSize; idx++) 
          {
            int32_t biasParam = TIDL_normalize(data[idx], 0 , max);
            params[idx] = (int16_t)TIDL_roundSat(biasParam,0,SHRT_MIN,SHRT_MAX);
          }
          fwrite(params, NUM_BIAS_BYTES,dataSize,fp1);
          free(params);
        }
        free(data);
        pTIDLPCLayers->bias.ptr = NULL;
        pTIDLPCLayers->bias.bufSize = 0;

        if (pTIDLPCLayers->layerParams.innerProductParams.biasQ == 0)
        {
          pTIDLPCLayers->layerParams.innerProductParams.biasQ = 1;
        }
      }
      else if (pTIDLPCLayers->layerType == TIDL_BatchNormLayer)
      {
        float *  data = (float *)pTIDLPCLayers->weights.ptr;
        uint32_t dataSize = pTIDLPCLayers->weights.bufSize;

        TIDL_importWriteParams(fpBnParams, data, dataSize);

        uint8_t * params = (uint8_t *)malloc(dataSize * ((weightsElementSizeInBits - 1) / 8 + 1));
        TIDL_findRange(data, dataSize, &min, &max, 1.0);

        pTIDLPCLayers->layerParams.batchNormParams.weightsQ =
          TIDL_QuantizeUnsignedMax((uint8_t *)params, data, dataSize, min, max, NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
        pTIDLPCLayers->layerParams.batchNormParams.zeroWeightValue = zeroWeightValue;

        if (weightsElementSizeInBits > 8)
          fwrite(params, 2, dataSize, fp1);
        else
          fwrite(params, 1, dataSize, fp1);

        free(params);
        free(data);
        pTIDLPCLayers->weights.ptr = NULL;
        pTIDLPCLayers->weights.bufSize = 0;

        {
          min = FLT_MAX;
          max = FLT_MIN;
          {
            float * biasData = (float *)pTIDLPCLayers->bias.ptr;
            uint32_t biasDataSize = pTIDLPCLayers->bias.bufSize;
            TIDL_findRange(biasData, biasDataSize, &min, &max, 1.0);
          }

          data = (float *)pTIDLPCLayers->bias.ptr;
          dataSize = pTIDLPCLayers->bias.bufSize;

          TIDL_importWriteParams(fpBnParams, data, dataSize);

          max = abs(min) > abs(max) ? abs(min) : abs(max);
          pTIDLPCLayers->layerParams.batchNormParams.biasQ =
            TIDL_QuantizeUnsignedMax(0, 0, 0, 0, max, NUM_BIAS_BITS, (NUM_BIAS_BYTES * 8), &zeroWeightValue);

          int16_t * params = (int16_t *)malloc(dataSize*NUM_BIAS_BYTES);
          for (int idx = 0; idx < dataSize; idx++)
          {
            int32_t biasParam = TIDL_normalize(data[idx], 0, max);
            params[idx] = (int16_t)TIDL_roundSat(biasParam, 0, SHRT_MIN, SHRT_MAX);
          }
          fwrite(params, NUM_BIAS_BYTES, dataSize, fp1);
          free(params);
          free(data);
          if (pTIDLPCLayers->layerParams.batchNormParams.biasQ == 0)
          {
            pTIDLPCLayers->layerParams.batchNormParams.biasQ = 1;
          }
          pTIDLPCLayers->bias.ptr = NULL;
          pTIDLPCLayers->bias.bufSize = 0;
        }
        if (pTIDLPCLayers->layerParams.batchNormParams.reluParams.reluType == TIDL_PRelU)
        {
          float * slopeData = (float *)pTIDLPCLayers->slope.ptr;
          uint32_t slopeDataSize = pTIDLPCLayers->slope.bufSize;

          //TIDL_importWriteParams(fpBnParams, data, dataSize);

          uint8_t * params = (uint8_t *)malloc(slopeDataSize * ((weightsElementSizeInBits - 1) / 8 + 1));
          float min = FLT_MAX;
          float max = FLT_MIN;
          TIDL_findRange(slopeData, slopeDataSize, &min, &max, (1.0));
          pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ =
            TIDL_QuantizeUnsignedMax((uint8_t *)params, slopeData, slopeDataSize, min, max, NUM_WHGT_BITS, weightsElementSizeInBits, &zeroWeightValue);
          pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ /= 256;
          pTIDLPCLayers->layerParams.batchNormParams.reluParams.zeroSlopeValue = zeroWeightValue;

          if (weightsElementSizeInBits > 8)
          {
            fwrite(params, 2, slopeDataSize, fp1);
          }
          else
          {
            fwrite(params, 1, slopeDataSize, fp1);
          }
          free(params);
          free(slopeData);
          pTIDLPCLayers->slope.ptr = NULL;
          pTIDLPCLayers->slope.bufSize = 0;
          if (pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ == 0)
          {
            pTIDLPCLayers->layerParams.batchNormParams.reluParams.slopeQ = 1;
          }
        }
      }
    }
  }  // for(i=0; i<numLayers; i++)

  fclose(fpConvParams);
  fclose(fpIpParams);
  fclose(fpBnParams);
  fclose(fpConvParamsQuant);
  fclose(fpIpParamsQuant);
  fclose(fpBnParamsQuant);

} //TIDL_importQuantWriteLayerParamsDbg

int32_t tidl_copyPCNetToDeviceNet(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, sTIDL_Network_t  &tIDLNetStructure, int32_t layerIndex, int weightsElementSizeInBits)
{
  int32_t i, j;
  int64_t                    totalMacs = 0;
  int32_t tiLayerIndex = 0;
  printf("\nNum of Layer Detected : %3d \n", layerIndex);
  FILE *layerInfoFile = fopen(LAYER_INFO_FILENAME, "w");

  tIDLNetStructure.dataElementSize = 1;
  tIDLNetStructure.biasElementSize = 2;
  tIDLNetStructure.weightsElementSize = ((weightsElementSizeInBits-1)/8 + 1);
  tIDLNetStructure.slopeElementSize = tIDLNetStructure.weightsElementSize;
  tIDLNetStructure.interElementSize = 1;
  tIDLNetStructure.quantizationStyle = gParams.quantizationStyle;

  printf("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
  printf("%5s|%-30s|%-50s|%-6s|%-6s|%-6s|%-32s|%-10s|%-36s|%-36s|%-11s|\n", "Num", "TIDL Layer Name", "Out Data Name", "Group", "#Ins", "#Outs", "Inbuf Ids", "Outbuf Id", "In NCHW", "Out NCHW", "MACS");
  printf("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
  for (i = 0; i < layerIndex; i++)
  {
    if ((orgTIDLNetStructure.TIDLPCLayers[i].layerType != TIDL_UnSuportedLayer) &&
      (orgTIDLNetStructure.TIDLPCLayers[i].layerType != TIDL_ConstDataLayer))
    {
      tIDLNetStructure.TIDLLayers[tiLayerIndex].layerType = orgTIDLNetStructure.TIDLPCLayers[i].layerType;
      tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams = orgTIDLNetStructure.TIDLPCLayers[i].layerParams;
      tIDLNetStructure.TIDLLayers[tiLayerIndex].numInBufs = orgTIDLNetStructure.TIDLPCLayers[i].numInBufs;
      tIDLNetStructure.TIDLLayers[tiLayerIndex].numOutBufs = orgTIDLNetStructure.TIDLPCLayers[i].numOutBufs;
      tIDLNetStructure.TIDLLayers[tiLayerIndex].weightsElementSizeInBits = orgTIDLNetStructure.TIDLPCLayers[i].weightsElementSizeInBits;
      if ((gParams.modelType == 2) || (gParams.modelType == 0))
      {
        tIDLNetStructure.TIDLLayers[tiLayerIndex].strideOffsetMethod = TIDL_strideOffsetTopLeft;
      }
      else
      {
        tIDLNetStructure.TIDLLayers[tiLayerIndex].strideOffsetMethod = orgTIDLNetStructure.TIDLPCLayers[i].strideOffsetMethod;
      }

      if (tIDLNetStructure.TIDLLayers[tiLayerIndex].layerType == TIDL_DataLayer)
      {
        tIDLNetStructure.TIDLLayers[tiLayerIndex].layersGroupId = 0;
      }
      else
      {
        tIDLNetStructure.TIDLLayers[tiLayerIndex].coreID             = gParams.layersGroupId[tiLayerIndex];
	    tIDLNetStructure.TIDLLayers[tiLayerIndex].layersGroupId      = gParams.layersGroupId[tiLayerIndex];
      }

      // Copy sBuff_t information from PC Network to device network
      if ((orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_ConvolutionLayer) ||
        (orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_Deconv2DLayer))
      {
        TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.convParams.weights, &orgTIDLNetStructure.TIDLPCLayers[i].weights);
        if (orgTIDLNetStructure.TIDLPCLayers[i].layerParams.convParams.enableBias)
        {
          TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.convParams.bias, &orgTIDLNetStructure.TIDLPCLayers[i].bias);
        }
      }
      else if (orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_InnerProductLayer)
      {
        TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.innerProductParams.weights, &orgTIDLNetStructure.TIDLPCLayers[i].weights);
        TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.innerProductParams.bias, &orgTIDLNetStructure.TIDLPCLayers[i].bias);
      }
      else if (orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_BatchNormLayer)
      {
        TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.batchNormParams.weights, &orgTIDLNetStructure.TIDLPCLayers[i].weights);
        TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.batchNormParams.bias, &orgTIDLNetStructure.TIDLPCLayers[i].bias);

        if (orgTIDLNetStructure.TIDLPCLayers[i].layerParams.batchNormParams.reluParams.reluType == TIDL_PRelU)
        {
          TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.batchNormParams.reluParams.slope, &orgTIDLNetStructure.TIDLPCLayers[i].slope);
        }
      }
      else if (orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_DetectionOutputLayer)
      {
        TIDL_convertSbuff(&tIDLNetStructure.TIDLLayers[tiLayerIndex].layerParams.detectOutParams.priorBox, &orgTIDLNetStructure.TIDLPCLayers[i].priorBox);
      }

      printf("%5d|%-30s|", tiLayerIndex, TIDL_LayerString[orgTIDLNetStructure.TIDLPCLayers[i].layerType]);
      if (strlen((const char *)orgTIDLNetStructure.TIDLPCLayers[i].outDataNames[0]) > 50)
      {
        printf("%-50s|", &orgTIDLNetStructure.TIDLPCLayers[i].outDataNames[0][strlen((const char *)orgTIDLNetStructure.TIDLPCLayers[i].outDataNames[0]) - 50]);
      }
      else
      {
        printf("%-50s|", orgTIDLNetStructure.TIDLPCLayers[i].outDataNames[0]);
      }
      fprintf(layerInfoFile, "%d %s \n", orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dataId,
        orgTIDLNetStructure.TIDLPCLayers[i].outDataNames[0]);

      printf("%6d|%6d|%6d|", tIDLNetStructure.TIDLLayers[tiLayerIndex].layersGroupId, orgTIDLNetStructure.TIDLPCLayers[i].numInBufs, orgTIDLNetStructure.TIDLPCLayers[i].numOutBufs);

      for (j = 0; j < orgTIDLNetStructure.TIDLPCLayers[i].numInBufs; j++)
      {
        printf("%3d ", orgTIDLNetStructure.TIDLPCLayers[i].inData[j].dataId);
        tIDLNetStructure.TIDLLayers[tiLayerIndex].inData[j] = orgTIDLNetStructure.TIDLPCLayers[i].inData[j];

      }
      for (j = (orgTIDLNetStructure.TIDLPCLayers[i].numInBufs > 0 ? orgTIDLNetStructure.TIDLPCLayers[i].numInBufs : 0); j < 8; j++)
      {
        printf("  x ");
      }
      printf("|");
      printf("%3d ", orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dataId);
      printf("      |");
      for (j = 0; j < orgTIDLNetStructure.TIDLPCLayers[i].numOutBufs; j++)
      {
        tIDLNetStructure.TIDLLayers[tiLayerIndex].outData[j] = orgTIDLNetStructure.TIDLPCLayers[i].outData[j];

      }
      for (j = 0; j < TIDL_DIM_MAX; j++)
      {
        printf("%8d ", orgTIDLNetStructure.TIDLPCLayers[i].inData[0].dimValues[j]);
      }
      printf("|");

      for (j = 0; j < TIDL_DIM_MAX; j++)
      {
        printf("%8d ", orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dimValues[j]);
      }
      printf("|");
#ifdef PLATFORM_64BIT
      printf("%10ld |", orgTIDLNetStructure.TIDLPCLayers[i].numMacs);
#else
      printf("%10lld |", orgTIDLNetStructure.TIDLPCLayers[i].numMacs);
#endif
      totalMacs += orgTIDLNetStructure.TIDLPCLayers[i].numMacs;
      printf("\n");
      tiLayerIndex++;
    }

  }
  fclose(layerInfoFile);
  printf("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
  printf("Total Giga Macs : %4.4f\n", ((float)totalMacs / 1000000000));
  printf("--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------\n");
  return tiLayerIndex;
}

int32_t tidl_addOutDataLayer(sTIDL_Network_t  &tIDLNetStructure, int32_t tiLayerIndex)
{
  int32_t i, j;

  if(tIDLNetStructure.TIDLLayers[tiLayerIndex-1].layerType == TIDL_DataLayer)
  { // if last layer is already DataLayer, overwrite it
    //printf("Last layer is already data layer.\n");
    tiLayerIndex -= 1;
  }

  tIDLNetStructure.TIDLLayers[tiLayerIndex].layerType = TIDL_DataLayer;
  tIDLNetStructure.TIDLLayers[tiLayerIndex].numInBufs = 0;
  tIDLNetStructure.TIDLLayers[tiLayerIndex].numOutBufs = -1;
  tIDLNetStructure.TIDLLayers[tiLayerIndex].coreID = 255;

  for (i = 0; i < tiLayerIndex; i++)
  {
    if (tIDLNetStructure.TIDLLayers[i].layerType != TIDL_DataLayer)
    {
      for (j = 0; j < tIDLNetStructure.TIDLLayers[i].numOutBufs; j++)
      {
        if (!TIDL_isDataBufUsed(tIDLNetStructure.TIDLLayers[i].outData[j].dataId, &tIDLNetStructure, tiLayerIndex))
        {
          tIDLNetStructure.TIDLLayers[tiLayerIndex].inData[tIDLNetStructure.TIDLLayers[tiLayerIndex].numInBufs] = tIDLNetStructure.TIDLLayers[i].outData[j];
          tIDLNetStructure.TIDLLayers[tiLayerIndex].numInBufs++;
        }
      }
    }
  }
  tIDLNetStructure.numLayers = tiLayerIndex + 1;
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_addInDataLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex, int32_t * dataIndex)
{
  int32_t i, j;
  int32_t idx;

  for (i = 0; i < layerIndex; i++)
  {
    if (pOrgTIDLNetStructure.TIDLPCLayers[i].layerType != TIDL_DataLayer)
    {
      for (j = 0; j < pOrgTIDLNetStructure.TIDLPCLayers[i].numInBufs; j++)
      {

        idx = tidl_getInLayer(pOrgTIDLNetStructure, layerIndex, pOrgTIDLNetStructure.TIDLPCLayers[i].inData[j].dataId);
        if (idx == -1)
        {
          pOrgTIDLNetStructure.TIDLPCLayers[layerIndex].layerType = TIDL_DataLayer;
          pOrgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs  = -1;
          pOrgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs = 1;
          strcpy((char *)pOrgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[0], (char *)pOrgTIDLNetStructure.TIDLPCLayers[i].inDataNames[j]);
          pOrgTIDLNetStructure.TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
          pOrgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerCnt[0]    = 1;
          pOrgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerLinked[0] = 0;
          tidl_linkOutputTensors(&pOrgTIDLNetStructure, layerIndex);
          layerIndex++;
        }
      }
    }
  }
  pOrgTIDLNetStructure.numLayers = layerIndex;
  return TIDL_IMPORT_NO_ERR;
}

int32_t tidl_fillInDataLayerShape(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, tidl_import_config * params, int32_t layerIndex)
{
  int32_t i, j, inDataIdx;
  int overWritefirstNode = 1;
  if ((params->inWidth == -1) || (params->inHeight == -1) || (params->inNumChannels == -1))
  {
    overWritefirstNode = 0;
  }
  inDataIdx = 0;
  for (i = 0; i < layerIndex; i++)
  {
    if ((orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_DataLayer) && (orgTIDLNetStructure.TIDLPCLayers[i].numOutBufs > 0))
    {
      orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dimValues[0] = 1;
      if (overWritefirstNode)
      {
        orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dimValues[1] = params->inNumChannels;
        orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dimValues[2] = params->inHeight;
        orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dimValues[3] = params->inWidth;
      }
      orgTIDLNetStructure.TIDLPCLayers[i].outData[0].elementType = params->inElementType;
      orgTIDLNetStructure.TIDLPCLayers[i].outData[0].dataQ = params->inQuantFactor;

      inDataIdx++;
    }
  }
  return TIDL_IMPORT_NO_ERR;
}

#if 0
int32_t TIDL_isInputLayer(sTIDL_OrgNetwork_t * pOrgTIDLNetStructure, int32_t numLayer, const char *bufName, int32_t layerType)
{
  int32_t i, j;
  for (i = (numLayer - 1); i >= 0; i--)
  {
    for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[i].numOutBufs; j++)
    {
      if (strcmp((const char*)bufName, (const char*)pOrgTIDLNetStructure->TIDLPCLayers[i].outDataNames[j]) == 0)
      {
        if ((pOrgTIDLNetStructure->TIDLPCLayers[i].numOutBufs == 1) && (pOrgTIDLNetStructure->TIDLPCLayers[i].layerType == layerType))
        {
          return 1;
        }
        else
        {
          return 0;
        }
      }
    }
  }
  return 0;
}
#endif

int32_t tidl_getLayerTypeMapIdx(const char* layerName, TIDL_TFLayerMapping_t* TIDL_TFLayerMap, int32_t tblSize)
{
  int32_t idx;
  for (idx = 0; idx < tblSize; idx++)
  {
    if (strcmp(layerName, TIDL_TFLayerMap[idx].layerName) == 0)
    {
      return (idx);
    }
  }
  return -1;
}

int32_t tidl_isLayerType(const char* layerName, int32_t  startLayer, sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, TIDL_TFLayerMapping_t* TIDL_TFLayerMap, int32_t tblSize)
{
  int32_t i, numOps;
  int32_t mapIdx = tidl_getLayerTypeMapIdx(layerName, TIDL_TFLayerMap, tblSize);
  if (mapIdx != -1)
  {
    char layerOpsString[300] = "";
    numOps = TIDL_TFLayerMap[mapIdx].NumOps;
    for (i = 0; ((i < numOps) && ((startLayer + i) < pOrgTIDLNetStructure.numLayers)); i++)
    {
      strcat(layerOpsString, TIDL_LayerString[pOrgTIDLNetStructure.TIDLPCLayers[(startLayer + i)].layerType]);
    }
    if (strcmp(layerOpsString, TIDL_TFLayerMap[mapIdx].layerOpsString) == 0)
    {
      return (1);
    }
  }
  return (0);
}

/* Define strings for layers that can be merged into TIDL layers when requirements
   are met. The corresponding layer types are defined in ti_dl.h. */
const char * TIDL_Unsupported_Layers[] = 
{
"ShuffleChannel Layer" ,  // TIDL_ShuffleChannelLayer
"Resize Layer" ,          // TIDL_ResizeLayer
"PriorBox Layer" ,        // TIDL_PriorBoxLayer
"Permute Layer" ,         // TIDL_PermuteLayer
"Reshape Layer" ,         // TIDL_ReshapeLayer
"Shape Layer" ,           // TIDL_ShapeLayer
"Squeeze Layer" ,         // TIDL_SqueezeLayer
"Pad Layer" ,             // TIDL_PadLayer
"Transpose Layer",        // TIDL_TransposeLayer
};

/*==============================================================================
* Function tidl_countUnsupportedLayers()
*
*  For a given network model, it may have layers that TIDL implemented, and layers
*  that TIDL has not implemented yet. For layers not implemented by TIDL, some can
*  be coalesced into TIDL layers under certain conditions. These layers are defined
*  in ti_dl.h and also listed in table TIDL_Unsupported_Layers.
*
*  These layers, even though not implemented by TIDL, are still mapped to dummy
*  TIDL layers (defined in ti_dl.h) during the parsing stage of the import. Then
*  the import process will try to coalesce (merge) them into layers implemented
*  by TIDL (defined in itidl_ti.h). However, there are limitations on coalescing
*  these unsupported layers into TIDL layers. If these layers cannot be coalesced
*  into TIDL layers, it should be caught during the import process.
*
*  This function scans through a provided network structure and count how many
*  layers are not supported by TIDL and not able to be merged into TIDL layers.
*  It prints out each unsupported layer name and returns total number of unsupported
*  layers. 
==============================================================================*/
int32_t tidl_countUnsupportedLayers(sTIDL_Network_t *pNetStructure, 
                                    int32_t numLayers)
{
  int32_t i, layerType, numUnsupportedLayers;

  numUnsupportedLayers = 0;
  for(i=0; i<numLayers; i++)
  {
    layerType = pNetStructure->TIDLLayers[i].layerType;
    if(  (layerType == TIDL_ShuffleChannelLayer)
       ||(layerType == TIDL_ResizeLayer)
       ||(layerType == TIDL_PriorBoxLayer)
       ||(layerType == TIDL_PermuteLayer)
       ||(layerType == TIDL_ReshapeLayer)
       ||(layerType == TIDL_ShapeLayer)
       ||(layerType == TIDL_SqueezeLayer)
       ||(layerType == TIDL_PadLayer)
       ||(layerType == TIDL_TransposeLayer) )
    {
      printf("%s is not supported by TIDL and cannot be merged into any TIDL layer.\n", 
             TIDL_Unsupported_Layers[layerType-TIDL_ShuffleChannelLayer]);
      numUnsupportedLayers++;
    }
  }

  return numUnsupportedLayers;
} /* tidl_countUnsupportedLayers */

int32_t tidl_getStringsFromList(char *list, char * names, int strLen)
{
  int32_t numStrings = 0;
  char *ptr = list;
  while (ptr[0] != '\0')
  {
    if (ptr[0] == ',') ptr[0] = ' ';
    ptr++;
  }
  ptr = list;
  while (ptr[0] != '\0' )
  {
    if((ptr[0] == ' ') || (ptr[0] == ',') || (ptr[0] == '\t'))
    {
      ptr++;
    }
    else
    {
      sscanf(ptr, "%s", &names[strLen*numStrings]);
      ptr += strlen((char*)(&names[strLen*numStrings]));
      numStrings++;
    }
  }
  return numStrings;
}

