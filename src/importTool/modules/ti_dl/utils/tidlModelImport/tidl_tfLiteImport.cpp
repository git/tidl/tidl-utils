/*
*
* Copyright (c) {2015 - 2017} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
using namespace std;
using ::google::protobuf::Message;
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <cmath>

#include "ti_dl.h"
#include "schema_generated.h"
#include "tidl_import_config.h"

using namespace std;
using namespace tflite;

#include "tidl_import_common.h" 

#define IS_SIGNED_DATA (1)
#define QUAN_STYLE2_ROUND (0.5)
extern sTIDL_OrgNetwork_t      orgTIDLNetStructure;
extern sTIDL_OrgNetwork_t      tempTIDLNetStructure;
extern sTIDL_Network_t         tIDLNetStructure;

extern sTIDL_tfOutRehapeMap_t sTIDL_tfOutRehapeTable[];

extern int32_t tidl_FindFlattenLayer(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex);

#define TIDL_MAX_DATA_NAME (300)

uint32_t TIDL_tflitekernelReshape(float * param, uint32_t w, uint32_t h, uint32_t ci, uint32_t co)
{
  float * tPtr = (float * )my_malloc(w*h*ci*co*sizeof(float));
  int32_t counter = 0;
  for(int l1 = 0; l1 < co; ++l1){
    for(int l = 0; l < ci; ++l){
      int k = l;
      for(int j = 1; j<=w*h; ++j){
        tPtr[counter] = param[l1*w*h*ci + k];
        k+=ci;
        counter++;
      }
    }   
  }
  memcpy(param,tPtr,w*h*ci*co*sizeof(float));
  free(tPtr);
  return 0;
}

static void readTensor(const flatbuffers::Vector<uint8_t> * data, uint8_t * ptr)
{
  for (int id = 0; id < data->size(); id++)
  {
    ptr[id] = data->Get(id);
  }
}

int32_t TIDL_tfliteCopyInputConstTensor(const Model* tfliteModel, int32_t nIdx, int32_t inIdx, sBufferPc_t &buf)
{
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(nIdx);
  int tensor_idx = op->inputs()->Get(inIdx);
  const auto *tensor = tensors->Get(tensor_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  float * ptr = (float *)malloc(data->size());
  readTensor(data, (uint8_t *) ptr);
  buf.bufSize = data->size()/sizeof(float);
  buf.ptr = (float *)my_malloc(buf.bufSize*sizeof(float));
  if (((tensor->type() == 0) || (tensor->type() == 2))) //DT_FLOAT
  {
    memcpy(buf.ptr, ptr, buf.bufSize*sizeof(float));
    return TIDL_IMPORT_NO_ERR;;
  }
  else
  {
    printf("\nOnly float and DT_INT32 tensor is suported \n");
    return TIDL_IMPORT_ERR_TENSOR_TYPE_NOT_SUPPORTED;
  }
}

bool findAddInput(const Model* tfliteModel,int32_t idx)
{
  int i, j, k, l;
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  char index[1000];
  strcpy(index,tensors->Get(idx)->name()->c_str());
  for (i = 0; i < operators->size(); i++)
  {
    const auto *op = operators->Get(i);
    for(j = 0; j < op->outputs()->size(); j++)
    {
      char str[1000]={'\0'};
      strcpy(str,tensors->Get(op->outputs()->Get(j))->name()->c_str());
      if(strcmp(index,str)==0)
        return true;
    }
  }
  return false;
}

int32_t TIDL_tfLiteFillActParams(sTIDL_ReLUParams_t & reluParams, int32_t & enableRelU, int32_t tfLiteActType)
{
  if (tfLiteActType == ActivationFunctionType_NONE)
  {
    enableRelU = 0;
  }
  else if (tfLiteActType == ActivationFunctionType_RELU)
  {
    reluParams.reluType = TIDL_RelU;
    enableRelU = 1;
  }
  else if (tfLiteActType == ActivationFunctionType_RELU6)
  {
    reluParams.reluType = TIDL_RelU6;
    enableRelU = 1;
  }
  else
  {
    printf("TIDL limitation: Unsupported Activation Function type %d \n", tfLiteActType);
    return TIDL_IMPORT_ERR_LAYER_PARAMS_NOT_FOUND;
  }
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapPlaceHolderParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_DataLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = -1;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapConvParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t status;
  int32_t padType;
  int32_t idx1, idx2;
  sTIDL_LayerPC_t &TIDLPCLayers       = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams      = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.convParams;

  TIDLPCLayers.layerType = TIDL_ConvolutionLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(i);
  int kernel_idx = op->inputs()->Get(1);
  const auto *tensor = tensors->Get(kernel_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  convParams.numInChannels   = shape->Get(3);
  convParams.numOutChannels  = shape->Get(0);
  convParams.kernelW         = shape->Get(2);
  convParams.kernelH         = shape->Get(1);

  convParams.numGroups       = 1;
  convParams.dilationW       = 1;
  convParams.dilationH       = 1;
  convParams.strideW         = 1;
  convParams.strideH         = 1;
  convParams.padW            = 0;
  convParams.padH            = 0;
  convParams.enableBias      = 0;
  convParams.enablePooling   = 0;

  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *Conv2DParams = op->builtin_options_as_Conv2DOptions();
  convParams.strideW = Conv2DParams->stride_w();
  convParams.strideH = Conv2DParams->stride_h();
  convParams.dilationW = Conv2DParams->dilation_w_factor();
  convParams.dilationH = Conv2DParams->dilation_h_factor();

  status = TIDL_tfLiteFillActParams(convParams.reluParams, convParams.enableRelU, Conv2DParams->fused_activation_function());
  if (status)
  {
    return status;
  }
  // Get padding parameters
  padType = Conv2DParams->padding();
  if (padType == 0)
  {
    convParams.padW = ((convParams.kernelW - 1)*convParams.dilationW) / 2;
    convParams.padH = ((convParams.kernelH - 1)*convParams.dilationH) / 2;
  }
  if (tflite::BuiltinOperator_CONV_2D == operator_codes)
  {
    // Read in the kernel weights
    status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 1, TIDLPCLayers.weights);
    if (status)
    {
      return status;
    }
    // Read in bias when it is provided
    if(op->inputs()->size()==3)
    {
      status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 2, TIDLPCLayers.bias);
      if (status)
      {
        return status;
      }
      convParams.enableBias = 1;
    }
    TIDL_tflitekernelReshape((float *)TIDLPCLayers.weights.ptr, convParams.kernelW, convParams.kernelH, convParams.numInChannels, convParams.numOutChannels);
  }
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapDeConvParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t status;
  int32_t padType;
  int32_t idx1, idx2;
  sTIDL_LayerPC_t &TIDLPCLayers       = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams      = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.convParams;

  TIDLPCLayers.layerType = TIDL_Deconv2DLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(i);
  int kernel_idx = op->inputs()->Get(1);
  const auto *tensor = tensors->Get(kernel_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  convParams.numInChannels   = shape->Get(3);
  convParams.numOutChannels  = shape->Get(0);
  convParams.kernelW         = shape->Get(2);
  convParams.kernelH         = shape->Get(1);

  convParams.numGroups       = 1;
  convParams.dilationW       = 1;
  convParams.dilationH       = 1;
  convParams.strideW         = 1;
  convParams.strideH         = 1;
  convParams.padW            = 0;
  convParams.padH            = 0;
  convParams.enableBias      = 0;
  convParams.enablePooling   = 0;

  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *Conv2DParams = op->builtin_options_as_TransposeConvOptions();
  convParams.strideW = Conv2DParams->stride_w();
  convParams.strideH = Conv2DParams->stride_h();

  padType = Conv2DParams->padding();
  if (padType == 0)
  {
    convParams.padW = ((convParams.kernelW - 1)*convParams.dilationW) / 2;
    convParams.padH = ((convParams.kernelH - 1)*convParams.dilationH) / 2;
  }
  status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 1, TIDLPCLayers.weights);
  TIDL_tflitekernelReshape((float *)TIDLPCLayers.weights.ptr, convParams.kernelW, convParams.kernelH, convParams.numInChannels, convParams.numOutChannels);
  
  const auto *shape_tensor = tensors->Get(op->inputs()->Get(0));
  auto *shape_data   = (*tfliteModel->buffers())[shape_tensor->buffer()]->data();
  int32_t * ptr = (int32_t *)malloc(shape_data->size());
  readTensor(shape_data, (uint8_t *) ptr);
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dimValues[TIDL_DIM_HEIGHT] = -ptr[1];
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dimValues[TIDL_DIM_WIDTH]  = -ptr[2];
  free(ptr);

  return status;
}

int32_t TIDL_tfliteMapDWConvParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t status;
  int32_t padType;
  int32_t idx1, idx2;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_ConvParams_t &convParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.convParams;
  TIDLPCLayers.layerType = TIDL_ConvolutionLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(i);
  int kernel_idx = op->inputs()->Get(1);
  const auto *tensor = tensors->Get(kernel_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  convParams.numInChannels   = shape->Get(3);
  convParams.numOutChannels  = shape->Get(0);
  convParams.kernelW         = shape->Get(2);
  convParams.kernelH         = shape->Get(1);

  convParams.numGroups       = 1;
  convParams.dilationW       = 1;
  convParams.dilationH       = 1;
  convParams.strideW         = 1;
  convParams.strideH         = 1;
  convParams.padW            = 0;
  convParams.padH            = 0;
  convParams.enableBias      = 0;
  convParams.enablePooling   = 0;

  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *DWConv2DParams = op->builtin_options_as_DepthwiseConv2DOptions();
  convParams.strideW = DWConv2DParams->stride_w();
  convParams.strideH = DWConv2DParams->stride_h();
  convParams.dilationW = DWConv2DParams->dilation_w_factor();
  convParams.dilationH = DWConv2DParams->dilation_h_factor();
  status = TIDL_tfLiteFillActParams(convParams.reluParams, convParams.enableRelU, DWConv2DParams->fused_activation_function());
  if (status)
  {
    return status;
  }

  // Get padding parameters
  padType = DWConv2DParams->padding();
  if (padType == 0)
  {
    convParams.padW = ((convParams.kernelW - 1)*convParams.dilationW) / 2;
    convParams.padH = ((convParams.kernelH - 1)*convParams.dilationH) / 2;
  }
  if (convParams.numOutChannels != 1)
  {
    printf("DW Convolution with Depth multiplier > 1 is not suported now\n");
    return TIDL_IMPORT_ERR_DWCONV_DEPTH_MULT_INVALID;
  }

  convParams.numGroups      =
  convParams.numOutChannels =
  convParams.numInChannels;

  status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 1, TIDLPCLayers.weights);
  if (status)
  {
    return status;
  }
  if(op->inputs()->size()==3)
  {
    status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 2, TIDLPCLayers.bias);
    if (status)
    {
      return status;
    }
    convParams.enableBias = 1;
  }
  TIDL_tflitekernelReshape((float *)TIDLPCLayers.weights.ptr, convParams.kernelW, convParams.kernelH,
   convParams.numOutChannels, convParams.numInChannels/ convParams.numGroups );
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapAddParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{ 
  int32_t status;
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  const auto* op = operators->Get(i);
  int tensor_idx = op->inputs()->Get(1);
  const auto *tensor = tensors->Get(tensor_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *AddParams = op->builtin_options_as_AddOptions();
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  if(findAddInput(tfliteModel,tensor_idx))
  {
    sTIDL_EltWiseParams_t &addParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.eltWiseParams;
    TIDLPCLayers.layerType = TIDL_EltWiseLayer;
    TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
    TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;
    addParams.eltWiseType = TIDL_EltWiseSum;
    addParams.numInData = 2;
    TIDLPCLayers.numInBufs = op->inputs()->size();
    status = TIDL_tfLiteFillActParams(addParams.reluParams, addParams.enableRelU, AddParams->fused_activation_function());
  }
  else
  {
    sTIDL_BatchNormParams_t &addParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.batchNormParams;
    TIDLPCLayers.layerType = TIDL_BatchNormLayer;
    TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
    TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;
    status = TIDL_tfLiteFillActParams(addParams.reluParams, addParams.enableRelU, AddParams->fused_activation_function());
    if (status)
    {
      return status;
    }

    int32_t dataSize = shape->Get(0);
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.ptr     = my_malloc(dataSize*sizeof(float));
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.bufSize = dataSize;
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.ptr        = my_malloc(dataSize*sizeof(float));
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.bufSize    = dataSize;  
    float* ptr = (float*)malloc(dataSize*sizeof(float));
    for(int lc = 0; lc < dataSize; ++lc)
      ptr[lc] = 1;
    memcpy((void*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weights.ptr, ptr, dataSize*sizeof(float));  
    status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 1, TIDLPCLayers.bias);
  }
  return status;
}

int32_t TIDL_tfliteMapMulParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t status;
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  const auto* op = operators->Get(i);
  int tensor_idx = op->inputs()->Get(1);
  const auto *tensor = tensors->Get(tensor_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *MulParams = op->builtin_options_as_MulOptions();
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_BatchNormParams_t &mulParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.batchNormParams;
  TIDLPCLayers.layerType = TIDL_BatchNormLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;
  status = TIDL_tfLiteFillActParams(mulParams.reluParams, mulParams.enableRelU, MulParams->fused_activation_function());
  if (status)
  {
    return status;
  }

  status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 1, TIDLPCLayers.weights);
  int32_t dataSize = shape->Get(0);
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.ptr        = my_malloc(dataSize*sizeof(float));
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.bufSize    = dataSize;  
  float* ptr = (float*)malloc(dataSize*sizeof(float));
  for(int lc = 0; lc < dataSize; ++lc)
    ptr[lc] = 0;
  memcpy((void*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].bias.ptr, ptr, dataSize*sizeof(float));

  return status;
}

int32_t TIDL_tfliteMapMaxPoolParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t padType;
  int32_t idx1, idx2;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_PoolingParams_t &poolParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams;
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(i);

  TIDLPCLayers.layerType = TIDL_PoolingLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  poolParams.poolingType = TIDL_MaxPooling;

  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;

  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *Pool2DParams = op->builtin_options_as_Pool2DOptions();
  poolParams.strideW = Pool2DParams->stride_w();
  poolParams.strideH = Pool2DParams->stride_h();
  poolParams.kernelW = Pool2DParams->filter_width();
  poolParams.kernelH = Pool2DParams->filter_height();

  if ( (poolParams.kernelW > 9) || (poolParams.kernelH > 9) )
  {
    return TIDL_IMPORT_ERR_SIZE_NOT_MATCH;
  }

  padType = Pool2DParams->padding();
  if (padType == 0)
  {
    poolParams.padW = ((poolParams.kernelW - 1)) / 2;
    poolParams.padH = ((poolParams.kernelH - 1)) / 2;
  }

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapAvgPoolParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t status;
  status = TIDL_tfliteMapMaxPoolParams(pOrgTIDLNetStructure, i, layerIndex, dataIndex, tfliteModel);
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams.poolingType = TIDL_AveragePooling;
  return status;
}

int32_t TIDL_tfliteMapConcatV2Params(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t idx;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ConcatLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  sTIDL_ConcatParams_t &concatParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.concatParams;
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(i);
  
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs = op->inputs()->size();

  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *ConcatParams = op->builtin_options_as_ConcatenationOptions();

  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapReshapeParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ReshapeLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapSqueezeParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ReshapeLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapSoftmaxParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_SoftMaxLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapArgmaxParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ArgMaxLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}


int32_t TIDL_tfliteMapPadParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t idx,j;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_PadLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;

  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(i);
  int tensor_idx = op->inputs()->Get(1);
  const auto *tensor = tensors->Get(tensor_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  float * ptr; 
  if(data)  
    {
      ptr = (float *)malloc(data->size());
      readTensor(data, (uint8_t *) ptr);
    }
  memcpy((void*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerPCParams.padParams.padTensor, ptr, data->size());
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapMeanParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t status;
  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_PoolingParams_t &poolParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.poolParams;

  TIDLPCLayers.layerType = TIDL_PoolingLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  poolParams.poolingType = TIDL_AveragePooling;
  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;
  poolParams.kernelW = 0;
  poolParams.kernelH = 0;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapFullyConnectedParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  int32_t status;

  sTIDL_LayerPC_t &TIDLPCLayers = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex];
  sTIDL_InnerProductParams_t &innerProductParams = pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.innerProductParams;
  
  orgTIDLNetStructure.TIDLPCLayers[i].layerType = TIDL_InnerProductLayer;
  
  TIDLPCLayers.layerType = TIDL_InnerProductLayer;
  TIDLPCLayers.outData[0].dataId = (*dataIndex)++;
  TIDLPCLayers.outData[0].elementType = TIDL_SignedChar;

  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  auto* op = operators->Get(i);
  int kernel_idx = op->inputs()->Get(1);
  const auto *tensor = tensors->Get(kernel_idx);
  auto *shape = tensor->shape();
  auto *data   = (*tfliteModel->buffers())[tensor->buffer()]->data();
  innerProductParams.numOutNodes = shape->Get(0);
  innerProductParams.numInNodes = shape->Get(1);
  auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
  auto *FullyConnectedParams = op->builtin_options_as_FullyConnectedOptions();

  status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 1, TIDLPCLayers.weights);
  if (status)
  {
    return status;
  }

  status = TIDL_tfliteCopyInputConstTensor(tfliteModel, i, 2, TIDLPCLayers.bias);
  return status;
}

static int32_t tidl_getFBModel(uint8_t * fileString, uint8_t **buffer_pointer)
{
  FILE * fptr;
  int32_t netSize;

  fptr = fopen((const char *)fileString, "rb");
  if (fptr)
  {
    fseek(fptr, 0L, SEEK_END);
    netSize = ftell(fptr);
    fseek(fptr, 0L, SEEK_SET);
    *buffer_pointer = (uint8_t *)malloc(netSize);
    fread(*buffer_pointer, 1, netSize, fptr);
    fclose(fptr);
    return netSize;
  }
  else
  {
    printf("Could Not Open Files %s\n", fileString);
    return -1;
  }

}

int32_t TIDL_tfliteGetNodeIdx(const Model* tfliteModel, const char *bufName)
{
  int32_t i,j,flag = 0, nameLen, nodeIdx = -1;
  char nodeName[TIDL_MAX_DATA_NAME];
  char inDataName[TIDL_MAX_DATA_NAME];
  auto operator_codes = tfliteModel->operator_codes();
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  //auto metadata_buffer = tfliteModel->metadata_buffer();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  
  for (i = 0; i < operators->size(); i++)
  { 
    const auto *op = operators->Get(i);
    for(j = 0; j < op->outputs()->size(); j++)
    { 
      const auto *tensor = tensors->Get(op->outputs()->Get(j));
      const auto *op_code = operator_codes->Get(op->opcode_index());
      //printf("Node Name %d - %d- %s- %s - %d \n",i, op->opcode_index(), EnumNamesBuiltinOperator()[ op_code->builtin_code()], tensor->name()->c_str());
      strcpy(nodeName,tensor->name()->c_str());
      strcpy(inDataName, bufName);
      if (strcmp(nodeName, inDataName) == 0)
      {
        nodeIdx = i;
        flag = 1;
        break;
      }
    }
    if(flag) 
      break;
  }
  return nodeIdx;
}

int32_t TIDL_tfliteMapReluParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_ReLULayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

int32_t TIDL_tfliteMapRelu6Params(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  TIDL_tfliteMapReluParams(pOrgTIDLNetStructure, i, layerIndex, dataIndex, tfliteModel);
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerParams.reluParams.reluType = TIDL_RelU6;
  return TIDL_IMPORT_NO_ERR;
}

typedef struct {
  int8_t name[TIDL_STRING_SIZE];
  int32_t(*tidl_tfliteMapFunc)(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
    int32_t              i,
    int32_t              layerIndex,
    int32_t              *dataIndex,
    const Model*         tfliteModel);
}sTIDL_tfliteOpParamMap_t;

#define TIDL_NUM_SUPPORTED_TFLITE_OPERATORS 17
sTIDL_tfliteOpParamMap_t tidl_TfliteOpParamMapTable[] = 
{
  { "Placeholder",                     TIDL_tfliteMapPlaceHolderParams },       //  TIDL_DataLayer,
  { "CONV_2D",                         TIDL_tfliteMapConvParams },       //  TIDL_ConvolutionLayer ,
  { "TRANSPOSE_CONV",                  TIDL_tfliteMapDeConvParams },       //  TIDL_ConvolutionLayer ,
  { "DEPTHWISE_CONV_2D",               TIDL_tfliteMapDWConvParams },       //  TIDL_ConvolutionLayer ,
  { "ADD",                             TIDL_tfliteMapAddParams },       //  TIDL_EltWiseLayer ,
  { "MUL",                             TIDL_tfliteMapMulParams },       //  TIDL_EltWiseLayer ,
  { "RELU",                            TIDL_tfliteMapReluParams },       //  TIDL_ReLULayer ,
  { "RELU6",                           TIDL_tfliteMapRelu6Params },       //  TIDL_ReLULayer ,
  { "MAX_POOL_2D",                     TIDL_tfliteMapMaxPoolParams },       //  TIDL_PoolingLayer ,
  { "AVERAGE_POOL_2D",                 TIDL_tfliteMapAvgPoolParams },      //  TIDL_PoolingLayer ,
  { "CONCATENATION",                   TIDL_tfliteMapConcatV2Params },       //  TIDL_ConcatLayer ,
  { "RESHAPE",                         TIDL_tfliteMapReshapeParams },       //  TIDL_ReshapeLayer ,
  { "SQUEEZE",                         TIDL_tfliteMapSqueezeParams },       //  TIDL_ReshapeLayer ,
  { "SOFTMAX",                         TIDL_tfliteMapSoftmaxParams },      //  TIDL_SoftMaxLayer ,
  { "ARG_MAX",                         TIDL_tfliteMapArgmaxParams },      //  TIDL_ArgMaxLayer ,
  { "PAD",                             TIDL_tfliteMapPadParams },       //  TIDL_SoftMaxLayer ,
  { "MEAN",                            TIDL_tfliteMapMeanParams },       //  TIDL_SoftMaxLayer ,
  { "FULLY_CONNECTED",                 TIDL_tfliteMapFullyConnectedParams } // TIDL_InnerProductLayer ,
};


int32_t TIDL_getTfliteOpParamMapId(const char  * name)
{
  int32_t i = -1;
  for (i = 0; i < sizeof(tidl_TfliteOpParamMapTable) / sizeof(sTIDL_tfliteOpParamMap_t); i++)
  {
    if ((strcmp(name, (const char *)tidl_TfliteOpParamMapTable[i].name) == 0))
    {
      return (i);
    }
  }
  return (-1);
}

int32_t TIDL_tfliteMapUnSuportedlayerParams(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  int32_t              *dataIndex,
  const Model*         tfliteModel)
{
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType = TIDL_UnSuportedLayer;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outData[0].dataId = (*dataIndex)++;
  return TIDL_IMPORT_NO_ERR;
}

void tidl_findTfliteOutputNames(const Model* tfliteModel, char * outList)
{
  int i, j, k, l;
  char tensorName[FILE_NAME_SIZE];
  char inTensorName[FILE_NAME_SIZE];
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  for (i = 0; i < operators->size(); i++)
  {
    int outDataUsed = 0;
    const auto *op = operators->Get(i);
    for(j = 0; j < op->outputs()->size(); j++)
    {
      strcpy((char *)tensorName, tensors->Get(op->outputs()->Get(j))->name()->c_str());
      for (k = 0; k < operators->size(); k++)
      {
        const auto *op1 = operators->Get(k);
        for (l = 0; l < op1->inputs()->size(); l++)
        { 
          strcpy((char *)inTensorName,  tensors->Get(op1->inputs()->Get(l))->name()->c_str());
          if (strcmp(tensorName, inTensorName) == 0)
          {
            outDataUsed = 1;
            break;
          }
          
          if (outDataUsed)
          break;
        }
        if (outDataUsed)
          break;
      }
      if (outDataUsed == 0)
      {
        strcpy(tensorName,tensors->Get(op->outputs()->Get(0))->name()->c_str());
        strcat(outList, tensorName);
        strcat(outList, ",");
      }
    }
  }
} 

int32_t tidl_tfliteLayerFillTensorNames(sTIDL_OrgNetwork_t   *pOrgTIDLNetStructure,
  int32_t              i,
  int32_t              layerIndex,
  const Model*         tfliteModel)
{
  int32_t j;
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();

  const auto *op = operators->Get(i);
  strcpy((char*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].name, tensors->Get(op->outputs()->Get(0))->name()->c_str());

  if (pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs > 0)
  {
    for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numInBufs; j++)
    { 
      if(pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].layerType == TIDL_Deconv2DLayer)
      {
        strcpy((char*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].inDataNames[j], tensors->Get(op->inputs()->Get(j+2))->name()->c_str());
      }
      else
      {
        strcpy((char*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].inDataNames[j], tensors->Get(op->inputs()->Get(j))->name()->c_str());
      }
    }
  }
  if (pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs > 0)
  {
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerLinked[0] = 0;
    for (j = 0; j < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs; j++)
    {
      strcpy((char*)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outDataNames[j], tensors->Get(op->outputs()->Get(j))->name()->c_str());
      pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerLinked[j] = 0;
    }
  }
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].weightsElementSizeInBits = NUM_WHGT_BITS;
  pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].strideOffsetMethod = TIDL_strideOffsetCenter;

  return 0;
}

int32_t tidl_tfliteGetNewNodeToAdd(sTIDL_OrgNetwork_t   &orgTIDLNetStructure,
  int32_t              layerIndex,
  const Model*         tfliteModel)
{
  int32_t i, j, nodeIdx = -1;

  for (i = 0; i < layerIndex; i++)
  {
    for (j = 0; j < orgTIDLNetStructure.TIDLPCLayers[i].numInBufs; j++)
    {
      if (TIDL_getLayerIdx(&orgTIDLNetStructure, layerIndex, (const char *)orgTIDLNetStructure.TIDLPCLayers[i].inDataNames[j]) == -1)
      {
        
        nodeIdx = TIDL_tfliteGetNodeIdx(tfliteModel, (const char *)orgTIDLNetStructure.TIDLPCLayers[i].inDataNames[j]);
        if (nodeIdx != -1)
        {
          break;
        }
      }
    }
    if (nodeIdx != -1)
    {
      break;
    }
  }
  return nodeIdx;
}

int32_t tidl_tfliteLayerUpdateConsumerCount(sTIDL_OrgNetwork_t *pOrgTIDLNetStructure,
                                        int32_t i,
                                        int32_t layerIndex,
                                        const Model* tfliteModel)
{
  int32_t i0, i1, i2;
  int32_t numCons = 0;
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors = (*tfliteModel->subgraphs())[0]->tensors();

  for (i0 = 0; i0 < pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].numOutBufs; i0++)
  {

    for (i1 = 0; i1 < operators->size(); i1++)
    {
      const auto *op = operators->Get(i1);
      for (i2 = 0; i2 < op->inputs()->size(); i2++)
      {        
        if (strcmp((const char *)pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outDataNames[i0], tensors->Get(op->inputs()->Get(i2))->name()->c_str()) == 0)
        {
          numCons++;
        }
      }
    }
    pOrgTIDLNetStructure->TIDLPCLayers[layerIndex].outConsumerCnt[i0] = numCons;
  }
  return 0;
}

extern int32_t tfLite_write(tidl_import_config * params, const Model* tfliteModel, int32_t (&nodeEvaluated)[TIDL_NUM_MAX_LAYERS], int32_t (&nodeNotImported)[TIDL_NUM_MAX_LAYERS]);
extern std::string out_data_name_list;

int32_t tfLite_import(tidl_import_config * params)
{
  int32_t i;
  int32_t layerIndex;
  int32_t tiLayerIndex;
  int32_t dataIndex;
  int32_t importStatus;
  int32_t numUnsupportedLayers, numErrs;
  int32_t mapTblIdx = -1;
  int32_t numNetOutData;
  char    outDataNames[TIDL_MAX_ALG_OUT_BUFS][TIDL_MAX_DATA_NAME];
  char    outDataNamesList[TIDL_MAX_ALG_OUT_BUFS*FILE_NAME_SIZE];
  int32_t nodeEvaluated[TIDL_NUM_MAX_LAYERS];
  int32_t nodeNotImported[TIDL_NUM_MAX_LAYERS];
  int32_t numNodeEvaluated = 0;
  FILE    *fpNetFile;
  FILE    *fpParamsFile;

  printf("TFLite Model (Flatbuf) File  : %s  \n",(const char *)params->inputNetFile);
  printf("TIDL Network File      : %s  \n", (const char *)params->outputNetFile);
  printf("TIDL IO Info File      : %s  \n", (const char *)params->outputParamsFile);
  uint8_t *buffer_pointer ;

  if (tidl_getFBModel(params->inputNetFile, &buffer_pointer) == -1)
  {
    printf("Failed in reading the TFLite model!!!");
    return TIDL_IMPORT_FAILURE;
  }
  auto* tfliteModel = GetModel(buffer_pointer);
  auto operators = (*tfliteModel->subgraphs())[0]->operators();
  auto tensors   = (*tfliteModel->subgraphs())[0]->tensors();
  printf("TFLite node size: %d\n",operators->size());
  layerIndex = 0;
  dataIndex  = 0;
  memset(nodeEvaluated, -2, TIDL_NUM_MAX_LAYERS*sizeof(int32_t));
  memset(nodeNotImported, -2, TIDL_NUM_MAX_LAYERS*sizeof(int32_t));

  if (out_data_name_list.empty()) {
    outDataNamesList[0] = '\0'; //Initialize outDataNamesList to an empty string
    tidl_findTfliteOutputNames(tfliteModel, (char*)outDataNamesList);
  } else {
    strcpy(outDataNamesList, out_data_name_list.c_str());
  }

  numNetOutData = tidl_getStringsFromList((char *)outDataNamesList,  (char*)outDataNames, TIDL_MAX_DATA_NAME);

  numErrs = 0;
  numUnsupportedLayers = 0;
  for (i = 0; i < numNetOutData; i++)
  {
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs =  1;
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs = -1;
    strcpy((char*)orgTIDLNetStructure.TIDLPCLayers[layerIndex].name, outDataNames[i]);
    strcpy((char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].inDataNames[0], outDataNames[i]);
    strcpy((char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[0], outDataNames[i]);
    layerIndex++;
  }

  int newNode = tidl_tfliteGetNewNodeToAdd(orgTIDLNetStructure, layerIndex, tfliteModel);
  nodeEvaluated[numNodeEvaluated++] = newNode;

  while (newNode != -1)
  {
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs = 1;
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs = 1;
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].inData[0].dataId = -1;
    const auto *op = operators->Get(newNode);
    auto operator_codes = (*tfliteModel->operator_codes())[op->opcode_index()]->builtin_code();
    const char* str = EnumNameBuiltinOperator(operator_codes);
    mapTblIdx = TIDL_getTfliteOpParamMapId(str);
    if (mapTblIdx == -1)
    {
      printf(" TFlite operator %s is not suported now..  By passing\n", str);
      TIDL_tfliteMapUnSuportedlayerParams(&orgTIDLNetStructure, newNode, layerIndex, &dataIndex, tfliteModel);
      numUnsupportedLayers++;
    }
    else
    {
      importStatus = tidl_TfliteOpParamMapTable[mapTblIdx].tidl_tfliteMapFunc(&orgTIDLNetStructure, newNode, layerIndex, &dataIndex, tfliteModel);
      if(importStatus != TIDL_IMPORT_NO_ERR)
      {
        printf("TIDL limitation: TFLite operator %s cannot be mapped to a TIDL layer.\n", str);
        numUnsupportedLayers++;
      }
    }

    tidl_tfliteLayerFillTensorNames(&orgTIDLNetStructure, newNode, layerIndex, tfliteModel);
    tidl_tfliteLayerUpdateConsumerCount(&orgTIDLNetStructure, newNode, layerIndex, tfliteModel);
    tidl_linkInputTensors(&orgTIDLNetStructure, layerIndex);
    tidl_linkOutputTensors(&orgTIDLNetStructure, layerIndex);
    layerIndex++;

    // Re-building the tidl supported subgraph
    if (numUnsupportedLayers > 0)
    {
      layerIndex = 0;
      dataIndex  = 0;
      numUnsupportedLayers = 0;
      numNetOutData = 0;
      memset((void *)&orgTIDLNetStructure, 0, sizeof(orgTIDLNetStructure));

      // Input of the current node (which is not supported by TIDL) would be
      // the output of the rebuilt subgraph
      for (i = 0; i < op->inputs()->size(); i++)
      {
        auto curTensorIn = tensors->Get(op->inputs()->Get(i));
        auto *data   = (*tfliteModel->buffers())[curTensorIn->buffer()]->data();

        if (!data) // Identify the tensor input (buffer inputs are excluded)
        {
          strcpy((char *)outDataNames[numNetOutData++], curTensorIn->name()->c_str());
        }
      }

      for (i = 0; i < numNetOutData; i++)
      {
        orgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs =  1;
        orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs = -1;
        strcpy((char*)orgTIDLNetStructure.TIDLPCLayers[layerIndex].name, outDataNames[i]);
        strcpy((char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].inDataNames[0], outDataNames[i]);
        strcpy((char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[0], outDataNames[i]);
        layerIndex++;
      }
      // update the list of not imported node
      for (i = 0; i < numNodeEvaluated; i++)
      {
        nodeNotImported[i] = nodeEvaluated[i];
      }
    }

    newNode = tidl_tfliteGetNewNodeToAdd(orgTIDLNetStructure, layerIndex, tfliteModel);
    nodeEvaluated[numNodeEvaluated++] = newNode;
  }

  if(numUnsupportedLayers > 0)
  {
    printf("\nImport error: This TFLite model has unsupported operators. "
             "Converting the supported operators in TIDL format, "
             "and leaving the others untouched.\n");
  }

  tidl_addInDataLayer(orgTIDLNetStructure, layerIndex, &dataIndex);
  layerIndex = orgTIDLNetStructure.numLayers;

  importStatus = tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\nImport error: This TFLite model's topology is not supported. "
           "Please check TIDL User's Guide for supported network topologies.\n");
    numErrs++;
  }

  tidl_fillInDataLayerShape(orgTIDLNetStructure,  params, layerIndex);
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);
  tidl_updateOutDataShape(orgTIDLNetStructure, 0, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_tfOutRehapeTable);

  importStatus = tidl_mergeBiasLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Bias layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeBNLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Batch Norm layer cannot be merged into other layers.\n");
    numErrs++;
  }

  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;

  importStatus = tidl_mergeBNLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Batch Norm layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergeReluLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Relu layer cannot be merged into other layers.\n");
    numErrs++;
  }

  importStatus = tidl_mergePadLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Pad layer cannot be merged into other layers.\n");
    numErrs++;
  }

  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;

  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);
  
  tidl_FindFlattenLayer(orgTIDLNetStructure, layerIndex);
  
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);

  importStatus = tidl_convertConv2DToIpLayer(orgTIDLNetStructure, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_tfOutRehapeTable);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Conv2D layer cannot be converted into Inner Product layer.\n");
    numErrs++;
  }

  importStatus = tidl_mergeFlattenLayer(orgTIDLNetStructure, layerIndex);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\n Import error: Flatten layer cannot be merged.\n");
    numErrs++;
  }

  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;

  importStatus = tidl_mergeReshapeLayer(orgTIDLNetStructure, layerIndex, (sTIDL_tfOutRehapeMap_t *)&sTIDL_tfOutRehapeTable);
  if(importStatus != TIDL_IMPORT_NO_ERR)
  {
    printf("\nImport error: Reshape layer cannot be merged.\n");
    numErrs++;
  }

  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure, layerIndex);
  layerIndex = orgTIDLNetStructure.numLayers;
  tidl_sortDataIds(&orgTIDLNetStructure, layerIndex);

  tidl_importEltWiseParams(&orgTIDLNetStructure, layerIndex);

  /* Quantize and write out layer params */
  fpParamsFile = fopen((const char *)params->outputParamsFile, "wb+");
  TIDL_importQuantWriteLayerParams(&orgTIDLNetStructure, layerIndex, fpParamsFile);

  tiLayerIndex = tidl_copyPCNetToDeviceNet(orgTIDLNetStructure, tIDLNetStructure, layerIndex, NUM_WHGT_BITS);

  /* Have a final check if there are any layers which
     - are supported only by merging with other TIDL layers,
     - but are not able to be merged. */
  numUnsupportedLayers = tidl_countUnsupportedLayers(&tIDLNetStructure, tiLayerIndex);
  if(numUnsupportedLayers > 0)
  {
    printf("\nImport error: This TFLite model has operators that are supported"
           " by TIDL only if they can be merged with TIDL layers. But these operators"
           " cannot be merged with any TIDL layer.\n"
           "Please check TIDL User's Guide for supported TFlite operators.\n");
    numErrs++;
  }

  if(numErrs > 0)
  {
    /* Stop the import process here to prevent potential crash if continuing */
    return TIDL_IMPORT_FAILURE;
  }

  /* Function to set Conv2dKernelType in layer params based on the "conv2dKernelType"
     parameter from import config file.
  */
  TIDL_setConv2dKernelType(&tIDLNetStructure, tiLayerIndex);

  tidl_addOutDataLayer(tIDLNetStructure, tiLayerIndex);

  fpNetFile = fopen((const char *)params->outputNetFile, "wb+");
  fwrite(&tIDLNetStructure,1,sizeof(tIDLNetStructure),fpNetFile);

  fclose(fpNetFile);
  fclose(fpParamsFile);

  tfLite_write(params, tfliteModel, nodeEvaluated, nodeNotImported);

  return TIDL_IMPORT_SUCCESS;
}

/*==============================================================================
+* Print TensorFlow lite operators supported by TIDL.
+==============================================================================*/
void tidl_printTfLiteSupport()
{
  int32_t i;

  printf("\nTIDL supports following TensorFlow Lite operators "
         "(please refer to TIDL documentation for more detail): \n");
  for(i=0; i<TIDL_NUM_SUPPORTED_TFLITE_OPERATORS; i++)
  {
    printf("   %d. %s\n", i+1, tidl_TfliteOpParamMapTable[i].name);
  }
}

