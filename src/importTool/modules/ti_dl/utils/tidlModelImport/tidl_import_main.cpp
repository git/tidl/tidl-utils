/*
 *
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the  
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#ifndef _WIN32
#include <unistd.h>  // for function execvp()
#endif

#include "tidl_import_config.h"
#include "ti_dl.h"

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
using namespace std;
using ::google::protobuf::Message;

#include "tidl_import_common.h"

sTIDL_OrgNetwork_t      orgTIDLNetStructure;
sTIDL_OrgNetwork_t      tempTIDLNetStructure;
sTIDL_Network_t         tIDLNetStructure;

void setDefaultParams(tidl_import_config * params)
{
  int i;
  params->randParams          = 0;
  params->modelType           = 0; // 0 - caffe, 1- tensorFlow
  params->quantizationStyle   = TIDL_quantStyleDynamic; 
  params->quantRoundAdd       = 50; // 0 - caffe, 1- tensorFlow
  params->numParamBits        = 8;
  params->rawSampleInData     = 0; // 0 - Encoded, 1- RAW
  params->numSampleInData     = 1;
  params->foldBnInConv2D      = 1;
  params->preProcType         = 0;
  params->inElementType       = TIDL_SignedChar;
  params->inQuantFactor       = -1;
  params->inMean[0] = params->inMean[1] = params->inMean[2] = -1.0;
  params->inScale[0] = params->inScale[1] = params->inScale[2] = -1.0;
  params->inWidth               = -1;
  params->inHeight              = -1;
  params->inNumChannels         = -1;
  for(i = 0; i < TIDL_NUM_MAX_LAYERS; i++)
  {
    params->layersGroupId[i] = 1;
    // By default, conv2d kernel type is automatically set instead of being read from config file
    params->conv2dKernelType[i] = -1;
  }

}

void tidlQuantStatsTool(tidl_import_config * params)
{
  FILE * fp;
  char sysCommand[500];

#ifdef _WIN32
  sprintf(sysCommand, "if exist tempDir rmdir /S/Q tempDir");
  system(sysCommand);
  sprintf(sysCommand, "mkdir tempDir");
  system(sysCommand);
  fp = fopen("tempDir\\configFilesList.txt", "w+");
  
  if(fp== NULL)
  {
    printf("Could not open config  file tempDir\\configFilesList.txt  \n");
    return;
  }
  fprintf(fp, "1 .\\tempDir\\qunat_stats_config.txt \n0\n" );
  fclose(fp);

  fp = fopen("tempDir\\qunat_stats_config.txt", "w+");
  if(fp== NULL)
  {
    printf("Could not open config  file tempDir\\qunat_stats_config.txt  \n");
    return;
  }
  fprintf(fp, "rawImage    = %d\n",params->rawSampleInData);
  fprintf(fp, "numFrames   = %d\n",params->numSampleInData);
  fprintf(fp, "preProcType  = %d\n",params->preProcType);
  if( (params->inMean[2] != -1.0) && (params->inScale[2] != -1.0) )
  {
    fprintf(fp, "inMean  = %f %f %f\n", params->inMean[0], params->inMean[1], params->inMean[2]);
    fprintf(fp, "inScale = %f %f %f\n", params->inScale[0], params->inScale[1], params->inScale[2]);
  }
  fprintf(fp, "inData   = %s\n",params->sampleInData);
  fprintf(fp, "traceDumpBaseName   = \".\\tempDir\\trace_dump_\"\n");
  fprintf(fp, "outData   = \".\\tempDir\\stats_tool_out.bin\"\n");
  fprintf(fp, "updateNetWithStats   = 1\n");
  fprintf(fp, "outputNetBinFile     = %s\n",params->outputNetFile);
  fprintf(fp, "paramsBinFile        = %s\n",params->outputParamsFile);
  fprintf(fp, "netBinFile   = \".\\tempDir\\temp_net.bin\"\n");
  fclose(fp);
  
  sprintf(sysCommand, "copy  %s .\\tempDir\\temp_net.bin",params->outputNetFile) ;
  system(sysCommand);
  sprintf(sysCommand, " %s .\\tempDir\\configFilesList.txt",params->tidlStatsTool);
  system(sysCommand);  
#else
  system ("rm -rf tempDir; mkdir -p ./tempDir");
  fp = fopen("tempDir/configFilesList.txt", "w+");
  if(fp== NULL)
  {
    printf("Could not open config  file tempDir/configFilesList.txt  \n");
    return;
  }
  fprintf(fp, "1 ./tempDir/qunat_stats_config.txt \n0\n" );
  fclose(fp);

  fp = fopen("tempDir/qunat_stats_config.txt", "w+");
  if(fp== NULL)
  {
    printf("Could not open config  file tempDir/qunat_stats_config.txt  \n");
    return;
  }
  fprintf(fp, "rawImage    = %d\n",params->rawSampleInData);
  fprintf(fp, "numFrames   = %d\n",params->numSampleInData);
  fprintf(fp, "preProcType  = %d\n",params->preProcType);
  if( (params->inMean[2] != -1.0) && (params->inScale[2] != -1.0) )
  {
    fprintf(fp, "inMean  = %f %f %f\n", params->inMean[0], params->inMean[1], params->inMean[2]);
    fprintf(fp, "inScale = %f %f %f\n", params->inScale[0], params->inScale[1], params->inScale[2]);
  }
  fprintf(fp, "inData   = %s\n",params->sampleInData);
  fprintf(fp, "traceDumpBaseName   = \"./tempDir/trace_dump_\"\n");
  fprintf(fp, "outData   = \"./tempDir/stats_tool_out.bin\"\n");
  fprintf(fp, "updateNetWithStats   = 1\n");
  fprintf(fp, "outputNetBinFile     = %s\n",params->outputNetFile);
  fprintf(fp, "paramsBinFile        = %s\n",params->outputParamsFile);
  fprintf(fp, "netBinFile   = \"./tempDir/temp_net.bin\"\n");
  fclose(fp);
  
  sprintf(sysCommand, "cp  %s ./tempDir/temp_net.bin",params->outputNetFile) ;
  system(sysCommand);
  sprintf(sysCommand, " %s ./tempDir/configFilesList.txt",params->tidlStatsTool);
  // use execvp() instead of system() to search the executable in directories specified by env PATH
  char *args[]={(char *)params->tidlStatsTool, (char *) "./tempDir/configFilesList.txt", NULL};
  if(execvp(args[0],args)) {
    printf("Couldn't open tidlStatsTool file: %s  \n", params->tidlStatsTool);
    exit(0);
  }
#endif
  
  return;
  
}

/**
----------------------------------------------------------------------------
@ingroup    TIDL_Import
@fn         tidlValidateImportParams
@brief      Function validates input parameters related to tidl import 
            sets appropriate error in response to violation from 
            expected values.
            
@param      params : TIDL Create time parameters
@remarks    None
@return     Error related to parameter.
----------------------------------------------------------------------------
*/
int32_t tidlValidateImportParams(tidl_import_config * params)
{  

  /* randParams can be either 0 or 1*/
  if((params->randParams != 0) && (params->randParams != 1))
  {
    printf("\n Invalid randParams setting : set either 0 or 1");
    return -1;
  }
  /* modelType can be 0, 1, 2, or 3*/
  else if((params->modelType != 0) && (params->modelType != 1) && (params->modelType != 2) && (params->modelType != 3))
  {
    printf("\n Invalid modelType parameter setting : set to 0, 1, 2, or 3");
    return -1;
  }   
  /* Currently quantizationStyle = 1 is supported */
  /*else if(params->quantizationStyle != 1)
  {
    printf("\n Invalid quantizationStyle parameter setting : set it to 1");
    return -1;
  }*/
  /* quantRoundAdd can be 0 to 100 */
  else if((params->quantRoundAdd < 0) || (params->quantRoundAdd > 100))
  {
    printf("\n Invalid quantRoundAdd parameter setting : set it 0 to 100");
    return -1;
  }  
  /* numParamBits can be 4 to 12 */
  else if((params->numParamBits < 4) || (params->numParamBits > 12))
  {
    printf("\n Invalid numParamBits parameter setting : set it 4 to 12");
    return -1;
  } 
  /* rawSampleInData can be either 0 or 1*/
  else if((params->rawSampleInData != 0) && (params->rawSampleInData != 1))
  {
    printf("\n Invalid rawSampleInData parameter setting : set either 0 or 1");
    return -1;
  }   
  /* numSampleInData can be >0  */
  else if(params->numSampleInData <= 0)
  {
    printf("\n Invalid numSampleInData parameter setting : set it to >0 ");
    return -1;
  }    
  /* foldBnInConv2D can be either 0 or 1*/
  else if((params->foldBnInConv2D != 0) && (params->foldBnInConv2D != 1))
  {
    printf("\n Invalid foldBnInConv2D parameter setting : set either 0 or 1");
    return -1;
  }    
  /* preProcType can be 0 to 6, or 256 */
  else if((params->preProcType < 0) || ((params->preProcType > 6) && (params->preProcType != 256)))
  {
    printf("\n Invalid preProcType parameter setting : set it 0 to 6, or 256");
    return -1;
  }    
  else if(   (params->preProcType == 256) && (params->rawSampleInData == 0) 
          && ((params->inMean[2] == -1) || (params->inScale[2] == -1)) )
  {
    printf("\n For preProcType 256, inMean and inScale MUST be provided per channel.");
    return -1;
  }
  /* inElementType can be either 0 or 1*/
  else if((params->inElementType != 0) && (params->inElementType != 1))
  {
    printf("\n Invalid inElementType parameter setting : set either 0 or 1");
    return -1;
  }  
  /* inQuantFactor can be >0  */
  else if((params->inQuantFactor < -1) || (params->inQuantFactor == 0))
  {
    printf("\n Invalid inQuantFactor parameter setting : set it to >0 ");
    return -1;
  }
  /* inWidth can be >0  */
  else if((params->inWidth < -1) || (params->inWidth == 0))
  {
    printf("\n Invalid inWidth parameter setting : set it to >0 ");
    return -1;
  } 
  /* inHeight can be >0  */
  else if((params->inHeight < -1) || (params->inHeight == 0))
  {
    printf("\n Invalid inHeight parameter setting : set it to >0 ");
    return -1;
  } 
  /* inNumChannels can be 1 to 1024  */
  else if((params->inNumChannels < -1) || (params->inNumChannels == 0) || (params->inNumChannels > 1024))
  {
    printf("\n Invalid inNumChannels parameter setting : set it 1 to 1024 ");
    return -1;
  }  
  else
  {
    return 0;
  }
}

std::string out_data_name_list;
int32_t main(int32_t argc, char *argv[])
{
  int32_t status;
  FILE * fp;

  if(argc < 2)
  {
    printf("Number of input parameters are not enough \n");
    printf("Linux Usage : \n tidl_model_import.out config_file.txt or tidl_model_import.out --help\n");
    printf("Windows Usage : \n tidl_model_import.out.exe config_file.txt or tidl_model_import.out.exe --help\n");
    exit(-1);  
  }

  if(strcmp(argv[1],"--help") == 0)
  { /* Print what TIDL supports for each frame work. */
    tidl_printCaffeSupport();
    tidl_printTfSupport();
    tidl_printOnnxSupport();
    tidl_printTfLiteSupport();
    return(0);
  }

  fp = fopen(argv[1], "r");
  if(fp== NULL)
  {
    printf("Could not open config  file : %s  \n",argv[1]);
    return(0);
  }
  fclose(fp);

  setDefaultParams(&gParams);

  status = readparamfile(argv[1], &gsTokenMap_tidl_import_config[0]);

  if(status == -1)
  {
    printf("Parser Failed");
    return -1 ;
  }

  out_data_name_list = argc >= 3 ? argv[2] : "";

  status = tidlValidateImportParams(&gParams);
  if(status == -1)
  {
    printf("\n Validation of Parameters Failed \n");
    return -1 ;
  }

  /* Check if inputNetFile and inputParamsFile are available */
  fp = fopen((const char *)gParams.inputNetFile, "r");
  if(fp== NULL)
  {
    printf("Couldn't open inputNetFile file: %s  \n", gParams.inputNetFile);
    return(0);
  }
  fclose(fp);

  if(gParams.modelType == 0 && gParams.randParams != 1) /* only Caffe model has input params file */
  {
    fp = fopen((const char *)gParams.inputParamsFile, "r");
    if(fp== NULL)
    {
      printf("Couldn't open inputParamsFile file: %s  \n", gParams.inputParamsFile);
      return(0);
    }
    fclose(fp);
  }

  /* Check if outputNetFile and outputParamsFile are accessable for write */
  fp = fopen((const char *)gParams.outputNetFile, "w");
  if(fp== NULL)
  {
    printf("Couldn't open outputNetFile file to write: %s  \n", gParams.outputNetFile);
    return(0);
  }
  fclose(fp);

  fp = fopen((const char *)gParams.outputParamsFile, "w");
  if(fp== NULL)
  {
    printf("Couldn't open outputParamsFile file to write: %s  \n", gParams.outputParamsFile);
    return(0);
  }
  fclose(fp);

  /* First part of import process - parse user provided model and check for 
     violations to TIDL constraints */
  printf("\n=============================== TIDL import - parsing ===============================\n\n");

  if(gParams.modelType == 0)
  {
    if(gParams.inQuantFactor == -1)
    {
      gParams.inQuantFactor = 255;
    }
    status = caffe_import(&gParams);
  }
  else if (gParams.modelType == 1)
  {
    if(gParams.inQuantFactor == -1)
    {
      if(gParams.inElementType == TIDL_SignedChar)
      {
        gParams.inQuantFactor = 128*255;
      }
      else /* if(gParams.inElementType == TIDL_UnsignedChar) */
      {
        gParams.inQuantFactor = 256*255;
      }
    }
    status = tf_import(&gParams);
  }
  else if (gParams.modelType == 2)
  {
    if(gParams.inQuantFactor == -1)
    {
      gParams.inQuantFactor = 48*255;
    }
    status = onnx_import(&gParams);
  }
  else if (gParams.modelType == 3)
  {
    if(gParams.inQuantFactor == -1)
    {
      if(gParams.inElementType == TIDL_SignedChar)
      {
        gParams.inQuantFactor = 128*255;
      }
      else /* if(gParams.inElementType == TIDL_UnsignedChar) */
      {
        gParams.inQuantFactor = 256*255;
      }
    }
    status = tfLite_import(&gParams);
  }
  else
  {
    printf("Model not supported. Please rerun import tool with --help to find supported models.\n");
  }

  if(status == TIDL_IMPORT_FAILURE)
  {
    printf("\nTIDL import failed. Please check error messages. \n\n");

    // Terminate import process to prevent crash or hanging in calibration 
    return(0);
  }

  /* Second part of import process - calibrate TIDL parameters by running simulation */
  printf("\n=============================== TIDL import - calibration ===============================\n\n");

  fp = fopen((const char *)gParams.sampleInData, "r");
  if(fp== NULL)
  {
    printf("Couldn't open sampleInData file: %s  \n", gParams.sampleInData);
    return(0);
  }
  fclose(fp);

#ifdef _WIN32
  // This is only needed in Windows as execvp() checks if this file exists in Linux.
  fp = fopen((const char *)gParams.tidlStatsTool, "r");
  if(fp== NULL)
  {
    printf("Couldn't open tidlStatsTool file: %s  \n", gParams.tidlStatsTool);
    return(0);
  }
  fclose(fp);
#endif

  // invoke calibration 
  tidlQuantStatsTool(&gParams);

  return (0);
}
