This folder contains the source files of TIDL import tool. To build the import tool in x86 Linux, follow these steps:
1. Setup environment variables:
   - to build for x86 executable: 
      export PLATFORM_BUILD=x86
      export PROTOBUF_LIB_DIR=<protobuf lib folder>, e.g. in yocto, ~/yocto-plsdk/tisdk/build/arago-tmp-external-linaro-toolchain/sysroots-components/x86_64/protobuf-native/usr/lib
      export PROTOBUF_INC_DIR=<protobuf inc folder>, e.g. in yocto, ~/yocto-plsdk/tisdk/build/arago-tmp-external-linaro-toolchain/sysroots-components/x86_64/protobuf-native/usr/include
   - to build for ARM executable:
      export LINUXENV=oearm
      export LINUX_BUILD_TOOLS=<ARM toolchain>/bin/arm-linux-gnueabihf-
      export PROTOBUF_LIB_DIR=<protobuf lib folder>, e.g. ~/yocto-plsdk/tisdk/build/arago-tmp-external-linaro-toolchain/sysroots-components/armv7ahf-neon/protobuf/usr/lib
      export PROTOBUF_INC_DIR=<protobuf inc folder>, e.t. ~/yocto-plsdk/tisdk/build/arago-tmp-external-linaro-toolchain/sysroots-components/armv7ahf-neon/protobuf/usr/include
   - add path of protoc to environment variable PATH, e.g., export PATH=$PATH:~/yocto-plsdk/tisdk/build/arago-tmp-external-linaro-toolchain/sysroots-components/x86_64/protobuf-native/usr/bin
   - Note that when building for x86, "LINUXENV" must be unset if it's been set, and same for "PLATFORM_BUILD" when building for ARM. 

2. Go to folder modules/ti_dl/utils/caffeImport and run protoc to genenrate .cc and .h files:
   protoc --proto_path=. --cpp_out=. caffe.proto

3. Go to folder modules/ti_dl/utils/tfImport and run protoc to genenrate .cc and .h files:
   source genProtoC.sh

4. go to folder modules/ti_dl/utils/tidlModelImport and run makefile:
   make 

